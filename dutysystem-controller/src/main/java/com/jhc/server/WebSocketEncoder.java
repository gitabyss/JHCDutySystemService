package com.jhc.server;

import com.alibaba.fastjson.JSON;
import com.jhc.utils.CommonResult;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

/**
 * @Author: zfm
 * @Date: 2019/12/11 08:54
 */
public class WebSocketEncoder implements Encoder.Text<CommonResult> {
    @Override
    public String encode(CommonResult commonResult) throws EncodeException {
        return JSON.toJSONString(commonResult);
    }

    @Override
    public void init(EndpointConfig endpointConfig) {

    }

    @Override
    public void destroy() {

    }
}
