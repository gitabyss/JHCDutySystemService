package com.jhc.controller;


import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.dto.BacTermAddDto;
import com.jhc.dto.BacTermSearchDto;
import com.jhc.dto.BacTermViewDto;
import com.jhc.entity.BacTerm;
import com.jhc.entity.BaseEntity;
import com.jhc.service.IBacTermService;
import com.jhc.utils.CommonResult;
import com.jhc.utils.JHC;
import com.jhc.vo.PageVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 学期信息  前端控制器
 * </p>
 *
 * @author zhuzhixu
 */
@RestController
@RequestMapping("/term")
@Api(tags = "基础 学期周次操作")
public class BacTermController {
    @Autowired
    private IBacTermService iBacTermService;

    @ApiOperation("学期查询分页term:termPageData")
    @PostMapping("/termPageData")
    @PreAuthorize("hasAuthority('term:termPageData')")
    public CommonResult<IPage<BacTermViewDto>> getTermPageData(@RequestBody PageVo<BacTermSearchDto> pageVo) {
        return CommonResult.success(iBacTermService.getTeacherPageData(pageVo));
    }

    @ApiOperation("学期信息查询通过id term:termDetail")
    @GetMapping("/termDetail/{id}")
    @PreAuthorize("hasAuthority('term:termDetail')")
    public CommonResult<BacTermViewDto> getTermDetailById(@ApiParam(value = "学期信息编号", required = true) @PathVariable(value = "id") Long id) {
        BacTerm bacTerm = iBacTermService.getById(id);
        if (ObjectUtil.isNotNull(bacTerm)) {
            BacTermViewDto bacTermViewDto = new BacTermViewDto();
            BeanUtils.copyProperties(bacTerm, bacTermViewDto);
            return CommonResult.success(bacTermViewDto);
        }
        return CommonResult.success(null, "查询信息不存在");
    }

    @ApiOperation("添加学期信息term:addTerm")
    @PostMapping
    @PreAuthorize("hasAuthority('term:addTerm')")
    public CommonResult addTerm(@RequestBody @Validated BacTermAddDto bacTermAddDto) {
        String termName = bacTermAddDto.getTermName();
        BacTerm bacTerm = iBacTermService.getOne(new QueryWrapper<BacTerm>().lambda()
                .select(BaseEntity::getId).eq(BacTerm::getTermName, termName));
        String startDate = JHC.toDateString(bacTermAddDto.getOpeningDate());
        String endDate = JHC.toDateString(bacTermAddDto.getEndDate());
        List<BacTerm> bacTerm1 = iBacTermService.list(new QueryWrapper<BacTerm>().lambda()
                .le(BacTerm::getOpeningDate,startDate)
                .ge(BacTerm::getEndDate,startDate).or(i -> i.le(BacTerm::getOpeningDate,endDate)
                        .ge(BacTerm::getEndDate,endDate)).or(i -> i.ge(BacTerm::getOpeningDate,startDate)
                        .le(BacTerm::getEndDate,endDate)));
        if (bacTerm1.size()!=0){
            return CommonResult.failed("学期时间段覆盖");
        }
        if (bacTerm != null) {
            return CommonResult.failed("学期已存在");
        }
        if (iBacTermService.addTerm(bacTermAddDto)) {
            return CommonResult.success("生成成功");
        }
        return CommonResult.failed("生成失败");
    }

    @GetMapping("/termList")
    @ApiOperation("获取学期下拉框(到当前)")
    public CommonResult getTermList(){
        return CommonResult.success(iBacTermService.getTermList());
    }

    @GetMapping("/allTermList")
    @ApiOperation("获取学期下拉框(所有)")
    public CommonResult getAllTermList(){
        return CommonResult.success(iBacTermService.getAllTermList());
    }

    @GetMapping("/weekList/{termId}")
    @ApiOperation("获取周次下拉列表(到当前)")
    public CommonResult getWeekList(@PathVariable(name = "termId")Long termId){
        return CommonResult.success(iBacTermService.getWeekList(termId));
    }

    @GetMapping("/allWeekList/{termId}")
    @ApiOperation("获取周次下拉列表(所有)")
    public CommonResult getAllWeekList(@PathVariable(name = "termId")Long termId){
        return CommonResult.success(iBacTermService.getAllWeekList(termId));
    }

    @GetMapping("/nowDateInfo")
    @ApiOperation("获取当前日期周次")
    public CommonResult getNowDateInfo(){
        return CommonResult.success(iBacTermService.getNowDateInfo());
    }
}
