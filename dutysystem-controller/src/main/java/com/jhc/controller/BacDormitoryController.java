package com.jhc.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.dto.BacDormitorySearchDto;
import com.jhc.dto.BacDormitoryViewDto;
import com.jhc.dto.CmsStudentDormitoryViewDto;
import com.jhc.service.IBacDormitoryService;
import com.jhc.utils.CommonResult;
import com.jhc.vo.PageVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 寝室   前端控制器
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Api(tags = "基础 寝室")
@RestController
@RequestMapping("/dormitory")
public class BacDormitoryController {
    @Autowired
    private IBacDormitoryService iBacDormitoryService;

    @ApiOperation("寝室信息分页条件查询dormitory:dormitoryPageData")
    @PostMapping("/dormitoryPageData")
    @PreAuthorize("hasAuthority('dormitory:dormitoryPageData')")
    public CommonResult<IPage<BacDormitoryViewDto>> getDormitoryPageData(@RequestBody PageVo<BacDormitorySearchDto> pageVo) {
        return CommonResult.success(iBacDormitoryService.getDormitoryPageData(pageVo));
    }

    @ApiOperation("根据寝室编号查询学生数据dormitory:studentList")
    @GetMapping("/studentList")
    @PreAuthorize("hasAuthority('dormitory:studentList')")
    public CommonResult<List<CmsStudentDormitoryViewDto>> getStudentListByDormitory(@RequestParam(value = "dormitoryId", required = true) Long dormitoryId) {
        return CommonResult.success(iBacDormitoryService.getStudentListByDormitoryId(dormitoryId));
    }
}
