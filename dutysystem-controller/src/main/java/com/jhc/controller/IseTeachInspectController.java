package com.jhc.controller;


import com.jhc.dto.*;
import com.jhc.server.WebSocketServer;
import com.jhc.service.IBacTermService;
import com.jhc.service.IIseInspectionService;
import com.jhc.service.IIseTeachInspectService;
import com.jhc.utils.CommonResult;
import com.jhc.vo.DateVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 教学检查  前端控制器
 * </p>
 *
 * @author zfm
 * @since 2019-12-03
 */
@RestController
@RequestMapping("/iseTeachInspect")
@Api(tags = "检查 教学检查")
public class IseTeachInspectController {

    @Autowired
    private IIseTeachInspectService teachInspectService;

    @Autowired
    private IBacTermService termService;

    @Autowired
    private IIseInspectionService inspectionService;

    @GetMapping("/teachInspect")
    @ApiOperation("查询当前教学检查iseTeachInspect:teachInspect:get")
    @PreAuthorize("hasAuthority('iseTeachInspect:teachInspect:get')")
    public CommonResult getTeachInspect(IseTeaInspectSearchDto teaInspectSearchDto){
        return CommonResult.success(teachInspectService.getTeachInspect(teaInspectSearchDto));
    }

    @GetMapping("/studyInspectProgress")
    @ApiOperation("查询当前教学检查进度iseTeachInspect:studyInspectProgress:get")
//    @PreAuthorize("hasAuthority('iseTeachInspect:studyInspectProgress:get')")
    public CommonResult getProgress(){
        return CommonResult.success(teachInspectService.getProgress());
    }

    @GetMapping("/finishTeach")
    @ApiOperation("查询当前用户已完成的教学检查iseTeachInspect:finishTeach:get")
    @PreAuthorize("hasAuthority('iseTeachInspect:finishTeach:get')")
    public CommonResult getFinishTeach(FinishTeachSearchDto searchDto){
        AllTeachSearchDto allTeachSearchDto = new AllTeachSearchDto();
        BeanUtils.copyProperties(searchDto,allTeachSearchDto);
        return CommonResult.success(teachInspectService.getFinishTeach(allTeachSearchDto,true));
    }

    @GetMapping("/allTeach")
    @ApiOperation("查询所有已完成的教学检查iseTeachInspect:allTeach:get")
    @PreAuthorize("hasAuthority('iseTeachInspect:allTeach:get')")
    public CommonResult getAllTeach(AllTeachSearchDto searchDto){
        return CommonResult.success(teachInspectService.getFinishTeach(searchDto,false));
    }

    @GetMapping("/detailTeachInspect/{id}")
    @ApiOperation("查询详细教学检查信息(检查总表id)iseTeachInspect:detailTeachInspect:id:get")
    @PreAuthorize("hasAuthority('iseTeachInspect:detailTeachInspect:id:get')")
    public CommonResult getDetailTeachInspect(@PathVariable Long id){
        return CommonResult.success(teachInspectService.getDetailTeachInspect(id));
    }

    @GetMapping("/feedbackTeach")
    @ApiOperation("查询反馈列表iseTeachInspect:feedbackTeach:get")
    @PreAuthorize("hasAuthority('iseTeachInspect:feedbackTeach:get')")
    public CommonResult getFeedback7s(AllTeachSearchDto searchDto){
        return CommonResult.success(teachInspectService.getFinishTeach(searchDto,null));
    }

    @PostMapping("/feedbackTeach")
    @ApiOperation("填写反馈iseTeachInspect:feedbackTeach:insert")
    @PreAuthorize("hasAuthority('iseTeachInspect:feedbackTeach:insert')")
    public CommonResult addFeedbackTeach(@RequestBody @Validated FeedBackAddDto feedBackAddDto){
        Boolean addResult = teachInspectService.addFeedbackTeach(feedBackAddDto);
        if (addResult != null && addResult == true){
            return CommonResult.success("填写成功");
        }
        return CommonResult.failed("填写失败");
    }

    @PostMapping("/iseTeachInspect")
    @ApiOperation("填写教学检查iseTeachInspect:iseTeachInspect:insert")
    @PreAuthorize("hasAuthority('iseTeachInspect:iseTeachInspect:insert')")
    public CommonResult add7sInspect(@RequestBody @Validated IseTeaInspectAddDto addDto){
        Map<String,Object> addResult = teachInspectService.addTeachInspect(addDto);
        if (addResult!=null){
            List<BizCheckMessageDto> messageDtos = (List<BizCheckMessageDto>) addResult.get("msgList");
            if (messageDtos!=null){
                messageDtos.forEach(item -> {
                    try {
                        WebSocketServer.sendInfo(CommonResult.success(item), item.getUserNumber());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            }
            return CommonResult.success("添加成功");
        }
        return CommonResult.failed("添加失败");
    }

    @PutMapping("/iseTeachInspect")
    @ApiOperation("修改教学检查iseTeachInspect:iseTeachInspect:update")
    @PreAuthorize("hasAuthority('iseTeachInspect:iseTeachInspect:update')")
    public CommonResult updateTeachInspect(@RequestBody @Validated IseTeaInspectUpdateDto updateDto){
        Boolean updateResult = teachInspectService.updateTeachInspect(updateDto);
        if (updateResult==true){
            return CommonResult.success("修改成功");
        }
        return CommonResult.failed("修改失败");
    }

//    @GetMapping("/analysisTeach")
//    @ApiOperation("查询教学数据分析")
//    public CommonResult getAnalysisTeach(@Validated AnalysisSearchDto searchDto){
//        if (searchDto.getStartDate()==null||searchDto.getEndDate()==null){
//            DateVo dateVo = termService.getTermDate(new Date());
//            searchDto.setStartDate(dateVo.getStartDate());
//            searchDto.setEndDate(dateVo.getEndDate());
//        }
//        return CommonResult.success(teachInspectService.getAnalysis(searchDto));
//
//    }

    @GetMapping("/analysisStudent")
    @ApiOperation("查询教学检查任教班级学情数据分析iseTeachInspect:analysisStudent:get")
    @PreAuthorize("hasAuthority('iseTeachInspect:analysisStudent:get')")
    public CommonResult getAnalysisStudent(@Validated AnalysisDateNumSearchDto numSearchDto){
        AnalysisNumberSearchDto searchDto = new AnalysisNumberSearchDto();
        //查询日期
        DateVo dateVo = inspectionService.getDate(numSearchDto.getType(),numSearchDto.getDateId());
        searchDto.setStartDate(dateVo.getStartDate());
        searchDto.setEndDate(dateVo.getEndDate());
        searchDto.setNumber(numSearchDto.getNumber());
        return CommonResult.success(teachInspectService.getAnalysisStudent(searchDto));
    }

    @GetMapping("/analysisTeacher")
    @ApiOperation("查询教学检查教师授课状态数据分析iseTeachInspect:analysisTeacher:get")
    @PreAuthorize("hasAuthority('iseTeachInspect:analysisTeacher:get')")
    public CommonResult getAnalysisTeacher(@Validated AnalysisDateNumSearchDto numSearchDto){
        AnalysisNumberSearchDto searchDto = new AnalysisNumberSearchDto();
        //查询日期
        DateVo dateVo = inspectionService.getDate(numSearchDto.getType(),numSearchDto.getDateId());
        searchDto.setStartDate(dateVo.getStartDate());
        searchDto.setEndDate(dateVo.getEndDate());
        searchDto.setNumber(numSearchDto.getNumber());
        return CommonResult.success(teachInspectService.getAnalysisTeacher(searchDto));
    }

    @GetMapping("/iseTeachCountByItemId/{itemId}")
    @ApiOperation("获取应到人数(对象id使用)iseTeachInspect:iseTeachCountByItemId:id:get")
    @PreAuthorize("hasAuthority('iseTeachInspect:iseTeachCountByItemId:id:get')")
    public CommonResult getTeachCount(@PathVariable(name = "itemId")Long id){
        return CommonResult.success(teachInspectService.getTeachCount(id));
    }

    @GetMapping("/iseTeachCountByInspectId/{inspectId}")
    @ApiOperation("获取应到人数(检查总表id使用)iseTeachInspect:iseTeachCountByInspectId:id:get")
    @PreAuthorize("hasAuthority('iseTeachInspect:iseTeachCountByInspectId:id:get')")
    public CommonResult getTeachCountByInspectId(@PathVariable(name = "inspectId")Long id){
        return CommonResult.success(teachInspectService.getTeachCountByInspectId(id));
    }

}
