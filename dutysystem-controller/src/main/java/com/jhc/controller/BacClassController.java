package com.jhc.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.bo.UserSaveDetails;
import com.jhc.dto.*;
import com.jhc.service.IBacClassService;
import com.jhc.service.IIseInspectionService;
import com.jhc.utils.CommonResult;
import com.jhc.vo.DateVo;
import com.jhc.vo.PageVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 行政班  前端控制器
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@RestController
@RequestMapping("/class")
@Api(tags = "基础 行政班")
public class BacClassController {
    @Autowired
    private IBacClassService iBacClassService;

    @Autowired
    private UserSaveDetails userSaveDetails;

    @Autowired
    private IIseInspectionService inspectionService;

    @ApiOperation("行政班分页条件查询class:classPageData")
    @PostMapping("/classPageData")
    @PreAuthorize("hasAuthority('class:classPageData')")
    public CommonResult<IPage<BacClassViewDto>> getClassPageData(@RequestBody PageVo<BacClassSearchDto> pageVo) {
        return CommonResult.success(iBacClassService.getClassPageData(pageVo));
    }

    @ApiOperation("行政班学生分页条件查询class:classStuPageData")
    @PostMapping("/classStuPageData")
    @PreAuthorize("hasAuthority('class:classStuPageData')")
    public CommonResult<IPage<BacClassStuViewDto>> getClassStuPageData(@RequestBody PageVo<BacClassStuSearchDto> pageVo) {
        return CommonResult.success(iBacClassService.getClassStuPageData(pageVo));
    }

    @GetMapping("/classList")
    @ApiOperation("查询班级下拉框")
    public CommonResult getClassList(AnalysisAllSearchDto allSearchDto){
        AnalysisSearchDto searchDto = new AnalysisSearchDto();
        //查询日期
        DateVo dateVo = inspectionService.getDate(allSearchDto.getType(),allSearchDto.getDateId());
        searchDto.setStartDate(dateVo.getStartDate());
        searchDto.setEndDate(dateVo.getEndDate());
        return CommonResult.success(iBacClassService.getClassByTeacherNumber(searchDto));
    }
}
