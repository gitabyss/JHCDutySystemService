package com.jhc.controller;


import com.jhc.dto.IseImageViewDto;
import com.jhc.entity.IseImages;
import com.jhc.mapper.IseImagesMapper;
import com.jhc.service.IIseImagesService;
import com.jhc.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 * 检查图片表 前端控制器
 * </p>
 *
 * @author zfm
 * @since 2019-12-10
 */
@RestController
@RequestMapping("/imagesAll")
@Api(tags = "基础 图片")
public class IseImagesController {

    @Autowired
    private IIseImagesService imagesService;

    @PostMapping("/upload")
    @ApiOperation("检查图片上传")
    public CommonResult upload(@RequestParam(name = "file")MultipartFile file){
        IseImageViewDto images = imagesService.upload(file);
        if (images!=null){
            return CommonResult.success(images);
        }
        return CommonResult.failed("上传失败");
    }

}
