package com.jhc.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.dto.BacStudySearchDto;
import com.jhc.dto.BacStudyViewDto;
import com.jhc.service.IBacStudyService;
import com.jhc.utils.CommonResult;
import com.jhc.vo.PageVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 自习表  前端控制器
 * </p>
 *
 * @author zfm
 * @since 2019-12-06
 */
@Api(tags = "基础 自习")
@RestController
@RequestMapping("/study")
public class BacStudyController {
    @Autowired
    private IBacStudyService iBacStudyService;

    @ApiOperation("自习表分页条件查询study:studyPageData")
    @PostMapping("/studyPageData")
    @PreAuthorize("hasAuthority('study:studyPageData')")
    public CommonResult<IPage<BacStudyViewDto>> studyPageData(@RequestBody PageVo<BacStudySearchDto> pageVo) {
        return CommonResult.success(iBacStudyService.studyPageData(pageVo));
    }
}
