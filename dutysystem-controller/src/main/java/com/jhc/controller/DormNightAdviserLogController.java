package com.jhc.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 夜不归宿班主任操作记录 前端控制器
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
@RestController
@RequestMapping("/dorm-night-adviser-log")
public class DormNightAdviserLogController {

}
