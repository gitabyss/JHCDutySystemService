package com.jhc.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.dto.CmsPermissionAdd;
import com.jhc.dto.CmsPermissionBanRestart;
import com.jhc.dto.CmsPermissionUpdate;
import com.jhc.entity.CmsPermission;
import com.jhc.service.ICmsPermissionService;
import com.jhc.utils.CommonResult;
import io.swagger.annotations.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * cms_permission  前端控制器
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@RestController
@RequestMapping("/permission")
@Api(tags = "用户 权限接口")
public class CmsPermissionController {
    @Autowired
    private ICmsPermissionService cmsPermissionService;

    @ApiOperation("添加权限permission:add")
    @PostMapping()
    @PreAuthorize("hasAuthority('permission:add')")
    public CommonResult addPermission(@RequestBody @Validated CmsPermissionAdd cmsPermissionAdd) {
        System.out.println(cmsPermissionAdd);
        if (cmsPermissionService.getOne(new QueryWrapper<CmsPermission>().lambda()
                .eq(CmsPermission::getName, cmsPermissionAdd.getPermissionName())) != null) {
            return CommonResult.failed("权限名称重复");
        }
        CmsPermission cmsPermission = new CmsPermission();
        BeanUtils.copyProperties(cmsPermissionAdd, cmsPermission);
        if (cmsPermissionService.save(cmsPermission)) {
            return CommonResult.success("添加成功");
        }
        return CommonResult.failed("服务器异常");
    }

    @ApiOperation("删除权限permission:delete")
    @DeleteMapping()
    @PreAuthorize("hasAuthority('permission:delete')")
    public CommonResult deletePermission(@RequestBody @Validated CmsPermissionBanRestart permissionDelete) {
        if (cmsPermissionService.removeByIds(permissionDelete.getPermissionId())) {
            return CommonResult.success("删除成功");
        } else {
            return CommonResult.failed("权限不存在");
        }
    }

    @ApiOperation("更新权限0->目录；1->菜单；2->按钮permission:update")
    @PutMapping()
    @PreAuthorize("hasAnyAuthority('permission:update')")
    public CommonResult updatePermission(
            @Validated @RequestBody CmsPermissionUpdate cmsPermissionUpdate
    ) {
        CmsPermission cmsPermission = new CmsPermission();
        BeanUtils.copyProperties(cmsPermissionUpdate, cmsPermission);
        if (cmsPermissionService.updateById(cmsPermission)) {
            return CommonResult.success("更新成功");
        } else {
            return CommonResult.failed("更新失败");
        }
    }

    @ApiOperation("查询权限permission:list")
    @GetMapping("/list")
    @PreAuthorize("hasAuthority('permission:list')")
    public CommonResult list(
             @RequestParam(value = "pageSize", defaultValue = "5") Integer pageSize,
             @RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
             @RequestParam(value = "name", defaultValue = "") String name
    ) {
        IPage<CmsPermission> page = new Page<>(pageNum, pageSize);
        QueryWrapper<CmsPermission> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .like(CmsPermission::getPermissionName, name).orderByDesc(CmsPermission::getCreateTime);
        System.out.println(cmsPermissionService.page(page, queryWrapper).getRecords().toString());
        return CommonResult.success(cmsPermissionService.page(page, queryWrapper));
    }

    @ApiOperation("查询所有权限,平面结构permission:all")
    @GetMapping("/all")
    @PreAuthorize("hasAuthority('permission:all')")
    public CommonResult all() {
        QueryWrapper<CmsPermission> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().orderByAsc(CmsPermission::getCreateTime);
        return CommonResult.success(cmsPermissionService.list(queryWrapper));
    }

    @ApiOperation("查询所权限，树型结构permission:tree")
    @GetMapping("/tree")
    @PreAuthorize("hasAuthority('permission:tree')")
    public CommonResult tree() {
        return CommonResult.success(cmsPermissionService.tree());
    }

}
