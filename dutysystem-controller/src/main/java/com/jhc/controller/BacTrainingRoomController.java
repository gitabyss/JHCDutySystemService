package com.jhc.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.dto.BacTrainingRoomSearchDto;
import com.jhc.dto.BacTrainingRoomViewDto;
import com.jhc.service.IBacTrainingCenterService;
import com.jhc.service.IBacTrainingRoomService;
import com.jhc.utils.CommonResult;
import com.jhc.vo.PageVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 实训室  前端控制器
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@RestController
@RequestMapping("/trainingRoom")
@Api(tags = "基础 实训室")
public class BacTrainingRoomController {
    @Autowired
    private IBacTrainingRoomService iBacTrainingRoomService;

    @ApiOperation("实训室查询分页trainingRoom:trainingRoomPageData")
    @PostMapping("/trainingRoomPageData")
    @PreAuthorize("hasAuthority('trainingRoom:trainingRoomPageData')")
    public CommonResult<IPage<BacTrainingRoomViewDto>> getTrainingRoomData(@RequestBody PageVo<BacTrainingRoomSearchDto> pageVo) {
        return CommonResult.success(iBacTrainingRoomService.getTrainingRoomData(pageVo));
    }
}
