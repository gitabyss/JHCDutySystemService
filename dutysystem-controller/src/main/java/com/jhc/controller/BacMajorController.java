package com.jhc.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.dto.BacMajorSearchDto;
import com.jhc.dto.BacMajorViewDto;
import com.jhc.service.IBacMajorService;
import com.jhc.utils.CommonResult;
import com.jhc.vo.PageVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 专业档案  前端控制器
 * </p>
 *
 * @author zhuzhixu
 */
@RestController
@RequestMapping("/major")
@Api(tags = "基础 专业")
public class BacMajorController {
    @Autowired
    private IBacMajorService iBacMajorService;

    @ApiOperation("分页条件查询专业major:majorPageData")
    @PostMapping("/majorPageData")
    @PreAuthorize("hasAuthority('major:majorPageData')")
    public CommonResult<IPage<BacMajorViewDto>> getMajorPageData(@RequestBody PageVo<BacMajorSearchDto> pageVo) {
        return CommonResult.success(iBacMajorService.getMajorPageData(pageVo));
    }

    @GetMapping("/majorList")
    @ApiOperation("查询专业下拉框")
    public CommonResult getMajorList(){
        return CommonResult.success(iBacMajorService.getMajorList());
    }
}
