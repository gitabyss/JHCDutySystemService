package com.jhc.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.dto.AnalysisAllSearchDto;
import com.jhc.dto.AnalysisSearchDto;
import com.jhc.dto.BacCourseSearchDto;
import com.jhc.dto.BacCourseViewDto;
import com.jhc.service.IBacCourseService;
import com.jhc.service.IIseInspectionService;
import com.jhc.utils.CommonResult;
import com.jhc.vo.DateVo;
import com.jhc.vo.PageVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 课程信息  前端控制器
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@RestController
@RequestMapping("/course")
@Api(tags = "基础 课程")
public class BacCourseController {
    @Autowired
    private IBacCourseService iBacCourseService;

    @Autowired
    private IIseInspectionService inspectionService;

    @ApiOperation("课程分页条件查询course:coursePageData")
    @PostMapping("/coursePageData")
    @PreAuthorize("hasAuthority('course:coursePageData')")
    public CommonResult<IPage<BacCourseViewDto>> getCoursePageData(@RequestBody PageVo<BacCourseSearchDto> pageVo) {
        return CommonResult.success(iBacCourseService.getCoursePageData(pageVo));
    }

    @GetMapping("/courseList")
    @ApiOperation("查询课程下拉框列表")
    public CommonResult getCourseList(@Validated AnalysisAllSearchDto allSearchDto){
        AnalysisSearchDto searchDto = new AnalysisSearchDto();
        //查询日期
        DateVo dateVo = inspectionService.getDate(allSearchDto.getType(),allSearchDto.getDateId());
        searchDto.setStartDate(dateVo.getStartDate());
        searchDto.setEndDate(dateVo.getEndDate());
        System.out.println(searchDto);
        return CommonResult.success(iBacCourseService.getCourseList(searchDto));
    }
}
