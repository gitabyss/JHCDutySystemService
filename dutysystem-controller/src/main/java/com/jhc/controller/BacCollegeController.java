package com.jhc.controller;


import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.dto.BacCollegeSearchDto;
import com.jhc.dto.BacCollegeViewDto;
import com.jhc.dto.BacParentCollegeViewDto;
import com.jhc.entity.BacCollege;
import com.jhc.service.IBacCollegeService;
import com.jhc.utils.CommonResult;
import com.jhc.vo.PageVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 院系档案  前端控制器
 * </p>
 *
 * @author zhuzhixu
 */
@RestController
@RequestMapping("/college")
@Api(tags = "基础 院系接口")
public class BacCollegeController {
    @Autowired
    private IBacCollegeService iBacCollegeService;

    @ApiOperation("分页过滤条件查询院系信息college:collegePageData")
    @PostMapping("/collegePageData")
    @PreAuthorize("hasAuthority('college:collegePageData')")
    public CommonResult<IPage<BacCollegeViewDto>> getCollegePageData(@RequestBody PageVo<BacCollegeSearchDto> pageVo) {
        return CommonResult.success(iBacCollegeService.getCollegePageData(pageVo));
    }

    @GetMapping("/collegeList")
    @ApiOperation("获取学院下拉框")
    public CommonResult getCollegeList(){
        return CommonResult.success(iBacCollegeService.getCollegeList());
    }

    @GetMapping("/departmentList")
    @ApiOperation("获取部门下拉框")
    public CommonResult getDepartmentList(){
        return CommonResult.success(iBacCollegeService.getDepartmentList());
    }

    @GetMapping("/departmentListByCollege/{collegeNumber}")
    @ApiOperation("获取部门下拉框")
    public CommonResult getDepartmentList(@PathVariable(name = "collegeNumber") String collegeNumber){
        if (StringUtils.isNotBlank(collegeNumber)){
            return CommonResult.success(iBacCollegeService.getDepartmentList(collegeNumber));
        }
        return CommonResult.failed("参数不能为空");
    }

}
