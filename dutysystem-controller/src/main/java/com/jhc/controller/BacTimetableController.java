package com.jhc.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.dto.BacTimeTableSearchDto;
import com.jhc.dto.BacTimeTableViewDto;
import com.jhc.service.IBacTimetableService;
import com.jhc.utils.CommonResult;
import com.jhc.vo.PageVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 课表档案  前端控制器
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Api(tags = "课表档案")
@RestController
@RequestMapping("/timeTable")
public class BacTimetableController {
    @Autowired
    private IBacTimetableService iBacTimetableService;

    @ApiOperation("课表条件分页timeTable:getTimeTablePageData")
    @PostMapping("/getTimeTablePageData")
    @PreAuthorize("hasAuthority('timeTable:getTimeTablePageData')")
    public CommonResult<IPage<BacTimeTableViewDto>> getTimeTablePageData(@RequestBody PageVo<BacTimeTableSearchDto> pageVo) {
        return CommonResult.success(iBacTimetableService.getTimeTablePageData(pageVo));
    }

}
