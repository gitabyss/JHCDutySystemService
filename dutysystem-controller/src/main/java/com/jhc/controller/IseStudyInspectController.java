package com.jhc.controller;


import com.jhc.dto.*;
import com.jhc.server.WebSocketServer;
import com.jhc.service.IBacTermService;
import com.jhc.service.IIseStudyInspectService;
import com.jhc.utils.CommonResult;
import com.jhc.vo.DateVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 自习检查  前端控制器
 * </p>
 *
 * @author zfm
 * @since 2019-12-03
 */
@RestController
@RequestMapping("/iseStudyInspect")
@Api(tags = "检查 自习检查")
public class IseStudyInspectController {

    @Autowired
    private IIseStudyInspectService studyInspectService;

    @Autowired
    private IBacTermService termService;

    @GetMapping("/studyInspect")
    @ApiOperation("查询当前自习检查列表iseStudyInspect:studyInspect:get")
    @PreAuthorize("hasAuthority('iseStudyInspect:studyInspect:get')")
    public CommonResult getStudyInspect(IseStudyInspectSearchDto searchDto){
        return CommonResult.success(studyInspectService.getStudyInspect(searchDto));
    }

    @GetMapping("/studyInspectProgress")
    @ApiOperation("查询当前寝室检查进度iseStudyInspect:studyInspectProgress:get")
//    @PreAuthorize("hasAuthority('iseStudyInspect:studyInspectProgress:get')")
    public CommonResult getProgress(){
        return CommonResult.success(studyInspectService.getProgress());
    }

    @GetMapping("/finishStudy")
    @ApiOperation("查询当前用户已完成的自习检查列表iseStudyInspect:finishStudy:get")
    @PreAuthorize("hasAuthority('iseStudyInspect:finishStudy:get')")
    public CommonResult getFinishStudy(FinishStudySearchDto searchDto){
        AllStudySearchDto allStudySearchDto = new AllStudySearchDto();
        BeanUtils.copyProperties(searchDto,allStudySearchDto);
        return CommonResult.success(studyInspectService.getFinishStudy(allStudySearchDto,true));
    }

    @GetMapping("/allStudy")
    @ApiOperation("查询所有已完成的自习检查列表iseStudyInspect:allStudy:get")
    @PreAuthorize("hasAuthority('iseStudyInspect:allStudy:get')")
    public CommonResult getAllStudy(AllStudySearchDto searchDto){
        return CommonResult.success(studyInspectService.getFinishStudy(searchDto,false));
    }


    @GetMapping("/detailStudyInspect/{id}")
    @ApiOperation("查询详细自习检查列表(检查总表id)iseStudyInspect:detailStudyInspect:id:get")
    @PreAuthorize("hasAuthority('iseStudyInspect:detailStudyInspect:id:get')")
    public CommonResult getDetailStudyInspect(@PathVariable Long id){
        return CommonResult.success(studyInspectService.getDetailStudyInspect(id));
    }

    @GetMapping("/feedbackStudy")
    @ApiOperation("查询反馈列表iseStudyInspect:feedbackStudy:get")
    @PreAuthorize("hasAuthority('iseStudyInspect:feedbackStudy:get')")
    public CommonResult getFeedback7s(AllStudySearchDto searchDto){
        return CommonResult.success(studyInspectService.getFinishStudy(searchDto,null));
    }

    @PostMapping("/feedbackStudy")
    @ApiOperation("填写反馈iseStudyInspect:feedbackStudy:insert")
    @PreAuthorize("hasAuthority('iseStudyInspect:feedbackStudy:insert')")
    public CommonResult addFeedbackStudy(@RequestBody @Validated FeedBackAddDto feedBackAddDto){
        Boolean addResult = studyInspectService.addFeedbackStudy(feedBackAddDto);
        if (addResult != null && addResult == true){
            return CommonResult.success("填写成功");
        }
        return CommonResult.failed("填写失败");
    }

    @PostMapping("/iseStudyInspect")
    @ApiOperation("填写自习检查iseStudyInspect:iseStudyInspect:insert")
    @PreAuthorize("hasAuthority('iseStudyInspect:iseStudyInspect:insert')")
    public CommonResult addStudyInspect(@RequestBody @Validated IseStudyInspectAddDto addDto){
        Map<String,Object> addResult = studyInspectService.addStudyInspect(addDto);
        if (addResult!=null){
            List<BizCheckMessageDto> messageDtos = (List<BizCheckMessageDto>) addResult.get("msgList");
            if (messageDtos!=null){
                messageDtos.forEach(item -> {
                    try {
                        WebSocketServer.sendInfo(CommonResult.success(item), item.getUserNumber());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            }
            return CommonResult.success("添加成功");
        }
        return CommonResult.failed("添加失败");
    }

    @PutMapping("/iseStudyInspect")
    @ApiOperation("修改自习检查iseStudyInspect:iseStudyInspect:update")
    @PreAuthorize("hasAuthority('iseStudyInspect:iseStudyInspect:update')")
    public CommonResult updateStudyInspect(@RequestBody @Validated IseStudyInspectUpdateDto updateDto){
        Boolean updateResult = studyInspectService.updateStudyInspect(updateDto);
        if (updateResult==true){
            return CommonResult.success("修改成功");
        }
        return CommonResult.failed("修改失败");
    }

//    @GetMapping("/analysisStudy")
//    @ApiOperation("查询自习数据分析")
//    public CommonResult getAnalysisStudy(@Validated AnalysisSearchDto searchDto){
//        if (searchDto.getStartDate()==null||searchDto.getEndDate()==null){
//            DateVo dateVo = termService.getTermDate(new Date());
//            searchDto.setStartDate(dateVo.getStartDate());
//            searchDto.setEndDate(dateVo.getEndDate());
//        }
//        return CommonResult.success(studyInspectService.getAnalysis(searchDto));
//
//    }

    @GetMapping("/iseStudyCountByItemId/{itemId}")
    @ApiOperation("获取应到人数(对象id使用)iseStudyInspect:iseStudyCountByItemId:id:get")
    @PreAuthorize("hasAuthority('iseStudyInspect:iseStudyCountByItemId:id:get')")
    public CommonResult getStudyCount(@PathVariable(name = "itemId")Long id){
        return CommonResult.success(studyInspectService.getStudyCount(id));
    }

    @GetMapping("/iseStudyCountByInspectId/{inspectId}")
    @ApiOperation("获取应到人数(检查总表id使用)iseStudyInspect:iseStudyCountByInspectId:id:get")
    @PreAuthorize("hasAuthority('iseStudyInspect:iseStudyCountByInspectId:id:get')")
    public CommonResult getStudyCountByInspectId(@PathVariable(name = "inspectId")Long id){
        return CommonResult.success(studyInspectService.getStudyCountByInspectId(id));
    }


}
