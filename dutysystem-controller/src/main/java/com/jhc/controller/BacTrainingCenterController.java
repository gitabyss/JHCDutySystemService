package com.jhc.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.dto.BacTrainingCenterSearchDto;
import com.jhc.dto.BacTrainingCenterViewDto;
import com.jhc.service.IBacTrainingCenterService;
import com.jhc.utils.CommonResult;
import com.jhc.vo.PageVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 实训中心   前端控制器
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@RestController
@RequestMapping("/trainingCenter")
@Api(tags = "基础 实训中心")
public class BacTrainingCenterController {
    @Autowired
    private IBacTrainingCenterService iBacTrainingCenterService;

    @ApiOperation("实训中心查询分页trainingCenter:trainingCenterPageData")
    @PostMapping("/trainingCenterPageData")
    @PreAuthorize("hasAuthority('trainingCenter:trainingCenterPageData')")
    public CommonResult<IPage<BacTrainingCenterViewDto>> getTrainingCenterData(@RequestBody PageVo<BacTrainingCenterSearchDto> pageVo) {
        return CommonResult.success(iBacTrainingCenterService.getTrainingCenterData(pageVo));
    }

    @GetMapping("/trainingCenterList")
    @ApiOperation("实训中心下拉框")
    public CommonResult getTrainCenterList(){
        return CommonResult.success(iBacTrainingCenterService.getTrainCenterList());
    }
}
