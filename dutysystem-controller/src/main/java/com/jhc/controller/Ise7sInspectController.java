package com.jhc.controller;


import com.jhc.dto.*;
import com.jhc.server.WebSocketServer;
import com.jhc.service.IIse7sInspectService;
import com.jhc.service.IIseInspectionService;
import com.jhc.utils.CommonResult;
import com.jhc.vo.DateVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 7s检查记录表  前端控制器
 * </p>
 *
 * @author zfm
 * @since 2019-12-03
 */
@RestController
@RequestMapping("/ise7sInspect")
@Api(tags = "检查 7s检查")
public class Ise7sInspectController {

    @Autowired
    private IIse7sInspectService ise7sInspectService;

    @Autowired
    private IIseInspectionService inspectionService;

    @GetMapping("/7sInspect")
    @ApiOperation("查询当前7s检查ise7sInspect:list:my")
    @PreAuthorize("hasAuthority('ise7sInspect:list:my')")
    public CommonResult get7sInspect(Ise7sInspectSearchDto ise7sInspectSearchDto){
        return CommonResult.success(ise7sInspectService.get7sInspect(ise7sInspectSearchDto));
    }

    @GetMapping("/7sInspectProgress")
    @ApiOperation("查询当前7s检查进度ise7sInspect:progress")
//    @PreAuthorize("hasAuthority('ise7sInspect:progress')")
    public CommonResult getProgress(){
        return CommonResult.success(ise7sInspectService.getProgress());
    }

    @GetMapping("/finish7s")
    @ApiOperation("查询当前用户已完成的7s检查ise7sInspect:done")
    @PreAuthorize("hasAuthority('ise7sInspect:done')")
    public CommonResult getFinish7s(Finish7sSearchDto searchDto){
        All7sSearchDto all7sSearchDto = new All7sSearchDto();
        BeanUtils.copyProperties(searchDto,all7sSearchDto);
        return CommonResult.success(ise7sInspectService.getFinish7s(all7sSearchDto,true));
    }

    @GetMapping("/all7s")
    @ApiOperation("查询所有用户填写的7s检查列表ise7sInspect:list:all")
    @PreAuthorize("hasAuthority('ise7sInspect:list:all')")
    public CommonResult getAll7s(All7sSearchDto searchDto){
        return CommonResult.success(ise7sInspectService.getFinish7s(searchDto,false));
    }

    @GetMapping("/detail7sInspect/{id}")
    @ApiOperation("查询详细检查(检查总表id)ise7sInspect:get:id")
    @PreAuthorize("hasAuthority('ise7sInspect:get:id')")
    public CommonResult getDetail7sInspect(@PathVariable Long id){
        return CommonResult.success(ise7sInspectService.getDetail7sInspect(id));
    }

    @GetMapping("/feedback7s")
    @ApiOperation("查询反馈列表ise7sInspect:feedBack:list")
    @PreAuthorize("hasAuthority('ise7sInspect:feedBack:list')")
    public CommonResult getFeedback7s(All7sSearchDto searchDto){
        return CommonResult.success(ise7sInspectService.getFinish7s(searchDto,null));
    }

    @PutMapping("/feedback7s")
    @ApiOperation("填写反馈ise7sInspect:feedback7s:write")
    @PreAuthorize("hasAuthority('ise7sInspect:feedback7s:write')")
    public CommonResult addFeedback7s(@RequestBody @Validated FeedBackAddDto feedBackAddDto){
        Boolean addResult = ise7sInspectService.addFeedback7sInspect(feedBackAddDto);
        if (addResult == true){
            return CommonResult.success("填写成功");
        }
        return CommonResult.failed("填写失败");
    }

    @PostMapping("/ise7sInspect")
    @ApiOperation("填写7s检查ise7sInspect:write")
    @PreAuthorize("hasAuthority('ise7sInspect:write')")
    public CommonResult add7sInspect(@RequestBody @Validated Ise7sInspectAddDto addDto){
        Map<String,Object> addResult = ise7sInspectService.add7sInspect(addDto);
        if (addResult.get("success").equals(true)){
            List<BizCheckMessageDto> messageDtos = (List<BizCheckMessageDto>) addResult.get("msgList");
            if (messageDtos!=null){
                messageDtos.forEach(item -> {
                    try {
                        WebSocketServer.sendInfo(CommonResult.success(item), item.getUserNumber());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            }
            return CommonResult.success("添加成功");
        }
        return CommonResult.failed("添加失败");
    }

    @PutMapping("/ise7sUpdate")
    @ApiOperation("修改7s检查ise7sInspect:update")
    @PreAuthorize("hasAuthority('ise7sInspect:update')")
    public CommonResult update7sInspect(@RequestBody @Validated Ise7sInspectUpdateDto updateDto){
        Map<String,Object> updateResult = ise7sInspectService.update7sInspect(updateDto);
        if (updateResult.get("success").equals(true)){
            return CommonResult.success(updateResult.get("msg"),"修改成功");
        }
        return CommonResult.failed(updateResult.get("msg"),"修改失败");
    }

    @GetMapping("/analysis7s")
    @ApiOperation("查询7s检查数据分析ise7sInspect:analysis7s")
    @PreAuthorize("hasAuthority('ise7sInspect:analysis7s')")
    public CommonResult getAnalysis7s(@Validated AnalysisAllSearchDto allSearchDto){
        AnalysisSearchDto searchDto = new AnalysisSearchDto();
        //查询日期
        DateVo dateVo = inspectionService.getDate(allSearchDto.getType(),allSearchDto.getDateId());
        searchDto.setStartDate(dateVo.getStartDate());
        searchDto.setEndDate(dateVo.getEndDate());
        return CommonResult.success(ise7sInspectService.getAnalysis(searchDto));
    }


}
