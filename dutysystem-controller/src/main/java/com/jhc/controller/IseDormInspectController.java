package com.jhc.controller;


import com.jhc.dto.*;
import com.jhc.server.WebSocketServer;
import com.jhc.service.IBacTermService;
import com.jhc.service.IIseDormInspectService;
import com.jhc.utils.CommonResult;
import com.jhc.vo.DateVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 寝室检查记录表  前端控制器
 * </p>
 *
 * @author zfm
 * @since 2019-12-03
 */
@RestController
@RequestMapping("/iseDormInspect")
@Api(tags = "检查 寝室检查")
public class IseDormInspectController {

    @Autowired
    private IIseDormInspectService dormInspectService;

    @Autowired
    private IBacTermService termService;

    @GetMapping("/dormInspect")
    @ApiOperation("查询当前寝室检查iseDormInspect:dormInspect")
    @PreAuthorize("hasAuthority('iseDormInspect:dormInspect')")
    public CommonResult getDormInspect(IseDormInspectSearchDto dormInspectSearchDto){
        return CommonResult.success(dormInspectService.getDormInspect(dormInspectSearchDto));
    }

    @GetMapping("/dormInspectProgress")
    @ApiOperation("查询当前寝室检查进度iseDormInspect:dormInspectProgress")
//    @PreAuthorize("hasAuthority('iseDormInspect:dormInspectProgress')")
    public CommonResult getProgress(){
        return CommonResult.success(dormInspectService.getProgress());
    }

    @GetMapping("/finishDorm")
    @ApiOperation("查询当前用户填写过的寝室检查iseDormInspect:finishDorm:my")
    @PreAuthorize("hasAuthority('iseDormInspect:finishDorm:my')")
    public CommonResult getFinishDorm(FinishDormSearchDto searchDto){
        AllDormSearchDto allDormSearchDto = new AllDormSearchDto();
        BeanUtils.copyProperties(searchDto,allDormSearchDto);
        return CommonResult.success(dormInspectService.getFinishDorm(allDormSearchDto,true));
    }

    @GetMapping("/allDorm")
    @ApiOperation("查询所有填写过的寝室检查iseDormInspect:done:all")
    @PreAuthorize("hasAuthority('iseDormInspect:done:all')")
    public CommonResult getAllDorm(AllDormSearchDto searchDto){
        return CommonResult.success(dormInspectService.getFinishDorm(searchDto,false));
    }

    @GetMapping("/detailDormInspect/{id}")
    @ApiOperation("查询详细寝室检查信息(检查总表id)iseDormInspect:detailDormInspect:id")
    @PreAuthorize("hasAuthority('iseDormInspect:detailDormInspect:id')")
    public CommonResult getDetailDormInspect(@PathVariable Long id){
        return CommonResult.success(dormInspectService.getDetailDormInspect(id));
    }

    @GetMapping("/feedbackDorm")
    @ApiOperation("查询反馈列表iseDormInspect:feedbackDorm:list")
    @PreAuthorize("hasAuthority('iseDormInspect:feedbackDorm:list')")
    public CommonResult getFeedback7s(AllDormSearchDto searchDto){
        return CommonResult.success(dormInspectService.getFinishDorm(searchDto,null));
    }

    @PostMapping("/feedbackDorm")
    @ApiOperation("填写反馈iseDormInspect:feedbackDorm:write")
    @PreAuthorize("hasAuthority('iseDormInspect:feedbackDorm:write')")
    public CommonResult addFeedback7s(@RequestBody @Validated FeedBackAddDto feedBackAddDto){
        Boolean addResult = dormInspectService.addFeedbackDorm(feedBackAddDto);
        if (addResult != null && addResult == true){
            return CommonResult.success("填写成功");
        }
        return CommonResult.failed("填写失败");
    }

    @PostMapping("/iseDormInspect")
    @ApiOperation("填写寝室检查iseDormInspect:iseDormInspect:wtrite")
    @PreAuthorize("hasAuthority('iseDormInspect:iseDormInspect:wtrite')")
    public CommonResult addDormInspect(@RequestBody @Validated IseDormInspectAddDto addDto){
        Map<String,Object> addResult = dormInspectService.addDormInspect(addDto);
        if (addResult.get("success").equals(true)){
            List<BizCheckMessageDto> messageDtos = (List<BizCheckMessageDto>) addResult.get("msgList");
            if (messageDtos!=null){
                messageDtos.forEach(item -> {
                    try {
                        WebSocketServer.sendInfo(CommonResult.success(item), item.getUserNumber());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            }
            return CommonResult.success("添加成功");
        }
        return CommonResult.failed("添加失败");
    }

    @PutMapping("/iseDormInspect")
    @ApiOperation("修改寝室检查iseDormInspect:iseDormInspect:update")
    @PreAuthorize("hasAuthority('iseDormInspect:iseDormInspect:update')")
    public CommonResult updateDormInspect(@RequestBody @Validated IseDormInspectUpdateDto updateDto){
        Boolean updateResult = dormInspectService.updateDormInspect(updateDto);
        if (updateResult == true){
            return CommonResult.success("修改成功");
        }
        return CommonResult.failed("修改失败");
    }

//    @GetMapping("/analysisDorm")
//    @ApiOperation("查询寝室数据分析")
//    public CommonResult getAnalysisDorm(@Validated AnalysisSearchDto searchDto){
//        if (searchDto.getStartDate()==null||searchDto.getEndDate()==null){
//            DateVo dateVo = termService.getTermDate(new Date());
//            searchDto.setStartDate(dateVo.getStartDate());
//            searchDto.setEndDate(dateVo.getEndDate());
//        }
//        return CommonResult.success(dormInspectService.getAnalysis(searchDto));
//
//    }


}
