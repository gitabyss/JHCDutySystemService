package com.jhc.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.jhc.bo.UserSaveDetails;
import com.jhc.dto.*;
import com.jhc.entity.IseSchedule;
import com.jhc.service.IIseScheduleService;
import com.jhc.utils.CommonResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 值班计划   前端控制器
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@RestController
@RequestMapping("/iseSchedule")
@Api(tags = "检查 值班表接口")
public class IseScheduleController {

    @Autowired
    private UserSaveDetails userSaveDetails;

    @Autowired
    private IIseScheduleService iseScheduleService;

    /**
     * 插入值班信息
     *
     * @param scheduleAdd
     * @return
     */
    @PostMapping("/schedule")
    @ApiOperation("插入值班信息iseSchedule:schedule:insert")
    @PreAuthorize("hasAuthority('iseSchedule:schedule:insert')")
    public CommonResult addScheduleData(@RequestBody @Validated IseScheduleAdd scheduleAdd){
        if (iseScheduleService.addIseSchedule(scheduleAdd)){
            return CommonResult.success("添加成功");
        }else {
            return CommonResult.failed("已有该周的值班记录");
        }
    }

    @DeleteMapping("/schedule")
    @ApiOperation("删除值班信息iseSchedule:schedule:delete")
    @PreAuthorize("hasAuthority('iseSchedule:schedule:delete')")
    public CommonResult deleteSchedele(@Validated @RequestBody IseScheduleDelete iseScheduleDelete){
        if (iseScheduleService.removeByIds(iseScheduleDelete.getScheduleIds())){
            return CommonResult.success("删除成功");
        }else {
            return CommonResult.failed("删除失败");
        }
    }

    @PutMapping("/schedule")
    @ApiOperation("修改值班记录iseSchedule:schedule:update")
    @PreAuthorize("hasAuthority('iseSchedule:schedule:update')")
    public CommonResult updateSchedule(@Validated @RequestBody IseScheduleUpdate scheduleUpdate){
        //获取登录用户信息
        String username = userSaveDetails.getCmsUser().getNumber();
        //创建修改对象
        //查询值班信息
        IseSchedule schedule = iseScheduleService.getById(scheduleUpdate.getId());
        if (schedule==null){
            return CommonResult.failed("修改失败");
        }
        BeanUtil.copyProperties(scheduleUpdate,schedule, CopyOptions.create().setIgnoreNullValue(true));
//        BeanUtils.copyProperties(scheduleUpdate,schedule);
        schedule.setCreaterNumber(username);
        if (iseScheduleService.updateById(schedule)){
            return CommonResult.success("修改成功");
        }else {
            return CommonResult.failed("修改失败");
        }
    }


    @GetMapping("/schedule")
    @ApiOperation("查询值班信息iseSchedule:schedule:get")
    @PreAuthorize("hasAuthority('iseSchedule:schedule:get')")
    public CommonResult getSchedule(IseScheduleSearchDto scheduleSearchDto){
        return CommonResult.success(iseScheduleService.getSchedule(scheduleSearchDto));
    }

}
