package com.jhc.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 系统配置 前端控制器
 * </p>
 *
 * @author zfm
 * @since 2020-03-11
 */
@RestController
@RequestMapping("/cms-config")
public class CmsConfigController {

}
