package com.jhc.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 夜不归宿打卡记录 前端控制器
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
@RestController
@RequestMapping("/dorm-night-punch")
public class DormNightPunchController {

}
