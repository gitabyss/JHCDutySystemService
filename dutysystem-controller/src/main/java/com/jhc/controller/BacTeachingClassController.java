package com.jhc.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.dto.BacTeachingClassSearchDto;
import com.jhc.dto.BacTeachingClassViewDto;
import com.jhc.service.IBacTeachingClassService;
import com.jhc.utils.CommonResult;
import com.jhc.vo.PageVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 教学班  前端控制器
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@RestController
@RequestMapping("/teachingClass")
@Api(tags = "基础 教学班")
public class BacTeachingClassController {
    @Autowired
    private IBacTeachingClassService iBacTeachingClassService;

    @ApiOperation("分页条件查询教学班teachingClass:teachingClassPageData")
    @PostMapping("/teachingClassPageData")
    @PreAuthorize("hasAuthority('teachingClass:teachingClassPageData')")
    public CommonResult<IPage<BacTeachingClassViewDto>> getTeachingClassPageData(@RequestBody PageVo<BacTeachingClassSearchDto> pageVo) {
        return CommonResult.success(iBacTeachingClassService.getTeachingClassPageData(pageVo));
    }
}
