package com.jhc.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 夜不归宿抽查计划表 前端控制器
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
@RestController
@RequestMapping("/dorm-night-spot-plan")
public class DormNightSpotPlanController {

}
