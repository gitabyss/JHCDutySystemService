package com.jhc.controller;


import com.jhc.dto.AnalysisAllSearchDto;
import com.jhc.dto.AnalysisDateSearchDto;
import com.jhc.dto.AnalysisSearchDto;
import com.jhc.service.*;
import com.jhc.utils.CommonResult;
import com.jhc.vo.DateVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 检查汇总 填写完检查后生成一条新的记录，未填写前没有这条记录 前端控制器
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@RestController
@RequestMapping("/iseInspection")
@Api(tags = "检查 检查总表")
public class IseInspectionController {

    @Autowired
    private IIseInspectionService inspectionService;

    @Autowired
    private IIse7sInspectService ise7sInspectService;

    @Autowired
    private IIseDormInspectService dormInspectService;

    @Autowired
    private IIseStudyInspectService studyInspectService;

    @Autowired
    private IIseTeachInspectService teachInspectService;

    @Autowired
    private IBacTermService termService;

    @GetMapping("/detailInspect/{id}")
    @ApiOperation("根据检查总表id查询各检查表的详细信息iseInspection:detailInspect:id")
    @PreAuthorize("hasAuthority('iseInspection:detailInspect:id')")
    public CommonResult getDetailInspect(@PathVariable(name = "id",required = true)Long id){
        Map<String,Object> result = inspectionService.getDetailInspect(id);
        if (result != null){
            return CommonResult.success(result);
        }
        return CommonResult.failed("查询失败");
    }

    @GetMapping("/analysisAll")
    @ApiOperation("查询所有班级数据分析iseInspection:analysisAll")
    @PreAuthorize("hasAuthority('iseInspection:analysisAll')")
    public CommonResult getAnalysisAll(@Validated AnalysisAllSearchDto allSearchDto){
        AnalysisSearchDto searchDto = new AnalysisSearchDto();
        //查询日期
        DateVo dateVo = inspectionService.getDate(allSearchDto.getType(),allSearchDto.getDateId());
        searchDto.setStartDate(dateVo.getStartDate());
        searchDto.setEndDate(dateVo.getEndDate());

        return CommonResult.success(inspectionService.getAnalysis(searchDto));

    }

    @GetMapping("/analysisOne")
    @ApiOperation("查询单个班级数据分析iseInspection:analysisOne:one")
    @PreAuthorize("hasAuthority('iseInspection:analysisOne:one')")
    public CommonResult getAnalysisOne(@Validated AnalysisDateSearchDto dateSearchDto){
        AnalysisSearchDto searchDto = new AnalysisSearchDto();
        searchDto.setClassId(dateSearchDto.getClassId());
        //查询日期
        DateVo dateVo = inspectionService.getDate(dateSearchDto.getType(),dateSearchDto.getDateId());
        searchDto.setStartDate(dateVo.getStartDate());
        searchDto.setEndDate(dateVo.getEndDate());

        return CommonResult.success(inspectionService.getAnalysisOne(searchDto));

    }

    @GetMapping("/allProgress")
    @ApiOperation("查询当前全部检查进度iseInspection:allProgress")
//    @PreAuthorize("hasAuthority('iseInspection:allProgress')")
    public CommonResult getAllProgress(){
        //构造返回对象
        Map<String,Object> resultMap = new HashMap<>();
        //查询7s
        resultMap.put("ise7s",ise7sInspectService.getProgress());
        //查询寝室
        resultMap.put("iseDorm",dormInspectService.getProgress());
        //查询自习
        resultMap.put("iseStudy",studyInspectService.getProgress());
        //查询教学
        resultMap.put("IseTeach",teachInspectService.getProgress());
        return CommonResult.success(resultMap);
    }

    @GetMapping("/phoneInspect/{id}/{type}")
    @ApiOperation("手机扫码iseInspection:phoneInspect:id:type")
    @PreAuthorize("hasAuthority('iseInspection:phoneInspect:id:type')")
    public CommonResult getPhoneInspect(@PathVariable(name = "id")Long id,@PathVariable(name = "type")Integer type){
        Map<String,Object> resultMap = inspectionService.getPhoneInspect(id,type);
        return CommonResult.success(resultMap);
    }

    @GetMapping("/phoneInspect2/{id}/{type}")
    @ApiOperation("手机扫码iseInspection:phoneInspect2:id:type")
    @PreAuthorize("hasAuthority('iseInspection:phoneInspect2:id:type')")
    public CommonResult getPhoneInspect2(@PathVariable(name = "id")Long id,@PathVariable(name = "type")Integer type){
        Map<String,Object> resultMap = inspectionService.getPhoneInspect2(id,type);
        return CommonResult.success(resultMap);
    }

}
