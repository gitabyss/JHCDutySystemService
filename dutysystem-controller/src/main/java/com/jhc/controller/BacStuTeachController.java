package com.jhc.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.dto.BacStuTeachSearchDto;
import com.jhc.dto.BacStuTeachViewDto;
import com.jhc.service.IBacStuTeachService;
import com.jhc.utils.CommonResult;
import com.jhc.vo.PageVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 学生教学班关系表  前端控制器
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@RestController
@RequestMapping("/stuTeach")
@Api(tags = "基础 教学班学生")
public class BacStuTeachController {
    @Autowired
    private IBacStuTeachService iBacStuTeachService;

    @ApiOperation("获取教学班学生分页条件查询stuTeach:stuTeachPageData")
    @PostMapping("/stuTeachPageData")
    @PreAuthorize("hasAuthority('stuTeach:stuTeachPageData')")
    public CommonResult<IPage<BacStuTeachViewDto>> stuTeachPageData(@RequestBody PageVo<BacStuTeachSearchDto> pageVo) {
        return CommonResult.success(iBacStuTeachService.getStuTeachPageData(pageVo));
    }
}
