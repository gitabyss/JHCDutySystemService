package com.jhc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.entity.CmsStudent;

/**
 * <p>
 * 学生  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface ICmsStudentService extends IService<CmsStudent> {

}
