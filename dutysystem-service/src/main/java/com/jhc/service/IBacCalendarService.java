package com.jhc.service;

import com.jhc.entity.BacCalendar;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zfm
 * @since 2020-03-11
 */
public interface IBacCalendarService extends IService<BacCalendar> {

}
