package com.jhc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.dto.IseScheduleAdd;
import com.jhc.dto.IseScheduleSearchDto;
import com.jhc.dto.IseScheduleViewDto;
import com.jhc.entity.IseSchedule;
import com.jhc.vo.PageVo;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 值班计划   服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IIseScheduleService extends IService<IseSchedule> {

    /**
     * 添加值班信息
     *
     * @param iseScheduleAdd
     * @return
     */
    boolean addIseSchedule(IseScheduleAdd iseScheduleAdd);


    /**
     * 查询值班信息
     *
     * @param scheduleSearchDto
     * @return
     */
    IPage<IseScheduleViewDto> getSchedule(IseScheduleSearchDto scheduleSearchDto);

    /**
     * 判断是否值班
     *
     * @param username
     * @param nowDate
     * @return
     */
    IseSchedule isSchedule(String username,Date nowDate);



}
