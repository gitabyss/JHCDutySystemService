package com.jhc.service;

import com.jhc.entity.DormNightSpotPlan;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 夜不归宿抽查计划表 服务类
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
public interface IDormNightSpotPlanService extends IService<DormNightSpotPlan> {

}
