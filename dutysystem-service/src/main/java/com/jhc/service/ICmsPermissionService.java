package com.jhc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.dto.CmsTreePermission;
import com.jhc.entity.CmsPermission;

import java.util.List;

/**
 * <p>
 * cms_permission  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface ICmsPermissionService extends IService<CmsPermission> {

    /**
     * 查询所有权限
     * @return 树型结构
     */
    List<CmsTreePermission> tree();

}
