package com.jhc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.dto.BacDormitorySearchDto;
import com.jhc.dto.BacDormitoryViewDto;
import com.jhc.dto.CmsStudentDormitoryViewDto;
import com.jhc.entity.BacDormitory;
import com.jhc.vo.PageVo;

import java.util.List;

/**
 * <p>
 * 寝室   服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IBacDormitoryService extends IService<BacDormitory> {
    /**
     * 分页查询寝室分页数据
     * @param pageVo
     * @return
     */
    IPage<BacDormitoryViewDto> getDormitoryPageData(PageVo<BacDormitorySearchDto> pageVo);

    /**
     * 根据寝室id查询该寝室学生信息
     * @param id
     * @return
     */
    List<CmsStudentDormitoryViewDto> getStudentListByDormitoryId(Long id);
}
