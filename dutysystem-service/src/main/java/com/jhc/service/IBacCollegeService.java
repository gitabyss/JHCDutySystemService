package com.jhc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.dto.*;
import com.jhc.entity.BacCollege;
import com.jhc.vo.PageVo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 院系档案  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IBacCollegeService extends IService<BacCollege> {
    /**
     * 获取院系分页过滤条件查询数据
     * @param pageVo
     * @return
     */
    IPage<BacCollegeViewDto> getCollegePageData(PageVo<BacCollegeSearchDto> pageVo);

    /**
     *
     * 获取学院下拉框
     * @return
     */
    List<Map<String,Object>> getCollegeList();

    /**
     * 获取部门下拉框
     *
     * @return
     */
    List<Map<String,Object>> getDepartmentList();

    /**
     * 根据学院获取部门下拉框
     *
     * @param collegeNumber
     * @return
     */
    List<Map<String,Object>> getDepartmentList(String collegeNumber);

}
