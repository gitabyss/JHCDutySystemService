package com.jhc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.dto.BacTrainingCenterSearchDto;
import com.jhc.dto.BacTrainingCenterViewDto;
import com.jhc.dto.BacTrainingRoomSearchDto;
import com.jhc.dto.BacTrainingRoomViewDto;
import com.jhc.entity.BacTrainingRoom;
import com.jhc.vo.PageVo;

/**
 * <p>
 * 实训室  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IBacTrainingRoomService extends IService<BacTrainingRoom> {
    /**
     * 分页查询实训室分页数据
     * @param pageVo
     * @return
     */
    IPage<BacTrainingRoomViewDto> getTrainingRoomData(PageVo<BacTrainingRoomSearchDto> pageVo);
}
