package com.jhc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.dto.*;
import com.jhc.entity.BacClass;
import com.jhc.vo.PageVo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 行政班  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IBacClassService extends IService<BacClass> {
    /**
     * 行政班分页条件查询
     * @param pageVo
     * @return
     */
    IPage<BacClassViewDto> getClassPageData(PageVo<BacClassSearchDto> pageVo);

    /**
     * 行政班学生分页条件查询
     * @param pageVo
     * @return
     */
    IPage<BacClassStuViewDto> getClassStuPageData(PageVo<BacClassStuSearchDto> pageVo);

    /**
     * 查询班级列表下拉框
     *
     * @param searchDto
     * @return
     */
    List<Map<String,Object>> getClassByTeacherNumber(AnalysisSearchDto searchDto);
}
