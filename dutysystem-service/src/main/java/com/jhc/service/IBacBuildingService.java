package com.jhc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.entity.BacBuilding;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zfm
 * @since 2020-03-04
 */
public interface IBacBuildingService extends IService<BacBuilding> {

}
