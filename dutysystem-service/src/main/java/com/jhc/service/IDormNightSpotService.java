package com.jhc.service;

import com.jhc.entity.DormNightSpot;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 夜不归宿抽查记录 服务类
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
public interface IDormNightSpotService extends IService<DormNightSpot> {

}
