package com.jhc.service;

import com.jhc.entity.DormNightAdviserLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 夜不归宿班主任操作记录 服务类
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
public interface IDormNightAdviserLogService extends IService<DormNightAdviserLog> {

}
