package com.jhc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.dto.BacStuTeachSearchDto;
import com.jhc.dto.BacStuTeachViewDto;
import com.jhc.entity.BacStuTeach;
import com.jhc.vo.PageVo;

/**
 * <p>
 * 学生教学班关系表  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IBacStuTeachService extends IService<BacStuTeach> {
    /**
     * 分页查询教学班学生
     * @param pageVo
     * @return
     */
    IPage<BacStuTeachViewDto> getStuTeachPageData(PageVo<BacStuTeachSearchDto> pageVo);
}
