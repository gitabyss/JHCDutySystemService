package com.jhc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.dto.BizCheckMessageDto;
import com.jhc.dto.CheckNoticeAddDto;
import com.jhc.entity.CmsNotice;

import java.util.List;

/**
 * <p>
 * 业务消息表  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IBacNoticeService extends IService<CmsNotice> {

    /**
     * 发送消息
     *
     * @param checkNoticeAddDto
     * @return
     */
    List<BizCheckMessageDto> sendMessage(CheckNoticeAddDto checkNoticeAddDto);

}
