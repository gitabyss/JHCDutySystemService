package com.jhc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.dto.BacTeachingClassSearchDto;
import com.jhc.dto.BacTeachingClassViewDto;
import com.jhc.entity.BacTeachingClass;
import com.jhc.vo.PageVo;

/**
 * <p>
 * 教学班  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IBacTeachingClassService extends IService<BacTeachingClass> {
    /**
     * 分页条件查询教学班
     * @param pageVo
     * @return
     */
    IPage<BacTeachingClassViewDto> getTeachingClassPageData(PageVo<BacTeachingClassSearchDto> pageVo);
}
