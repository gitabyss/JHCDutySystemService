package com.jhc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.dto.BizRoleSearchUserByRoleDto;
import com.jhc.dto.CmsRoleUpdate;
import com.jhc.dto.CmsTreePermission;
import com.jhc.entity.CmsRole;
import com.jhc.entity.CmsUser;

import java.util.List;

/**
 * <p>
 * cms_role  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface ICmsRoleService extends IService<CmsRole> {

    /**
     * 遍历用户取消token
     *
     * @param cmsRoleUpdate 更新的角色信息
     */
    void updateRole(CmsRoleUpdate cmsRoleUpdate);

    /**
     * 根据角色ID查询权限
     *
     * @param roleId 角色ID
     * @return 权限集合
     */
    List<CmsTreePermission> getPermissionList(Long roleId);

    /**
     * 根据角色查询用户
     *
     * @param bizRoleSearchUserByRoleDto 查询DTO
     * @return 用户列表
     */
    IPage<CmsUser> getUserByRoleInfo(BizRoleSearchUserByRoleDto bizRoleSearchUserByRoleDto);
}
