package com.jhc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.dto.*;
import com.jhc.entity.BacTerm;
import com.jhc.vo.PageVo;
import com.jhc.vo.DateVo;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 学期信息  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IBacTermService extends IService<BacTerm> {
    /**
     * 通过页码和每页记录数获取分页数据
     * @param pageVo 封装分页参数
     * @return
     */
    IPage<BacTermViewDto> getTeacherPageData(PageVo<BacTermSearchDto> pageVo);

    /**
     * 添加学期信息
     *
     * @param bacTermAddDto
     * @return
     */
    Boolean addTerm(BacTermAddDto bacTermAddDto);

    /**
     * 获取学期开始结束日期
     *
     * @param date
     * @return
     */
    DateVo getTermDate(Date date);

    /**
     * 获取当前学期
     *
     * @param date
     * @return
     */
    BacTerm getTerm(Date date);

    /**
     * 获取开始结束日期
     *
     * @param termId
     * @return
     */
    DateVo getTermDate(Long termId);

    /**
     * 获取本学期及之前学期列表
     *
     * @return
     */
    List<TermIdAndNameDto> getTermList();

    /**
     * 获取所有学期列表
     *
     * @return
     */
    List<TermIdAndNameDto> getAllTermList();

    /**
     * 获取一学期本周之前列表
     *
     * @return
     */
    List<WeekIdAndNameDto> getWeekList(Long termId);

    /**
     * 获取一学期所有列表
     *
     * @return
     */
    List<WeekIdAndNameDto> getAllWeekList(Long termId);

    /**
     * 获取当前日期与周次
     *
     * @return
     */
    Map<String,Object> getNowDateInfo();
}
