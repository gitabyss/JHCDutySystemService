package com.jhc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.dto.BacTimeTableSearchDto;
import com.jhc.dto.BacTimeTableViewDto;
import com.jhc.entity.BacTimetable;
import com.jhc.vo.PageVo;

/**
 * <p>
 * 课表档案  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IBacTimetableService extends IService<BacTimetable> {
    /**
     * 查询课表分页数据
     * @param pageVo
     * @return
     */
    IPage<BacTimeTableViewDto> getTimeTablePageData(PageVo<BacTimeTableSearchDto> pageVo);
}
