package com.jhc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.dto.*;
import com.jhc.entity.CmsPermission;
import com.jhc.entity.CmsTeacher;
import com.jhc.entity.CmsUser;
import com.jhc.entity.RedisUserInfo;
import com.jhc.vo.PageVo;

import java.util.List;

/**
 * <p>
 * 教师档案  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface ICmsTeacherService extends IService<CmsTeacher> {

//    /**
//     * 获取用户所有权限
//     *
//     * @param staffNumber 用户账号
//     * @return 权限集合
//     */
//    List<CmsPermission> getPermissionList(String staffNumber);

    /**
     * 用户注册
     *
     * @param cmsRegister 注册实体类
     * @return 是否注册成功
     */
    Boolean register(CmsRegister cmsRegister);

//    /**
//     * 用户登陆
//     *
//     * @param username 用户名
//     * @param password 密码
//     * @return token
//     */
//    RedisUserInfo login(String username, String password);

//    /**
//     * 给用户添加权限
//     *
//     * @param cmsUserPermissionAdd 添加权限对象
//     * @return 是否添加成功
//     */
//    Boolean userAddPermission(CmsUserPermissionAdd cmsUserPermissionAdd);

    /**
     * 给用户添加角色
     *
     * @param cmsUserRoleAdd 添加角色对象
     * @return 是否添加成功
     */
    //Boolean userAddRole(CmsUserRoleAdd cmsUserRoleAdd);

    /**
     * 删除用户角色
     *
     * @param cmsUserRoleAdd 删除角色对象
     * @return
     */
    //Boolean userRemoveRole(CmsUserRoleAdd cmsUserRoleAdd);

    /**
     * 通过页码和每页记录数获取分页数据
     *
     * @param pageVo 封装分页参数
     * @return
     */
    IPage<CmsTeacherViewDto> getTeacherPageData(PageVo<CmsTeacherSearchDto> pageVo);

    /**
     * 模糊查询班主任列表
     *
     * @param name
     * @return
     */
    List<CmsTeacher> getTeacherListByLikeName(String name);

    /**
     * 校验老师编号列表是否存在
     *
     * @param numbers
     * @return
     */
//    Boolean isTeacherNumberList(List<String> numbers);

    /**
     * 修改老师密码
     *
     * @param cmsUpdatePasswordDto
     * @return
     */
    //boolean updatePassword(CmsUpdatePasswordDto cmsUpdatePasswordDto);

    /**
     * 更新用户信息
     *
     * @param cmsTeacherUpdate
     * @return
     */
    boolean updateInfo(CmsTeacherUpdate cmsTeacherUpdate);
}
