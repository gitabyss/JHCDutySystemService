package com.jhc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.entity.CmsOptionLog;

/**
 * <p>
 * 访问日志信息  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface ICmsOptionLogService extends IService<CmsOptionLog> {

}
