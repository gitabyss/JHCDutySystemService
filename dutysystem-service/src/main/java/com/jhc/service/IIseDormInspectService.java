package com.jhc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.dto.*;
import com.jhc.entity.IseDormInspect;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 寝室检查记录表  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-12-03
 */
public interface IIseDormInspectService extends IService<IseDormInspect> {

    /**
     * 查询当前寝室检查列表(sql方式)
     *
     * @param dormInspectSearchDto
     * @return
     */
    IPage<IseDormInspectViewDto> getDormInspect(IseDormInspectSearchDto dormInspectSearchDto);

    /**
     * 查询寝室检查进度条
     *
     * @return
     */
    Map<String,Object> getProgress();

    /**
     * 查询当前寝室检查列表(mp方式)
     *
     * @param dormInspectSearchDto
     * @return
     */
    IPage<IseDormInspectViewDto> getDormInspect2(IseDormInspectSearchDto dormInspectSearchDto);

    /**
     * 查询已填写的寝室检查列表
     *
     * @param searchDto
     * @return
     */
    IPage<FinishDormViewDto> getFinishDorm(AllDormSearchDto searchDto,Boolean type);


    /**
     * 查询寝室检查详细信息(检查总表id查询)
     *
     * @param id
     * @return
     */
    DetailDormViewDto getDetailDormInspect(Long id);

    /**
     * 获取多个教师和班级的拼接字符串
     *
     * @param teaches
     * @param classes
     * @return
     */
    List<String> getTeachAndClass(String teaches,String classes);

    /**
     * 拼接多个老师字符串
     *
     * @param teaches
     * @return
     */
    String getTeachersStr(String teaches);


    /**
     * 填写寝室反馈(检查总表id查询)
     *
     * @param feedBackAddDto
     * @return
     */
    Boolean addFeedbackDorm(FeedBackAddDto feedBackAddDto);

    /**
     * 填写寝室检查
     *
     * @param addDto
     * @return
     */
    Map<String,Object> addDormInspect(IseDormInspectAddDto addDto);

    /**
     * 修改寝室检查
     *
     * @param updateDto
     * @return
     */
    Boolean updateDormInspect(IseDormInspectUpdateDto updateDto);

    /**
     * 查询寝室数据分析
     *
     * @param searchDto
     * @return
     */
    List<AnalysisDormViewDto> getAnalysis(AnalysisSearchDto searchDto);

    /**
     * 计算一个班级寝室的分析数据
     *
     * @return
     */
    List<AnalysisDormViewDto> analysisDorm(List<Object> classIds, AnalysisSearchDto searchDto);

}
