package com.jhc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.dto.*;
import com.jhc.entity.IseStudyInspect;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 自习检查  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-12-03
 */
public interface IIseStudyInspectService extends IService<IseStudyInspect> {

    /**
     * 查询当前自习检查列表(sql方式)
     *
     * @param searchDto
     * @return
     */
    IPage<IseStudyInspectViewDto> getStudyInspect(IseStudyInspectSearchDto searchDto);

    /**
     * 查询自习检查进度条
     *
     * @return
     */
    Map<String,Object> getProgress();

    /**
     * 查询当前自习检查列表(mp方式)
     *
     * @param searchDto
     * @return
     */
    IPage<IseStudyInspectViewDto> getStudyInspect2(IseStudyInspectSearchDto searchDto);

    /**
     * 查询已填写的自习检查列表
     *
     * @param searchDto
     * @return
     */
    IPage<FinishStudyViewDto> getFinishStudy(AllStudySearchDto searchDto, Boolean type);

    /**
     * 查询详细自习检查信息(检查总表id查询)
     *
     * @param id
     * @return
     */
    DetailStudyViewDto getDetailStudyInspect(Long id);


    /**
     * 填写自习反馈(检查总表id查询)
     *
     * @param feedBackAddDto
     * @return
     */
    Boolean addFeedbackStudy(FeedBackAddDto feedBackAddDto);

    /**
     * 填写自习检查
     *
     * @param addDto
     * @return
     */
    Map<String,Object> addStudyInspect(IseStudyInspectAddDto addDto);

    /**
     * 修改仔细检查
     *
     * @param updateDto
     * @return
     */
    Boolean updateStudyInspect(IseStudyInspectUpdateDto updateDto);

    /**
     * 查询自习数据分析
     *
     * @param searchDto
     * @return
     */
    List<AnalysisStudyViewDto> getAnalysis(AnalysisSearchDto searchDto);

    /**
     * 计算自习检查的分析数据
     *
     * @param classIds
     * @param searchDto
     * @return
     */
    List<AnalysisStudyViewDto> analysisStudy(List<Object> classIds, AnalysisSearchDto searchDto);

    /**
     * 查询自习应到人数(对象id使用)
     *
     * @param id
     * @return
     */
    Integer getStudyCount(Long id);

    /**
     * 查询自习应到人数(检查总表id使用)
     *
     * @param id
     * @return
     */
    Integer getStudyCountByInspectId(Long id);

}
