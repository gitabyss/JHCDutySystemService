package com.jhc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.entity.IseStudentInspect;

/**
 * <p>
 * 学生检查表  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IIseStudentInspectService extends IService<IseStudentInspect> {

}
