package com.jhc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.dto.*;
import com.jhc.entity.BacTeachingClass;
import com.jhc.entity.IseTeachInspect;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 教学检查  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-12-03
 */
public interface IIseTeachInspectService extends IService<IseTeachInspect> {

    /**
     * 获取教学检查列表（sql方式）
     *
     * @param teaInspectSearchDto
     * @return
     */
    IPage<IseTeaInspectViewDto> getTeachInspect(IseTeaInspectSearchDto teaInspectSearchDto);

    /**
     * 查询教学检查进度条
     *
     * @return
     */
    Map<String,Object> getProgress();

    /**
     * 获取教学检查列表（mp方式）
     *
     * @param teaInspectSearchDto
     * @return
     */
    IPage<IseTeaInspectViewDto> getTeachInspect2(IseTeaInspectSearchDto teaInspectSearchDto);

    /**
     * 查询已填写的教学检查列表
     *
     * @param searchDto
     * @return
     */
    IPage<FinishTeachViewDto> getFinishTeach(AllTeachSearchDto searchDto, Boolean type);

    /**
     * 查询详细教学检查信息(检查总表id查询)
     *
     * @param id
     * @return
     */
    DetailTeachViewDto getDetailTeachInspect(Long id);

    /**
     * 填写反馈(检查总表id查询)
     *
     * @param feedBackAddDto
     * @return
     */
    Boolean addFeedbackTeach(FeedBackAddDto feedBackAddDto);

    /**
     * 填写教学检查
     *
     * @param addDto
     * @return
     */
    Map<String,Object> addTeachInspect(IseTeaInspectAddDto addDto);

    /**
     * 修改教学检查
     *
     * @param updateDto
     * @return
     */
    Boolean updateTeachInspect(IseTeaInspectUpdateDto updateDto);

    /**
     * 查询教学检查数据分析
     *
     * @param searchDto
     * @return
     */
    List<AnalysisTeachViewDto> getAnalysis(AnalysisSearchDto searchDto);

    /**
     *计算教学检查的分析数据
     *
     * @param classIds
     * @param searchDto
     * @return
     */
    List<AnalysisTeachViewDto> analysisTeach(List<Object> classIds, AnalysisSearchDto searchDto);

    /**
     * 查询反馈教学检查分析(学生)
     *
     * @param searchDto
     * @return
     */
    Map<String,Object> getAnalysisStudent(AnalysisNumberSearchDto searchDto);

    /**
     * 查询反馈教学检查分析(老师)
     *
     * @param searchDto
     * @return
     */
    Map<String,Object> getAnalysisTeacher(AnalysisNumberSearchDto searchDto);

    /**
     * 查询教学班列表
     *
     * @param searchDto
     * @return
     */
    List<BacTeachingClass> getTeachClassByAnalysisNumberSearchDto(AnalysisNumberSearchDto searchDto);

    /**
     * 计算数据分析
     *
     * @param teachingClassList
     * @param searchDto
     * @return
     */
    List<AnalysisTeachAllDto> analysisStudent(List<BacTeachingClass> teachingClassList,AnalysisNumberSearchDto searchDto);

    /**
     * 查询教学班应到人数(对象id使用)
     *
     * @param id
     * @return
     */
    Integer getTeachCount(Long id);

    /**
     * 查询教学班应到人数(检查总表id使用)
     *
     * @param id
     * @return
     */
    Integer getTeachCountByInspectId(Long id);


}
