package com.jhc.service;

import com.jhc.entity.DormNightSpotLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 学生值班计划分配表 服务类
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
public interface IDormNightSpotLogService extends IService<DormNightSpotLog> {

}
