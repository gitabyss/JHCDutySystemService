package com.jhc.service;

import com.jhc.entity.CmsConfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 系统配置 服务类
 * </p>
 *
 * @author zfm
 * @since 2020-03-11
 */
public interface ICmsConfigService extends IService<CmsConfig> {

}
