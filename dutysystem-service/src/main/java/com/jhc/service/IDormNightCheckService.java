package com.jhc.service;

import com.jhc.entity.DormNightCheck;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 学生夜不归宿打卡 服务类
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
public interface IDormNightCheckService extends IService<DormNightCheck> {

}
