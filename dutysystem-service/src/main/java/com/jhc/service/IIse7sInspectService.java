package com.jhc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.dto.*;
import com.jhc.entity.BacTrainingRoom;
import com.jhc.entity.Ise7sInspect;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 7s检查记录表  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-12-03
 */
public interface IIse7sInspectService extends IService<Ise7sInspect> {

    /**
     * 查询7s检查列表(mp方式)
     *
     * @param ise7sInspectSearchDto
     * @return
     */
    IPage<Ise7sInspectViewDto> get7sInspect(Ise7sInspectSearchDto ise7sInspectSearchDto);

    /**
     * 查询7s检查进度条
     *
     * @return
     */
    Map<String,Object> getProgress();

    /**
     * 查询填写的7s检查信息
     *
     * @param searchDto
     * @return
     */
    IPage<Finish7sViewDto> getFinish7s(All7sSearchDto searchDto,Boolean type);



    /**
     * 查询7s检查详情(检查总表id查询)
     *
     * @param id
     * @return
     */
    Detail7sViewDto getDetail7sInspect(Long id);



    /**
     * 填写7s反馈(检查总表id查询)
     *
     * @param feedBackAddDto
     * @return
     */
    Boolean addFeedback7sInspect(FeedBackAddDto feedBackAddDto);

    /**
     * 填写7s检查
     *
     * @param addDto
     * @return
     */
    Map<String,Object> add7sInspect(Ise7sInspectAddDto addDto);

    /**
     * 修改7s检查
     *
     * @param updateDto
     * @return
     */
    Map<String,Object> update7sInspect(Ise7sInspectUpdateDto updateDto);

    /**
     * 查询7s数据分析
     *
     * @param searchDto
     * @return
     */
    Map<String,Object> getAnalysis(AnalysisSearchDto searchDto);

    /**
     * 计算7s的数据分析
     *
     * @param rooms
     * @param searchDto
     * @return
     */
    List<Analysis7sViewDto> analysis7s(List<BacTrainingRoom> rooms, AnalysisSearchDto searchDto);

}
