package com.jhc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.dto.*;
import com.jhc.entity.IseInspection;
import com.jhc.vo.DateVo;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 检查汇总 填写完检查后生成一条新的记录，未填写前没有这条记录 服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IIseInspectionService extends IService<IseInspection> {

    /**
     * 查询个检查表的详细信息
     *
     * @param id
     * @return
     */
    Map<String,Object> getDetailInspect(Long id);

    /**
     * 查询已检查对象id
     *
     * @return
     */
    List<Object> getItemIds(Date nowDate, Integer type);

    /**
     * 查询检查总表记录
     *
     * @param nowDate
     * @param itemId    检查对象id
     * @param type
     * @return
     */
    List<IseInspection> getInspections(Date nowDate,Long itemId,Integer type);

    /**
     * 构造并添加检查总表记录
     *
     * @param addDto
     * @return
     */
    Map<String,Object> addInspection(InspectionCreateDto addDto,String feedbacker);

    /**
     * 查询检查总表记录（根据开始日期和结束日期）
     *
     * @param stratDate
     * @param endDate
     * @param itemIds
     * @param type
     * @return
     */
    List<IseInspection> getInspections(Date stratDate,Date endDate,List<Object> itemIds,Integer type);

    /**
     * 查询班情统揽数据分析
     *
     * @param searchDto
     * @return
     */
    List<AnalysisAllViewDto> getAnalysis(AnalysisSearchDto searchDto);

    /**
     * 查询日期
     *
     * @return
     */
    DateVo getDate(Integer type,Long dateId);

    /**
     * 查询单个班所有级数据分析
     *
     * @param searchDto
     * @return
     */
    Map<String,Object> getAnalysisOne(AnalysisSearchDto searchDto);

    /**
     * 手机扫码返回当前检查列表
     *
     * @param id
     * @param type
     * @return
     */
    Map<String,Object> getPhoneInspect(Long id,Integer type);

    /**
     * 手机扫码返回当前检查列表2
     *
     * @param id
     * @param type
     * @return
     */
    Map<String,Object> getPhoneInspect2(Long id,Integer type);

    /**
     * 手机扫码返回7s
     *
     * @param id
     * @return
     */
    Map<String,Object> getPhone7s(Long id);

    /**
     * 手机扫码返回dorm
     *
     * @param id
     * @return
     */
    Map<String,Object> getPhoneDorm(Long id);

    /**
     * 手机扫码返回study
     *
     * @param id
     * @return
     */
    Map<String,Object> getPhoneStudy(Long id);

    /**
     * 手机扫码返回teach
     *
     * @param id
     * @return
     */
    List<Map<String,Object>> getPhoneTeach(Long id);

    /**
     * 根据检查类型和房间id查询检查总表记录
     *
     * @param id
     * @param type
     * @return
     */
    List<Object> getIseInspect(Long id,Integer type);

}
