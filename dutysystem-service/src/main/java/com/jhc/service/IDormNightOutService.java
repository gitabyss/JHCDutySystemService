package com.jhc.service;

import com.jhc.entity.DormNightOut;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 夜不归宿进出公寓记录 服务类
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
public interface IDormNightOutService extends IService<DormNightOut> {

}
