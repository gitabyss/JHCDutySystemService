package com.jhc.service;

import com.jhc.dto.CmsUpdatePasswordDto;
import com.jhc.dto.CmsUserPermissionAdd;
import com.jhc.dto.CmsUserRoleAdd;
import com.jhc.entity.CmsPermission;
import com.jhc.entity.CmsTeacher;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.entity.CmsUser;
import com.jhc.entity.RedisUserInfo;

import java.util.List;

/**
 * <p>
 * 用户  服务类
 * </p>
 *
 * @author zhengyue
 * @since 2020-03-13
 */
public interface ICmsUserService extends IService<CmsUser> {

    /**
     * 获取用户所有权限
     *
     * @param number 用户账号 用户编号
     * @return 用户权限列表
     */
    public List<CmsPermission> getPermissionList(String number);

    /**
     * 用户登陆
     *
     * @param number 用户名
     * @param password 密码
     * @return token
     */
    RedisUserInfo login(String number, String password);

    /**
     * 给用户添加权限
     *
     * @param cmsUserPermissionAdd 添加权限对象
     * @return 是否添加成功
     */
    boolean userAddPermission(CmsUserPermissionAdd cmsUserPermissionAdd);

    /**
     * 给用户添加角色
     *
     * @param cmsUserRoleAdd 添加角色对象
     * @return 是否添加成功
     */
    Boolean userAddRole(CmsUserRoleAdd cmsUserRoleAdd);

    /**
     * 删除用户角色
     *
     * @param cmsUserRoleAdd 删除角色对象
     * @return
     */
    Boolean userRemoveRole(CmsUserRoleAdd cmsUserRoleAdd);

    /**
     * 密码更新
     * @param cmsUpdatePasswordDto
     * @return
     */
    boolean updatePassword(CmsUpdatePasswordDto cmsUpdatePasswordDto);

    /**
     * 校验用户编号列表是否存在
     *
     * @param numbers
     * @return
     */
    Boolean isUserNumberList(List<String> numbers);
}
