package com.jhc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.dto.BizCheckMessageDto;
import com.jhc.dto.BizNoticeSearchDto;
import com.jhc.entity.CmsNoticeObject;

/**
 * <p>
 * 业务消息对象表  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IBacNoticeObjectService extends IService<CmsNoticeObject> {

    /**
     * 查询消息列表
     *
     * @param bizNoticeSearchDto
     * @return
     */
    IPage<BizCheckMessageDto> selectByUsername(BizNoticeSearchDto bizNoticeSearchDto, String username);

}
