package com.jhc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.dto.BacMajorSearchDto;
import com.jhc.dto.BacMajorViewDto;
import com.jhc.entity.BacMajor;
import com.jhc.vo.PageVo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 专业档案  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IBacMajorService extends IService<BacMajor> {
    /**
     * 专业分页条件查询
     * @param pageVo
     * @return
     */
    IPage<BacMajorViewDto> getMajorPageData(PageVo<BacMajorSearchDto> pageVo);

    /**
     * 查询专业下拉框
     *
     * @return
     */
    List<Map<String,Object>> getMajorList();
}
