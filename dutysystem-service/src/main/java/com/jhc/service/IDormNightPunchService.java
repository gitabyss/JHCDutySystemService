package com.jhc.service;

import com.jhc.entity.DormNightPunch;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 夜不归宿打卡记录 服务类
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
public interface IDormNightPunchService extends IService<DormNightPunch> {

}
