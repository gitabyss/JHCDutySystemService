package com.jhc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.dto.AnalysisSearchDto;
import com.jhc.dto.BacCourseSearchDto;
import com.jhc.dto.BacCourseViewDto;
import com.jhc.entity.BacCourse;
import com.jhc.vo.PageVo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 课程信息  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IBacCourseService extends IService<BacCourse> {
    /**
     * 课程分页条件查询
     * @param pageVo
     * @return
     */
    IPage<BacCourseViewDto> getCoursePageData(PageVo<BacCourseSearchDto> pageVo);

    /**
     * 查询课程下拉列表
     *
     * @param searchDto
     * @return
     */
    List<Map<String,Object>> getCourseList(AnalysisSearchDto searchDto);
}
