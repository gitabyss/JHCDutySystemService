package com.jhc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.dto.BacStudySearchDto;
import com.jhc.dto.BacStudyViewDto;
import com.jhc.entity.BacStudy;
import com.jhc.vo.PageVo;

/**
 * <p>
 * 自习表  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-12-06
 */
public interface IBacStudyService extends IService<BacStudy> {
    /**
     * 获取自习分页数据
     * @param pageVo
     * @return
     */
    IPage<BacStudyViewDto> studyPageData(PageVo<BacStudySearchDto> pageVo);
}
