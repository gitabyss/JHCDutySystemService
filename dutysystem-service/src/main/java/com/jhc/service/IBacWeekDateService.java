package com.jhc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.entity.BacWeekDate;
import com.jhc.vo.DateVo;

import java.util.Date;

/**
 * @Author: zfm
 * @Date: 2019/12/1 11:01
 */
public interface IBacWeekDateService extends IService<BacWeekDate> {

    /**
     * 获取当前周次
     *
     * @param date
     * @return
     */
    BacWeekDate getWeek(Date date);

    /**
     * 获取开始结束时间
     *
     * @param weekId
     * @return
     */
    DateVo getWeekDate(Long weekId);

    /**
     * 查询日期
     *
     * @param date
     * @return
     */
    DateVo getWeekDate(Date date);
}
