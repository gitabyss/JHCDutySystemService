package com.jhc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.entity.IseInspectChild;

/**
 * <p>
 * 检查子表  服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IIseInspectChildService extends IService<IseInspectChild> {

}
