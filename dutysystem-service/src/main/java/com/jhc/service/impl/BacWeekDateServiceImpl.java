package com.jhc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.entity.BacTerm;
import com.jhc.entity.BacWeekDate;
import com.jhc.mapper.BacWeekDateMapper;
import com.jhc.service.IBacWeekDateService;
import com.jhc.utils.JHC;
import com.jhc.vo.DateVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @Author: zfm
 * @Date: 2019/12/1 11:01
 */
@Service
public class BacWeekDateServiceImpl extends ServiceImpl<BacWeekDateMapper, BacWeekDate> implements IBacWeekDateService {


    @Autowired
    private BacWeekDateMapper weekDateMapper;


    /**
     * 获取当前周次
     *
     * @param date
     * @return
     */
    @Override
    public BacWeekDate getWeek(Date date) {
        String dateStr = JHC.toDateString(date);
        BacWeekDate weekDate = weekDateMapper.selectOne(new QueryWrapper<BacWeekDate>().lambda()
                .ge(BacWeekDate::getEndDay,dateStr)
                .le(BacWeekDate::getStartDay,dateStr));
        return weekDate;
    }

    /**
     * 获取开始结束时间
     *
     * @param weekId
     * @return
     */
    @Override
    public DateVo getWeekDate(Long weekId) {
        if (weekId==null){
            return getWeekDate(new Date());
        }
        BacWeekDate weekDate = weekDateMapper.selectById(weekId);
        DateVo dateVo = new DateVo();
        dateVo.setStartDate(weekDate.getStartDay());
        dateVo.setEndDate(weekDate.getEndDay());
        return dateVo;
    }

    /**
     * 查询日期
     *
     * @param date
     * @return
     */
    @Override
    public DateVo getWeekDate(Date date) {
        String dateStr = JHC.toDateString(date);
        BacWeekDate weekDate = weekDateMapper.selectOne(new QueryWrapper<BacWeekDate>().lambda().ge(BacWeekDate::getEndDay,dateStr).le(BacWeekDate::getStartDay,dateStr));
        DateVo dateVo = new DateVo();
        dateVo.setStartDate(weekDate.getStartDay());
        dateVo.setEndDate(weekDate.getEndDay());
        return dateVo;
    }
}
