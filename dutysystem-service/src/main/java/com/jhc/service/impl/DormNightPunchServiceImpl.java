package com.jhc.service.impl;

import com.jhc.entity.DormNightPunch;
import com.jhc.mapper.DormNightPunchMapper;
import com.jhc.service.IDormNightPunchService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 夜不归宿打卡记录 服务实现类
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
@Service
public class DormNightPunchServiceImpl extends ServiceImpl<DormNightPunchMapper, DormNightPunch> implements IDormNightPunchService {

}
