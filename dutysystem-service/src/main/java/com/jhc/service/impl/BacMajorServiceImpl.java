package com.jhc.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.dto.BacCourseSearchDto;
import com.jhc.dto.BacCourseViewDto;
import com.jhc.dto.BacMajorSearchDto;
import com.jhc.dto.BacMajorViewDto;
import com.jhc.entity.BacMajor;
import com.jhc.mapper.BacCollegeMapper;
import com.jhc.mapper.BacMajorMapper;
import com.jhc.service.IBacMajorService;
import com.jhc.vo.PageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 专业档案  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class BacMajorServiceImpl extends ServiceImpl<BacMajorMapper, BacMajor> implements IBacMajorService {
    @Autowired
    private BacMajorMapper bacMajorMapper;
    /**
     * 专业分页条件查询
     * @param pageVo
     * @return
     */
    @Override
    public IPage<BacMajorViewDto> getMajorPageData(PageVo<BacMajorSearchDto> pageVo) {
        if (ObjectUtil.isNull(pageVo.getFilter())) {
            pageVo.setFilter(new BacMajorSearchDto());
        }

        //构造分页参数
        BacMajorSearchDto searchDto = pageVo.getFilter();
        Page page = new Page(pageVo.getCurrentPage(), pageVo.getPageSize());
        //查询院系分页数据
        IPage<BacMajorViewDto> viewDtoIPage = bacMajorMapper.getMajorPageData(page, searchDto);
        return viewDtoIPage;

    }

    /**
     * 查询专业下拉框
     *
     * @return
     */
    @Override
    public List<Map<String, Object>> getMajorList() {
        //构建返回对象
        List<Map<String,Object>> resultList = new ArrayList<>();
        //查询专业列表
        List<BacMajor> majorList = bacMajorMapper.selectList(null);
        majorList.stream().forEach(major -> {
            Map<String,Object> map = new HashMap<>();
            map.put("majorNumber",major.getNumber());
            map.put("majorName",major.getProfessionalName());
            resultList.add(map);
        });
        return resultList;
    }


}
