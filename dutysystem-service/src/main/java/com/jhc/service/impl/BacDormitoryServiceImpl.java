package com.jhc.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.dto.BacDormitorySearchDto;
import com.jhc.dto.BacDormitoryViewDto;
import com.jhc.dto.CmsStudentDormitoryViewDto;
import com.jhc.entity.BacDormitory;
import com.jhc.mapper.BacDormitoryMapper;
import com.jhc.service.IBacDormitoryService;
import com.jhc.vo.PageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 寝室   服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class BacDormitoryServiceImpl extends ServiceImpl<BacDormitoryMapper, BacDormitory> implements IBacDormitoryService {
    @Autowired
    private BacDormitoryMapper bacDormitoryMapper;
    /**
     * 分页查询寝室分页数据
     * @param pageVo
     * @return
     */
    @Override
    public IPage<BacDormitoryViewDto> getDormitoryPageData(PageVo<BacDormitorySearchDto> pageVo) {
        if (ObjectUtil.isNull(pageVo.getFilter())) {
            pageVo.setFilter(new BacDormitorySearchDto());
        }
        //构造分页参数
        BacDormitorySearchDto searchDto = pageVo.getFilter();
        Page page = new Page(pageVo.getCurrentPage(),pageVo.getPageSize());
        //查询院系分页数据
        IPage<BacDormitoryViewDto> viewDtoIPage = bacDormitoryMapper.getDormitoryPageData(page,searchDto);
        return viewDtoIPage;

    }

    /**
     * 根据寝室id查询该寝室学生信息
     * @param id
     * @return
     */
    @Override
    public List<CmsStudentDormitoryViewDto> getStudentListByDormitoryId(Long id) {
        List<CmsStudentDormitoryViewDto> list = new ArrayList<>(6);
        list.addAll(bacDormitoryMapper.getStudentListByDormitoryId(id));
        return list;
    }
}
