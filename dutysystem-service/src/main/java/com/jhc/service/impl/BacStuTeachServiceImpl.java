package com.jhc.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.dto.BacStuTeachViewDto;
import com.jhc.dto.BacStuTeachSearchDto;
import com.jhc.dto.BacTeachingClassSearchDto;
import com.jhc.dto.BacTeachingClassViewDto;
import com.jhc.entity.BacStuTeach;
import com.jhc.mapper.BacStuTeachMapper;
import com.jhc.service.IBacStuTeachService;
import com.jhc.vo.PageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 学生教学班关系表  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class BacStuTeachServiceImpl extends ServiceImpl<BacStuTeachMapper, BacStuTeach> implements IBacStuTeachService {
    @Autowired
    private BacStuTeachMapper bacStuTeachMapper;

    /**
     * 分页查询教学班学生
     * @param pageVo
     * @return
     */
    @Override
    public IPage<BacStuTeachViewDto> getStuTeachPageData(PageVo<BacStuTeachSearchDto> pageVo) {
        //防止传入filter数据为null
        if (ObjectUtil.isNull(pageVo.getFilter())) {
            pageVo.setFilter(new BacStuTeachSearchDto());
        }

        //构造分页参数
        BacStuTeachSearchDto searchDto = pageVo.getFilter();
        Page page = new Page(pageVo.getCurrentPage(), pageVo.getPageSize());
        //查询院系分页数据
        IPage<BacStuTeachViewDto> viewDtoIPage = bacStuTeachMapper.getStuTeachPageData(page, searchDto);
        return viewDtoIPage;
    }
}
