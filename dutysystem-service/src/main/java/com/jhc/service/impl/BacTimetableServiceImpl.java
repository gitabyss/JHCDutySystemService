package com.jhc.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.dto.BacCollegeSearchDto;
import com.jhc.dto.BacCollegeViewDto;
import com.jhc.dto.BacTimeTableSearchDto;
import com.jhc.dto.BacTimeTableViewDto;
import com.jhc.entity.BacTimetable;
import com.jhc.mapper.BacTimetableMapper;
import com.jhc.service.IBacTimetableService;
import com.jhc.vo.PageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课表档案  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class BacTimetableServiceImpl extends ServiceImpl<BacTimetableMapper, BacTimetable> implements IBacTimetableService {
    @Autowired
    private BacTimetableMapper bacTimetableMapper;
    /**
     * 查询课表分页数据
     * @param pageVo
     * @return
     */
    @Override
    public IPage<BacTimeTableViewDto> getTimeTablePageData(PageVo<BacTimeTableSearchDto> pageVo) {
        if (ObjectUtil.isNull(pageVo.getFilter())) {
            pageVo.setFilter(new BacTimeTableSearchDto());
        }

        //构造分页参数
        BacTimeTableSearchDto searchDto = pageVo.getFilter();
        Page page = new Page(pageVo.getCurrentPage(),pageVo.getPageSize());
        //查询院系分页数据
        IPage<BacTimeTableViewDto> viewDtoIPage = bacTimetableMapper.getTimeTableDataPage(page,searchDto);
        return viewDtoIPage;
    }
}
