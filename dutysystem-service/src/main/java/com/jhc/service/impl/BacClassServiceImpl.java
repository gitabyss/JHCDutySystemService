package com.jhc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.bo.UserSaveDetails;
import com.jhc.dto.*;
import com.jhc.entity.*;
import com.jhc.mapper.*;
import com.jhc.service.IBacClassService;
import com.jhc.vo.PageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * <p>
 * 行政班  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class BacClassServiceImpl extends ServiceImpl<BacClassMapper, BacClass> implements IBacClassService {
    @Autowired
    private BacClassMapper bacClassMapper;
    @Autowired
    private CmsTeacherMapper cmsTeacherMapper;
    @Autowired
    private BacCollegeMapper bacCollegeMapper;
    @Autowired
    private BacMajorMapper bacMajorMapper;
    @Autowired
    private CmsStudentMapper cmsStudentMapper;

    @Autowired
    private UserSaveDetails userSaveDetails;
    /**
     * 行政班分页条件查询
     * @param pageVo
     * @return
     */
    @Override
    public IPage<BacClassViewDto> getClassPageData(PageVo<BacClassSearchDto> pageVo) {
        //设置分页插件
        Page page = new Page(pageVo.getCurrentPage(),pageVo.getPageSize());
        //查询出分页数据
        BacClassSearchDto searchDto = pageVo.getFilter();
        if (searchDto==null){
            searchDto = new BacClassSearchDto();
        }
        IPage<BacClassViewDto> list = bacClassMapper.selectByBacClassSearchDto(page,searchDto);
        return list;
    }


    /**
     * 行政班学生分页条件查询
     * @param pageVo
     * @return
     */
    @Override
    public IPage<BacClassStuViewDto> getClassStuPageData(PageVo<BacClassStuSearchDto> pageVo) {
        //设置分页插件
        Page<CmsStudent> page = new Page<>();
        page.setCurrent(pageVo.getCurrentPage());
        page.setSize(pageVo.getPageSize());
        //查询出分页数据
        if (pageVo.getFilter()==null){
            pageVo.setFilter(new BacClassStuSearchDto());
        }
        IPage<BacClassStuViewDto> list = bacClassMapper.selectByBacClassStuViewDto(page,pageVo.getFilter());
        return list;
    }

    /**
     * 查询班级列表
     *
     * @param searchDto
     * @return
     */
    @Override
    public List<Map<String, Object>> getClassByTeacherNumber(AnalysisSearchDto searchDto) {
        CmsUser teacher = userSaveDetails.getCmsUser();
        List<BacClass> classes = bacClassMapper.selectList(new QueryWrapper<BacClass>().lambda()
                .eq(BacClass::getTeacherNumber,teacher.getNumber())
                .eq(BacClass::getIsEnabled,true)
                .ge(BacClass::getEndDate,searchDto.getEndDate())
                .le(BacClass::getStartDate,searchDto.getStartDate()));
        List<Map<String,Object>> resultList = new ArrayList<>();
        classes.stream().forEach(item -> {
            Map<String,Object> map = new HashMap<>();
            map.put("classId",item.getId());
            map.put("className",item.getName());
            resultList.add(map);
        });
        return resultList;
    }
}
