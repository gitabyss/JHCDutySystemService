package com.jhc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.entity.BacBuilding;
import com.jhc.mapper.BacBuildingMapper;
import com.jhc.service.IBacBuildingService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2020-03-04
 */
@Service
public class BacBuildingServiceImpl extends ServiceImpl<BacBuildingMapper, BacBuilding> implements IBacBuildingService {

}
