package com.jhc.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.bo.UserSaveDetails;
import com.jhc.dto.*;
import com.jhc.entity.*;
import com.jhc.mapper.*;
import com.jhc.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.utils.CheckType;
import com.jhc.utils.JHC;
import com.jhc.vo.DateVo;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 教学检查  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-12-03
 */
@Service
public class IseTeachInspectServiceImpl extends ServiceImpl<IseTeachInspectMapper, IseTeachInspect> implements IIseTeachInspectService {

    @Autowired
    private IseTeachInspectMapper teachInspectMapper;

    @Autowired
    private IseScheduleMapper scheduleMapper;

    @Autowired
    private UserSaveDetails userSaveDetails;

    @Autowired
    private IseInspectionMapper iseInspectionMapper;

    @Autowired
    private CmsTeacherMapper teacherMapper;

    @Autowired
    private BacTeachingClassMapper teachingClassMapper;

    @Autowired
    private BacTrainingRoomMapper roomMapper;

    @Autowired
    private BacTimetableMapper timetableMapper;

    @Autowired
    private IBacTermService termService;

    @Autowired
    private IIseDormInspectService dormInspectService;

    @Autowired
    private IseImagesMapper imagesMapper;

    @Autowired
    private IIseScheduleService scheduleService;

    @Autowired
    private IIseInspectionService inspectionService;

    @Autowired
    private ICmsTeacherService teacherService;

    @Autowired
    private BacClassMapper classMapper;

    @Autowired
    private CmsStudentMapper studentMapper;

    @Autowired
    private BacStuTeachMapper stuTeachMapper;

    @Autowired
    private BacTermMapper termMapper;

    @Autowired
    private CmsUserMapper cmsUserMapper;

    @Autowired
    private ICmsUserService cmsUserService;

    /**
     * 获取教学检查列表
     *
     * @param searchDto
     * @return
     */
    @Override
    public IPage<IseTeaInspectViewDto> getTeachInspect(IseTeaInspectSearchDto searchDto) {
//        //获取日期，去掉时间
//        Date date = JHC.removeTime(teaInspectSearchDto.getClassDate());
//        teaInspectSearchDto.setClassDate(date);
        //获取当前日期
        Date nowDate = JHC.removeTime(new Date());
        //查询值班人员
        String username = userSaveDetails.getCmsUser().getNumber();
        IseSchedule schedule = scheduleService.isSchedule(username,nowDate);
        //若不值班就返回null
        if (schedule == null){
            return new Page<>(searchDto.getCurrentPage(),searchDto.getPageSize());
        }
        System.out.println(schedule);
        //查询教学列表和已检查id
        //查询教学列表已检查对象id
        List<Object> itemCheckIds = inspectionService.getItemIds(nowDate, CheckType.ISETEACH.getCode());
        if (itemCheckIds.size()==0&&searchDto.getIsWrited()!=null&&searchDto.getIsWrited()){
            return new Page<>(searchDto.getCurrentPage(),searchDto.getPageSize());
        }
        //分页查询教学列表
        Page page = new Page(searchDto.getCurrentPage(),searchDto.getPageSize());
        IPage<IseTeaInspectViewDto> teaDtoPage = teachInspectMapper.selectByIseTeaInspectSearchDto(page,searchDto,itemCheckIds);
        //标记是否已填写
        teaDtoPage.getRecords().stream().forEach(item ->{
            if (itemCheckIds.size()!=0&&itemCheckIds.contains(item.getId())){
                item.setIsWrited(true);
                IseInspection inspection = iseInspectionMapper.selectOne(new QueryWrapper<IseInspection>().lambda()
                        .eq(IseInspection::getItemCheckId,item.getId())
                        .eq(IseInspection::getDutyDate,JHC.toDateString(nowDate))
                        .eq(IseInspection::getCheckType,CheckType.ISETEACH.getCode()));
                item.setInspectId(inspection.getId());
            }
        });
        System.out.println(teaDtoPage.getRecords());
        return teaDtoPage;
    }

    /**
     * 查询教学检查进度条
     *
     * @return
     */
    @Override
    public Map<String, Object> getProgress() {
        //获取当前日期
        Date nowDate = JHC.removeTime(new Date());
        //查询值班人员
        String username = userSaveDetails.getCmsUser().getNumber();
        IseSchedule schedule = scheduleService.isSchedule(username,nowDate);
        //若不值班就返回null
        if (schedule == null){
            return null;
        }
        //查询课表数据条数
        Integer allTeachInspect = timetableMapper.selectCount(new QueryWrapper<BacTimetable>().lambda()
                .eq(BacTimetable::getClassDate,JHC.toDateString(new Date())));
        //查询检查总表数据条数
        Integer finishTeachInspect = iseInspectionMapper.selectCount(new QueryWrapper<IseInspection>().lambda()
                .eq(IseInspection::getDutyDate,JHC.toDateString(new Date()))
                .eq(IseInspection::getCheckType,CheckType.ISETEACH.getCode()));
        Map<String,Object> resultMap = new HashMap<>();
        resultMap.put("allProgress",allTeachInspect);
        resultMap.put("finishProgress",finishTeachInspect);
        return resultMap;
    }

    /**
     * 获取教学检查列表(mp方式)
     *
     * @param searchDto
     * @return
     */
    @Override
    public IPage<IseTeaInspectViewDto> getTeachInspect2(IseTeaInspectSearchDto searchDto) {
//        //获取日期，去掉时间
//        Date date = JHC.removeTime(teaInspectSearchDto.getClassDate());
//        teaInspectSearchDto.setClassDate(date);
        //获取当前日期
        Date nowDate = JHC.removeTime(new Date());
        //查询值班人员
        IseSchedule schedule = scheduleMapper.selectByDate(JHC.toDateString(nowDate));
        String username = userSaveDetails.getCmsUser().getNumber();
        System.out.println(schedule);
        //若不值班就返回null
        if (schedule==null || !username.equals(schedule.getDutyOneNumber()) && !username.equals(schedule.getDutyTwoNumber())){
            return new Page<>(searchDto.getCurrentPage(),searchDto.getPageSize());
        }
        //查询教学列表已检查id
        List<Object> itemCheckIds = iseInspectionMapper.selectObjs(
                new QueryWrapper<IseInspection>().lambda().select(IseInspection::getItemCheckId)
                        .eq(IseInspection::getDutyDate,JHC.toDateString(nowDate)).eq(IseInspection::getCheckType,40)
        );
        itemCheckIds.add(0);
        Page page = new Page(searchDto.getCurrentPage(),searchDto.getPageSize());
//        IPage<IseTeaInspectViewDto> teaDtoPage = teachInspectMapper.selectByIseTeaInspectSearchDto(page,searchDto,itemCheckIds);
        //获取分条件查询的先置条件
        //模糊查询老师编号列表
        System.out.println(searchDto);
        List<Object> teachList = cmsUserMapper.selectObjs(new QueryWrapper<CmsUser>().lambda().select(CmsUser::getNumber)
                .like(searchDto.getTeacherName()!=null, CmsUser::getName,searchDto.getTeacherName()));
        System.out.println(teachList);
        teachList.add(String.valueOf(0));
        //模糊查询教学班id列表
        List<Object> teaClassList = teachingClassMapper.selectObjs(new QueryWrapper<BacTeachingClass>().lambda()
                .select(BacTeachingClass::getId)
                .like(searchDto.getTeachClassName()!=null,BacTeachingClass::getTeachingClassName,searchDto.getTeachClassName())
                .in(searchDto.getTeacherName()!=null,BacTeachingClass::getTeacherNumber,teachList));
        System.out.println(teaClassList);
        teaClassList.add(0);
        //模糊查询教室id列表
        List<Object> roomList = roomMapper.selectObjs(new QueryWrapper<BacTrainingRoom>().lambda()
                .select(BacTrainingRoom::getId)
                .like(searchDto.getRoomName()!=null,BacTrainingRoom::getRoomName,searchDto.getRoomName()));
        roomList.add(0);
        Boolean iswrited = searchDto.getIsWrited();
        IPage<BacTimetable> teaDtoPage = timetableMapper.selectPage(page,new QueryWrapper<BacTimetable>().lambda()
                .in(iswrited!=null&&iswrited,BacTimetable::getId,itemCheckIds)
                .notIn(iswrited!=null&&!iswrited,BacTimetable::getId,itemCheckIds)
                .in(searchDto.getRoomName()!=null,BacTimetable::getRoomId,roomList)
                .in(BacTimetable::getTeachingClassId,teaClassList)
                .like(searchDto.getCourseTitle()!=null,BacTimetable::getCourseTitle,searchDto.getCourseTitle())
                .like(searchDto.getSection()!=null,BacTimetable::getSection,searchDto.getSection())
                .eq(BacTimetable::getClassDate,JHC.toDateString(nowDate)));
        //标记是否已填写,拼接字段
        List<IseTeaInspectViewDto> viewDtos = new ArrayList<>();
        teaDtoPage.getRecords().stream().forEach(item ->{
            IseTeaInspectViewDto viewDto = new IseTeaInspectViewDto();
            BeanUtils.copyProperties(item,viewDto);
            if (itemCheckIds.contains(item.getId())){
                viewDto.setIsWrited(true);
            }
            viewDto.setRoomName(roomMapper.selectOne(new QueryWrapper<BacTrainingRoom>().lambda()
                    .eq(BacTrainingRoom::getId,item.getRoomId())).getRoomName());
            BacTeachingClass teachingClass = teachingClassMapper.selectOne(new QueryWrapper<BacTeachingClass>().lambda()
                    .eq(BacTeachingClass::getId,item.getTeachingClassId()));
            viewDto.setTeachClassName(teachingClass.getTeachingClassName());
            viewDto.setTeacherName(cmsUserMapper.selectOne(new QueryWrapper<CmsUser>().lambda()
                    .eq(CmsUser::getNumber,teachingClass.getTeacherNumber())).getName());
            viewDtos.add(viewDto);
        });
        IPage<IseTeaInspectViewDto> viewDtoIPage = new Page<>();
        BeanUtils.copyProperties(teaDtoPage,viewDtoIPage);
        viewDtoIPage.setRecords(viewDtos);
        return viewDtoIPage;
    }

    /**
     * 查询已填写教学检查列表
     *
     * @param searchDto
     * @param type
     * @return
     */
    @Override
    public IPage<FinishTeachViewDto> getFinishTeach(AllTeachSearchDto searchDto, Boolean type) {
        //获取当前用户名
        String username = null;
        String feedback = null;
        if (type!=null&&type){
            username = userSaveDetails.getCmsUser().getNumber();
        }
        if (type == null){
            feedback = userSaveDetails.getCmsUser().getNumber();
        }
        //获取当前学期开始和结束时间
        Date date = new Date();
        DateVo dateVo = termService.getTermDate(date);
        if (searchDto.getStartDate()==null){
            searchDto.setStartDate(dateVo.getStartDate());
        }
        if (searchDto.getEndDate()==null){
            searchDto.setEndDate(dateVo.getEndDate());
        }
        //查询默认当前学期信息
        Page page = new Page(searchDto.getCurrentPage(),searchDto.getPageSize());
        IPage<FinishTeachViewDto> viewDtoIPage = teachInspectMapper.selectByFinishTeachSearchDto(page,searchDto,username,feedback);
        return viewDtoIPage;
    }

    /**
     * 查询详细教学检查信息(检查总表id查询)
     *
     * @param id
     * @return
     */
    @Override
    public DetailTeachViewDto getDetailTeachInspect(Long id) {
        DetailTeachViewDto viewDto = new DetailTeachViewDto();
        //查询教学检查列表
        IseTeachInspect teachInspect = teachInspectMapper.selectOne(new QueryWrapper<IseTeachInspect>().lambda()
                .eq(IseTeachInspect::getInspectId,id));
        //查询检查总表
        IseInspection inspection = iseInspectionMapper.selectById(id);
        BeanUtils.copyProperties(teachInspect,viewDto);
        BeanUtils.copyProperties(inspection,viewDto);
        //查询课程表
        BacTimetable timetable = timetableMapper.selectById(inspection.getItemCheckId());
        BeanUtils.copyProperties(timetable,viewDto);
        //查询教学班
        BacTeachingClass teachingClass = teachingClassMapper.selectById(timetable.getTeachingClassId());
        viewDto.setTeachClassName(teachingClass.getTeachingClassName());
        //查询老师表，拼接授课老师姓名
        viewDto.setTeacherName(cmsUserMapper.selectOne(new QueryWrapper<CmsUser>().lambda().eq(CmsUser::getNumber,teachingClass.getTeacherNumber())).getName());
        viewDto.setRoomName(roomMapper.selectById(timetable.getRoomId()).getRoomName());
        viewDto.setDutyNumber(inspection.getDutyTeacher());
        //查询老师表，拼接值班老师名字属性
        viewDto.setDutyName(cmsUserMapper.selectOne(new QueryWrapper<CmsUser>().lambda().eq(CmsUser::getNumber,inspection.getDutyTeacher())).getName());
        //拼接多个老师字符串
        viewDto.setTeachersNumbers(dormInspectService.getTeachersStr(inspection.getTeachersNumbers()));
        viewDto.setId(teachInspect.getId());
        //查询图片
        IseImages images = imagesMapper.selectOne(new QueryWrapper<IseImages>().lambda().eq(IseImages::getInspectId,inspection.getId()));
        if (images!=null){
            viewDto.setImage(images.getAddress());
        }else {
            viewDto.setImage(null);
        }
        return viewDto;
    }

    /**
     * 填写反馈(检查总表id查询)
     *
     * @param feedBackAddDto
     * @return
     */
    @Override
    public Boolean addFeedbackTeach(FeedBackAddDto feedBackAddDto) {
//        IseTeachInspect teachInspect = teachInspectMapper.selectById(feedBackAddDto.getId());
//        if (teachInspect == null){
//            return false;
//        }
        IseInspection inspection = iseInspectionMapper.selectById(feedBackAddDto.getId());
        if (inspection == null||inspection.getFeedbackStatus()==10){
            return false;
        }
        //获取当前登录用户编号
        String username = userSaveDetails.getCmsUser().getNumber();
        //获取责任人用户编号
        String feedbacker = timetableMapper.selectById(inspection.getItemCheckId()).getTeacherNumber();
        if (!username.equals(feedbacker)){
            return false;
        }

        inspection.setFeedbackStatus(20);
        inspection.setResult(feedBackAddDto.getResult());
        iseInspectionMapper.updateById(inspection);
        return true;
    }

    /**
     * 填写教学检查
     *
     * @param addDto
     * @return
     */
    @Override
    public Map<String,Object> addTeachInspect(IseTeaInspectAddDto addDto) {
        //获取当前日期
        Date nowDate = JHC.removeTime(new Date());
        //查询值班人员
        String username = userSaveDetails.getCmsUser().getNumber();
        IseSchedule schedule = scheduleService.isSchedule(username,nowDate);
        //若不值班就返回false
        if (schedule == null){
            return null;
        }
        //查询检查对象列表，查看有无记录
        BacTimetable timetable = timetableMapper.selectById(addDto.getId());
        if (timetable==null){
            return null;
        }
        //查询检查总表，查看有无记录
        List<IseInspection> inspectionList = inspectionService.getInspections(nowDate,addDto.getId(),CheckType.ISETEACH.getCode());
        if (inspectionList.size()>0){
            return null;
        }
//        //遍历相关教师，校验数据
//        AtomicBoolean flag = new AtomicBoolean(true);
//        addDto.getTeacherNumbers().stream().forEach(item ->{
//            CmsUser teacher = teacherMapper.selectOne(new QueryWrapper<CmsUser>().lambda().eq(CmsUser::getNumber,item));
//            if (teacher==null){
//                flag.set(false);
//                return;
//            }
//        });
//        if (flag.equals(false)){
//            return false;
//        }
        //去重
        //遍历相关教师，校验数据
        addDto.setTeacherNumbers(addDto.getTeacherNumbers().stream().distinct().collect(Collectors.toList()));
        if (!cmsUserService.isUserNumberList(addDto.getTeacherNumbers())){
            return null;
        }
        //构造检查总表插入对象
        InspectionCreateDto createDto = new InspectionCreateDto(schedule.getWeek(),nowDate,username,CheckType.ISETEACH.getCode());
        BeanUtils.copyProperties(addDto,createDto);
        Map<String,Object> resultMap = inspectionService.addInspection(createDto,timetable.getTeacherNumber());
        IseInspection inspection = (IseInspection) resultMap.get("inspection");
        //构造教学检查表插入对象
        IseTeachInspect teachInspect = new IseTeachInspect();
        BeanUtils.copyProperties(addDto,teachInspect);
        teachInspect.setId(null);
        teachInspect.setImage(null);
        teachInspect.setInspectId(inspection.getId());
        teachInspectMapper.insert(teachInspect);
        //构造图片插入对象
        if (addDto.getImage()!=null&& StringUtils.isNotBlank(addDto.getImage())){
            IseImages iseImages = new IseImages();
            iseImages.setAddress(addDto.getImage());
            iseImages.setInspectId(inspection.getId());
            imagesMapper.insert(iseImages);
        }
        Map<String,Object> trueAndMsg = new HashMap<>();
        trueAndMsg.put("msgList",resultMap.get("msgList"));
        return trueAndMsg;
    }

    /**
     * 修改教学检查
     *
     * @param updateDto
     * @return
     */
    @Override
    public Boolean updateTeachInspect(IseTeaInspectUpdateDto updateDto) {
        //获取当前日期
        Date nowDate = JHC.removeTime(new Date());
        //查询值班人员
        String username = userSaveDetails.getCmsUser().getNumber();
        IseSchedule schedule = scheduleService.isSchedule(username,nowDate);
        //若不值班就返回false
        if (schedule == null){
            return false;
        }
        //查询检查总表，查看是否有记录
        IseInspection iseInspection = iseInspectionMapper.selectById(updateDto.getInspectId());
        if (iseInspection == null||!iseInspection.getDutyDate().equals(nowDate)){
            return false;
        }
        //遍历相关教师，校验数据
        updateDto.setTeacherNumbers(updateDto.getTeacherNumbers().stream().distinct().collect(Collectors.toList()));
        if (!cmsUserService.isUserNumberList(updateDto.getTeacherNumbers())){
            return false;
        }
        //封装对象数据，更新数据
        BeanUtil.copyProperties(updateDto,iseInspection, CopyOptions.create().setIgnoreNullValue(true));
        iseInspection.setTeachersNumbers(JSON.toJSONString(updateDto.getTeacherNumbers()));
        iseInspection.setDutyTeacher(username);
        iseInspection.setUpdateTime(null);
        iseInspectionMapper.updateById(iseInspection);
        //查询寝室检查表
        IseTeachInspect teachInspect = teachInspectMapper.selectOne(new QueryWrapper<IseTeachInspect>().lambda()
                .eq(IseTeachInspect::getInspectId,updateDto.getInspectId()));
        if (teachInspect==null){
            return false;
        }
        BeanUtil.copyProperties(updateDto,teachInspect,CopyOptions.create().setIgnoreNullValue(true));
        teachInspect.setUpdateTime(null);
        teachInspectMapper.updateById(teachInspect);
        //查询检查图片
        if (updateDto.getImageAddress()==null||!StringUtils.isNotBlank(updateDto.getImageAddress())){
            return true;
        }
        IseImages images = imagesMapper.selectOne(new QueryWrapper<IseImages>().lambda()
                .eq(IseImages::getInspectId,iseInspection.getId()));
        if (images==null){
            images = new IseImages();
            images.setAddress(updateDto.getImageAddress());
            images.setInspectId(iseInspection.getId());
            imagesMapper.insert(images);
            return true;
        }
        images.setAddress(updateDto.getImageAddress());
        images.setUpdateTime(null);
        imagesMapper.updateById(images);
        return true;
    }

    /**
     * 查询教学检查数据分析
     *
     * @param searchDto
     * @return
     */
    @Override
    public List<AnalysisTeachViewDto> getAnalysis(AnalysisSearchDto searchDto) {
        CmsUser teacher = userSaveDetails.getCmsUser();
        //查询班主任所在班级id
        List<Object> classIds = classMapper.selectObjs(new QueryWrapper<BacClass>().lambda()
                .select(BacClass::getId)
                .eq(BacClass::getTeacherNumber,teacher.getNumber()));
        if (classIds.size()==0){
            return new ArrayList<>();
        }
        //判断查询条件classId
        if (searchDto.getClassId()!=null){
            if (!classIds.contains(searchDto.getClassId())){
                return new ArrayList<>();
            }
            classIds.clear();
            classIds.add(searchDto.getClassId());
        }
        return analysisTeach(classIds,searchDto);
    }

    /**
     *计算教学检查的分析数据
     *
     * @param classIds
     * @param searchDto
     * @return
     */
    @Override
    public List<AnalysisTeachViewDto> analysisTeach(List<Object> classIds, AnalysisSearchDto searchDto) {
        //定义返回对象
        List<AnalysisTeachViewDto> viewDtoList = new ArrayList<>();
        classIds.stream().forEach(classId -> {
            //查询班级信息
            BacClass bacClass = classMapper.selectOne(new QueryWrapper<BacClass>().lambda()
                    .eq(BacClass::getId,classId));

            AnalysisTeachViewDto viewDto = new AnalysisTeachViewDto();

            viewDto.setClassId(bacClass.getId());
            viewDto.setClassName(bacClass.getName());
            //查询学生列表
            List<Object> studentNumbers = studentMapper.selectObjs(new QueryWrapper<CmsStudent>().lambda()
                    .select(CmsStudent::getStudentNumber)
                    .eq(CmsStudent::getClassId,bacClass.getId()));
            if (studentNumbers.size()==0){
                viewDtoList.add(viewDto);
                return;
            }
            //查询教学班
            List<Object> teachClassIds = stuTeachMapper.selectObjs(new QueryWrapper<BacStuTeach>().lambda()
                    .select(BacStuTeach::getClassId)
                    .in(BacStuTeach::getStudentNumber,studentNumbers)
                    .groupBy(BacStuTeach::getClassId));
            if (teachClassIds.size()==0){
                viewDtoList.add(viewDto);
                return;
            }
            //查询课表
            List<Object> timetableIds = timetableMapper.selectObjs(new QueryWrapper<BacTimetable>().lambda()
                    .select(BacTimetable::getId)
                    .in(BacTimetable::getTeachingClassId,teachClassIds)
                    .ge(BacTimetable::getClassDate,searchDto.getStartDate())
                    .le(BacTimetable::getClassDate,searchDto.getEndDate()));
            if (timetableIds.size()==0){
                viewDtoList.add(viewDto);
                return;
            }
            //查询检查总表
            List<IseInspection> inspectionList = inspectionService.getInspections(searchDto.getStartDate(),searchDto.getEndDate(),timetableIds,CheckType.ISETEACH.getCode());
            if (inspectionList.size()==0){
                viewDtoList.add(viewDto);
                return;
            }
            List<Long> inspectIds = inspectionList.stream().map(item -> item.getId()).collect(Collectors.toList());
            System.out.println(inspectIds);
            //查询教学检查表
            List<IseTeachInspect> teachInspectList = teachInspectMapper.selectList(new QueryWrapper<IseTeachInspect>().lambda()
                    .in(IseTeachInspect::getInspectId,inspectIds));
            teachInspectList.stream().forEach(teachInspect -> {
                viewDto.addAnalysis(teachInspect);
            });
            viewDtoList.add(viewDto);
        });
        return viewDtoList;
    }

    /**
     * 查询反馈教学检查分析
     *
     * @param searchDto
     * @return
     */
    @Override
    public Map<String, Object> getAnalysisStudent(AnalysisNumberSearchDto searchDto) {
        //构造返回对象
        Map<String,Object> resultMap = new HashMap<>();
        List<BacTeachingClass> teachingClassList = getTeachClassByAnalysisNumberSearchDto(searchDto);
        if (teachingClassList.size()==0){
            return null;
        }
        List<AnalysisTeachAllDto> allDtos = analysisStudent(teachingClassList,searchDto);
        List<AnalysisTeachStudentViewDto> viewDtoList = new ArrayList<>();
        allDtos.stream().forEach(allDto -> {
            AnalysisTeachStudentViewDto viewDto = new AnalysisTeachStudentViewDto();
            BeanUtils.copyProperties(allDto,viewDto);
            viewDtoList.add(viewDto);
        });
        //构造总表对象
        AnalysisTeachStudentViewDto allViewDto = new AnalysisTeachStudentViewDto();
        viewDtoList.stream().forEach(viewDto -> {
            allViewDto.addAnalysisAll(viewDto);
        });
        if (searchDto.getNumber()==null){
            resultMap.put("allAnalysis",allViewDto);
            return resultMap;
        }
        resultMap.put("allAnalysis",allViewDto);
        resultMap.put("analysisList",viewDtoList);
        return resultMap;
    }

    /**
     * 查询反馈教学检查分析(老师)
     *
     * @param searchDto
     * @return
     */
    @Override
    public Map<String, Object> getAnalysisTeacher(AnalysisNumberSearchDto searchDto) {
        //构造返回对象
        Map<String,Object> resultMap = new HashMap<>();

        List<BacTeachingClass> teachingClassList = getTeachClassByAnalysisNumberSearchDto(searchDto);
        if (teachingClassList.size()==0){
            return null;
        }
        List<AnalysisTeachAllDto> allDtos = analysisStudent(teachingClassList,searchDto);
        List<AnalysisTeachTeacherViewDTo> viewDtoList = new ArrayList<>();
        allDtos.stream().forEach(allDto -> {
            AnalysisTeachTeacherViewDTo viewDto = new AnalysisTeachTeacherViewDTo();
            BeanUtils.copyProperties(allDto,viewDto);
            viewDtoList.add(viewDto);
        });
        //构造总表对象
        AnalysisTeachTeacherViewDTo allViewDto = new AnalysisTeachTeacherViewDTo();
        viewDtoList.stream().forEach(viewDto -> {
            allViewDto.addAnalysisAll(viewDto);
        });
        if (searchDto.getNumber()==null){
            resultMap.put("allAnalysis",allViewDto);
            return resultMap;
        }
        resultMap.put("allAnalysis",allViewDto);
        resultMap.put("analysisList",viewDtoList);
        return resultMap;
    }

    /**
     * 查询教学班列表
     *
     * @param searchDto
     * @return
     */
    @Override
    public List<BacTeachingClass> getTeachClassByAnalysisNumberSearchDto(AnalysisNumberSearchDto searchDto) {
        //获取当前用户信息
        CmsUser teacher = userSaveDetails.getCmsUser();
        //查询学期
        BacTerm term = termMapper.selectOne(new QueryWrapper<BacTerm>().lambda()
                .ge(BacTerm::getEndDate,searchDto.getEndDate())
                .le(BacTerm::getOpeningDate,searchDto.getStartDate()));
        //查询教学班列表
        List<BacTeachingClass> teachingClassList = teachingClassMapper.selectList(new QueryWrapper<BacTeachingClass>().lambda()
                .eq(BacTeachingClass::getTeacherNumber,teacher.getNumber())
                .eq(searchDto.getNumber()!=null,BacTeachingClass::getCourseNumber,searchDto.getNumber())
                .eq(BacTeachingClass::getTermId,term.getId()));
        return teachingClassList;
    }

    /**
     * 计算数据分析
     *
     * @param teachingClassList
     * @param searchDto
     * @return
     */
    @Override
    public List<AnalysisTeachAllDto> analysisStudent(List<BacTeachingClass> teachingClassList, AnalysisNumberSearchDto searchDto) {
        //构造返回对象
        List<AnalysisTeachAllDto> viewDtoList = new ArrayList<>();
        teachingClassList.stream().forEach(item -> {
            AnalysisTeachAllDto viewDto = new AnalysisTeachAllDto();
            //插入教学班信息
            viewDto.setTeachClassId(item.getId());
            viewDto.setTeachClassName(item.getTeachingClassName());
            //查询课表信息
            List<Object> timetableIds = timetableMapper.selectObjs(new QueryWrapper<BacTimetable>().lambda()
                    .select(BacTimetable::getId)
                    .eq(BacTimetable::getTeachingClassId,item.getId())
                    .ge(BacTimetable::getClassDate,searchDto.getStartDate())
                    .le(BacTimetable::getClassDate,searchDto.getEndDate()));
            if (timetableIds.size()==0){
                viewDtoList.add(viewDto);
                return;
            }
            //查询检查总表信息
            List<IseInspection> inspectionList = inspectionService.getInspections(searchDto.getStartDate(),searchDto.getEndDate(),timetableIds,CheckType.ISETEACH.getCode());
            List<Object> inspectIds = inspectionList.stream().map(inspection -> inspection.getId()).collect(Collectors.toList());
            if (inspectionList.size()==0){
                viewDtoList.add(viewDto);
                return;
            }
            //查询教学检查表
            List<IseTeachInspect> teachInspectList = teachInspectMapper.selectList(new QueryWrapper<IseTeachInspect>().lambda()
                    .in(IseTeachInspect::getInspectId,inspectIds));
            teachInspectList.stream().forEach(teachInspect -> {
                viewDto.addAnalysis(teachInspect);
            });
            viewDtoList.add(viewDto);
        });
        return viewDtoList;
    }

    /**
     * 查询教学班应到人数(对象id使用)
     *
     * @param id
     * @return
     */
    @Override
    public Integer getTeachCount(Long id) {
        //查询教学班和学生关联表
        return stuTeachMapper.selectCount(new QueryWrapper<BacStuTeach>().lambda()
                .eq(BacStuTeach::getClassId,id));
    }

    /**
     * 查询教学班应到人数(检查总表id使用)
     *
     * @param id
     * @return
     */
    @Override
    public Integer getTeachCountByInspectId(Long id) {
        //查询检查总表记录
        IseInspection inspection = iseInspectionMapper.selectById(id);
        //查询课表记录
        BacTimetable timetable = timetableMapper.selectById(inspection.getItemCheckId());
        //查询学生教学班关联表记录
        return stuTeachMapper.selectCount(new QueryWrapper<BacStuTeach>().lambda()
                .eq(BacStuTeach::getClassId,timetable.getTeachingClassId()));
    }
}
