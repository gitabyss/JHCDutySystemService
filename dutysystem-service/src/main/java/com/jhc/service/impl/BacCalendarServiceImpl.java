package com.jhc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.entity.BacCalendar;
import com.jhc.mapper.BacCalendarMapper;
import com.jhc.service.IBacCalendarService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2020-03-11
 */
@Service
public class BacCalendarServiceImpl extends ServiceImpl<BacCalendarMapper, BacCalendar> implements IBacCalendarService {

}
