package com.jhc.service.impl;

import com.jhc.entity.CmsConfig;
import com.jhc.mapper.CmsConfigMapper;
import com.jhc.service.ICmsConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统配置 服务实现类
 * </p>
 *
 * @author zfm
 * @since 2020-03-11
 */
@Service
public class CmsConfigServiceImpl extends ServiceImpl<CmsConfigMapper, CmsConfig> implements ICmsConfigService {

}
