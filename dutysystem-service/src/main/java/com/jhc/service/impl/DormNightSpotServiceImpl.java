package com.jhc.service.impl;

import com.jhc.entity.DormNightSpot;
import com.jhc.mapper.DormNightSpotMapper;
import com.jhc.service.IDormNightSpotService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 夜不归宿抽查记录 服务实现类
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
@Service
public class DormNightSpotServiceImpl extends ServiceImpl<DormNightSpotMapper, DormNightSpot> implements IDormNightSpotService {

}
