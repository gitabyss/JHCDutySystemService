package com.jhc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.bo.UserSaveDetails;
import com.jhc.dto.IseScheduleAdd;
import com.jhc.dto.IseScheduleSearchDto;
import com.jhc.dto.IseScheduleViewDto;
import com.jhc.entity.IseSchedule;
import com.jhc.mapper.BacWeekDateMapper;
import com.jhc.mapper.CmsTeacherMapper;
import com.jhc.mapper.IseScheduleMapper;
import com.jhc.service.IIseScheduleService;
import com.jhc.utils.JHC;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * <p>
 * 值班计划   服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class IseScheduleServiceImpl extends ServiceImpl<IseScheduleMapper, IseSchedule> implements IIseScheduleService {

    @Autowired
    private IseScheduleMapper iseScheduleMapper;

    @Autowired
    private BacWeekDateMapper weekDateMapper;

    @Autowired
    private CmsTeacherMapper cmsTeacherMapper;

    @Autowired
    private UserSaveDetails userSaveDetails;


    /**
     * 添加值班信息
     *
     * @param iseScheduleAdd
     * @return
     */
    @Override
    public boolean addIseSchedule(IseScheduleAdd iseScheduleAdd) {
        //获取登录用户名
        String username = userSaveDetails.getCmsUser().getNumber();
        //构造插入对象
        IseSchedule schedule = new IseSchedule();
        BeanUtils.copyProperties(iseScheduleAdd,schedule);
        schedule.setCreaterNumber(username);
        //查询是否有相同时间记录
        IseSchedule iseSchedule = iseScheduleMapper.selectOne(new QueryWrapper<IseSchedule>().lambda().eq(IseSchedule::getWeekDayId,schedule.getWeekDayId()));
        if (iseSchedule!=null){
            return false;
        }
        return iseScheduleMapper.insert(schedule) == 1;
    }

    /**
     * 查询值班信息
     *
     * @param scheduleSearchDto
     * @return
     */
    @Override
    public IPage<IseScheduleViewDto> getSchedule(IseScheduleSearchDto scheduleSearchDto) {
        Page page = new Page(scheduleSearchDto.getCurrentPage(),scheduleSearchDto.getPageSize());
        return iseScheduleMapper.selectByIseScheduleSearchDto(page,scheduleSearchDto);
    }

    /**
     * 判断是否值班
     *
     * @param username
     * @param nowDate
     * @return
     */
    @Override
    public IseSchedule isSchedule(String username, Date nowDate) {
        System.out.println(username+"@"+nowDate);
        IseSchedule schedule = iseScheduleMapper.selectByDate(JHC.toDateString(nowDate));
        System.out.println(schedule);
        //若不值班就返回null
        if (schedule==null || !username.equals(schedule.getDutyOneNumber()) && !username.equals(schedule.getDutyTwoNumber())){
            return null;
        }
        return schedule;
    }
}
