package com.jhc.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.bo.UserSaveDetails;
import com.jhc.dto.*;
import com.jhc.entity.*;
import com.jhc.mapper.*;
import com.jhc.service.*;
import com.jhc.utils.CheckType;
import com.jhc.utils.JHC;
import com.jhc.vo.DateVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 检查汇总 填写完检查后生成一条新的记录，未填写前没有这条记录 服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class IseInspectionServiceImpl extends ServiceImpl<IseInspectionMapper, IseInspection> implements IIseInspectionService {


    @Autowired
    private IIse7sInspectService ise7sInspectService;

    @Autowired
    private IIseDormInspectService dormInspectService;

    @Autowired
    private IIseStudyInspectService studyInspectService;

    @Autowired
    private IIseTeachInspectService teachInspectService;

    @Autowired
    private IseInspectionMapper inspectionMapper;

    @Autowired
    private IBacNoticeService noticeService;

    @Autowired
    private UserSaveDetails userSaveDetails;

    @Autowired
    private BacClassMapper classMapper;

    @Autowired
    private IBacTermService termService;

    @Autowired
    private IBacWeekDateService weekDateService;

    @Autowired
    private IIseScheduleService scheduleService;

    @Autowired
    private BacStudyMapper studyMapper;

    @Autowired
    private BacTimetableMapper timetableMapper;

    @Autowired
    private BacTeachingClassMapper teachingClassMapper;

    @Autowired
    private BacTrainingRoomMapper trainingRoomMapper;

    @Autowired
    private CmsTeacherMapper teacherMapper;

    @Autowired
    private BacDormitoryMapper dormitoryMapper;

    @Autowired
    private CmsUserMapper cmsUserMapper;

    @Autowired
    private ICmsUserService cmsUserService;


    /**
     * 查询个检查表的详细信息
     *
     * @param id
     * @return
     */
    @Override
    public Map<String, Object> getDetailInspect(Long id) {
        //查询检查总表信息
        IseInspection inspection = inspectionMapper.selectById(id);
        if (inspection == null){
            return null;
        }
        Map<String,Object> result = new HashMap<>();
        if (inspection.getCheckType().equals(CheckType.ISE7S.getCode())){
            Detail7sViewDto viewDto = ise7sInspectService.getDetail7sInspect(id);
            result.put("detailData",viewDto);
            result.put("type",CheckType.ISE7S.getCode());
        }else if (inspection.getCheckType().equals(CheckType.ISEDORM.getCode())){
            DetailDormViewDto viewDto = dormInspectService.getDetailDormInspect(id);
            result.put("detailData",viewDto);
            result.put("type",CheckType.ISEDORM.getCode());
        }else if (inspection.getCheckType().equals(CheckType.ISESTUDY.getCode())){
            DetailStudyViewDto viewDto = studyInspectService.getDetailStudyInspect(id);
            result.put("detailData",viewDto);
            result.put("type",CheckType.ISESTUDY.getCode());
        }else if (inspection.getCheckType().equals(CheckType.ISETEACH.getCode())){
            DetailTeachViewDto viewDto = teachInspectService.getDetailTeachInspect(id);
            result.put("detailData",viewDto);
            result.put("type",CheckType.ISETEACH.getCode());
        }
        return result;
    }

    /**
     * 查询已检查对象id
     *
     * @return
     */
    @Override
    public List<Object> getItemIds(Date nowDate, Integer type) {
        List<Object> itemCheckIds = inspectionMapper.selectObjs(
                new QueryWrapper<IseInspection>().lambda().select(IseInspection::getItemCheckId)
                        .eq(nowDate!=null,IseInspection::getDutyDate, JHC.toDateString(nowDate))
                        .eq(type!=null,IseInspection::getCheckType,type)
        );
        return itemCheckIds;
    }

    /**
     * 查询检查总表记录
     *
     * @param nowDate
     * @param itemId    检查对象id
     * @param type
     * @return
     */
    @Override
    public List<IseInspection> getInspections(Date nowDate, Long itemId, Integer type) {
        List<IseInspection> inspectionList = inspectionMapper.selectList(new QueryWrapper<IseInspection>().lambda()
                .eq(nowDate!=null,IseInspection::getDutyDate,JHC.toDateString(nowDate))
                .eq(itemId!=null,IseInspection::getItemCheckId,itemId)
                .eq(type!=null,IseInspection::getCheckType,type));
        return inspectionList;
    }

    /**
     * 构造并添加检查总表记录
     *
     * @param addDto
     * @return
     */
    @Override
    public Map<String,Object> addInspection(InspectionCreateDto addDto,String feedbacker) {
        boolean sendMessage = false;
        //构造检查总表插入对象
        IseInspection inspection = new IseInspection();
        BeanUtils.copyProperties(addDto,inspection);
        inspection.setWeek(addDto.getWeek());
        inspection.setDutyDate(addDto.getNowDate());
        inspection.setDutyTeacher(addDto.getUsername());
        inspection.setCheckType(addDto.getType());
        inspection.setTeachersNumbers(JSON.toJSONString(addDto.getTeacherNumbers()));
        inspection.setItemCheckId(addDto.getId());
        inspection.setId(null);
        if (addDto.getFeedback()==false){
            inspection.setFeedbackStatus(10);
        }else {
            inspection.setFeedbackStatus(30);
            sendMessage = true;
        }
        inspectionMapper.insert(inspection);
        List<BizCheckMessageDto> messageDtos = null;
        if (sendMessage){
            //构建插入消息表对象
            CheckNoticeAddDto checkNoticeAddDto = new CheckNoticeAddDto();
            checkNoticeAddDto.setAdminNumber(addDto.getUsername());
            checkNoticeAddDto.setCheckId(inspection.getId());
            checkNoticeAddDto.setTitle(CheckType.getCheckType(addDto.getType()).getMessage()+"反馈消息");
            List<String> teachList = addDto.getTeacherNumbers();
            teachList.add(feedbacker);
            checkNoticeAddDto.setStaffNumberList(teachList);
            checkNoticeAddDto.setContent("请点击此信息查看检查详细信息");
            //插入消息
            messageDtos = noticeService.sendMessage(checkNoticeAddDto);
        }
        Map<String,Object> resultObject = new HashMap<>();
        resultObject.put("inspection",inspection);
        resultObject.put("msgList",messageDtos);
        return resultObject;
    }

    /**
     * 查询检查总表记录（根据开始日期和结束日期）
     *
     * @param stratDate
     * @param endDate
     * @param itemIds
     * @param type
     * @return
     */
    @Override
    public List<IseInspection> getInspections(Date stratDate, Date endDate, List<Object> itemIds, Integer type) {
        List<IseInspection> inspectionList = inspectionMapper.selectList(new QueryWrapper<IseInspection>().lambda()
                .in(IseInspection::getItemCheckId,itemIds)
                .eq(IseInspection::getCheckType,type)
                .ge(IseInspection::getDutyDate,stratDate)
                .le(IseInspection::getDutyDate,endDate));
        return inspectionList;
    }

    /**
     * 查询班情统揽数据分析
     *
     * @param searchDto
     * @return
     */
    @Override
    public List<AnalysisAllViewDto> getAnalysis(AnalysisSearchDto searchDto) {
        CmsUser teacher = userSaveDetails.getCmsUser();
        //查询班主任所在班级id
        List<Object> classIds = classMapper.selectObjs(new QueryWrapper<BacClass>().lambda()
                .select(BacClass::getId)
                .eq(BacClass::getTeacherNumber,teacher.getNumber())
                .ge(BacClass::getEndDate,searchDto.getEndDate())
                .le(BacClass::getStartDate,searchDto.getStartDate()));
        if (classIds.size()==0){
            return new ArrayList<>();
        }
        //判断查询条件classId
        if (searchDto.getClassId()!=null){
            if (!classIds.contains(searchDto.getClassId())){
                return new ArrayList<>();
            }
            classIds.clear();
            classIds.add(searchDto.getClassId());
        }
        //构造返回对象
        List<AnalysisAllViewDto> viewDtoList = new ArrayList<>();
        List<AnalysisDormViewDto> dormViewDtos = dormInspectService.analysisDorm(classIds,searchDto);
        List<AnalysisStudyViewDto> studyViewDtos = studyInspectService.analysisStudy(classIds,searchDto);
        List<AnalysisTeachViewDto> teachViewDtos = teachInspectService.analysisTeach(classIds,searchDto);
        dormViewDtos.stream().forEach(dormViewDto -> {
            AnalysisAllViewDto viewDto = new AnalysisAllViewDto();
            for (AnalysisStudyViewDto studyViewDto : studyViewDtos) {
                if (studyViewDto.getClassId().equals(dormViewDto.getClassId())){
                    viewDto.setStudyState(studyViewDto.getStudyState());
                    break;
                }
            }
            for (AnalysisTeachViewDto teachViewDto : teachViewDtos) {
                if (teachViewDto.getClassId().equals(dormViewDto.getClassId())){
                    BeanUtils.copyProperties(teachViewDto,viewDto);
                    viewDto.setClassingState(teachViewDto.getClassStateAdd());
                    break;
                }
            }
            BeanUtils.copyProperties(dormViewDto,viewDto);
            viewDto.setDormHygiene(dormViewDto.getDormHygiene());
            viewDtoList.add(viewDto);
        });
        return viewDtoList;
    }

    /**
     * 查询日期
     *
     * @return
     */
    @Override
    public DateVo getDate(Integer type,Long dateId) {
        if (type!=null&&type==1){
            System.out.println("1");
            return termService.getTermDate(dateId);
        }
        System.out.println("2");
        return weekDateService.getWeekDate(dateId);
    }

    /**
     * 查询单个班所有级数据分析
     *
     * @param searchDto
     * @return
     */
    @Override
    public Map<String,Object> getAnalysisOne(AnalysisSearchDto searchDto) {
        CmsUser teacher = userSaveDetails.getCmsUser();
        //查询班主任所在班级id
        List<Object> classIds = classMapper.selectObjs(new QueryWrapper<BacClass>().lambda()
                .select(BacClass::getId)
                .eq(BacClass::getTeacherNumber,teacher.getNumber()));
        if (classIds.size()==0){
            return new HashMap<>();
        }
        //判断查询条件classId
        if (searchDto.getClassId()==null){
            return new HashMap<>();
        }
        if (!classIds.contains(searchDto.getClassId())){
            return new HashMap<>();
        }
        classIds.clear();
        classIds.add(searchDto.getClassId());
        //构造返回对象
//        List<Map<String,Object>> resultList = new ArrayList<>();
        Map<String,Object> resultMap = new HashMap<>();
        AnalysisAllViewDto allViewDto = new AnalysisAllViewDto();
        //获取各项检查数据分析
        List<AnalysisDormViewDto> dormViewDtos = dormInspectService.analysisDorm(classIds,searchDto);
        List<AnalysisStudyViewDto> studyViewDtos = studyInspectService.analysisStudy(classIds,searchDto);
        List<AnalysisTeachViewDto> teachViewDtos = teachInspectService.analysisTeach(classIds,searchDto);

        allViewDto.setStudyState(studyViewDtos.get(0).getStudyState());
        BeanUtils.copyProperties(teachViewDtos.get(0),allViewDto);
        allViewDto.setClassingState(teachViewDtos.get(0).getClassStateAdd());
        BeanUtils.copyProperties(dormViewDtos.get(0),allViewDto);
        allViewDto.setDormHygiene(dormViewDtos.get(0).getDormHygiene());

//        Map<String,Object> map1 = new HashMap<>();
//        map1.put("inspection",allViewDto);
//        Map<String,Object> map2 = new HashMap<>();
//        map2.put("dormInspect",dormViewDtos.get(0));
//        Map<String,Object> map3 = new HashMap<>();
//        map3.put("studyInspect",studyViewDtos.get(0));
//        Map<String,Object> map4 = new HashMap<>();
//        map4.put("teachInspect",teachViewDtos.get(0));
//        resultList.add(map1);
//        resultList.add(map2);
//        resultList.add(map3);
//        resultList.add(map4);
        resultMap.put("inspection",allViewDto);
        resultMap.put("dormInspect",dormViewDtos.get(0));
        resultMap.put("studyInspect",studyViewDtos.get(0));
        resultMap.put("teachInspect",teachViewDtos.get(0));
        return resultMap;
    }

    /**
     * 手机扫码返回当前检查列表
     *
     * @param id
     * @param type
     * @return
     */
    @Override
    public Map<String,Object> getPhoneInspect(Long id, Integer type) {
        //构造返回对象
        Map<String,Object> resultMap = new HashMap<>();
        //获取用户名
        CmsUser teacher = userSaveDetails.getCmsUser();
        //获取当前日期
        Date nowDate = JHC.removeTime(new Date());
        //查询值班
        IseSchedule schedule = scheduleService.isSchedule(teacher.getNumber(),nowDate);
        //当前用户不值班
        if (schedule == null){
            resultMap.put("isWrited",null);
            resultMap.put("msg","未到值班时间");
            return resultMap;
        }
        //查询检查总表
        //查询对象id
        List<Object> itemIds = getIseInspect(id,type);
        if (itemIds.size()==0){
            resultMap.put("isWrited",null);
            resultMap.put("msg","没有对应信息");
            return resultMap;
        }
        //查询检查总表
        List<IseInspection> inspectionList = getInspections(nowDate,nowDate,itemIds,type);
        //教学检查
        if (type.equals(CheckType.ISETEACH.getCode())){
            List<Map<String,Object>> mapList = new ArrayList<>();
            List<Object> inspectIds = inspectionList.stream().map(inspect -> inspect.getItemCheckId()).collect(Collectors.toList());
            for (Object itemId : itemIds) {
                //查询课表信息
                BacTimetable timetable = timetableMapper.selectById((Long) itemId);
                //查询教学班信息
                BacTeachingClass teachingClass = teachingClassMapper.selectById(timetable.getTeachingClassId());
                //构造封装对象
                Map<String,Object> map = new HashMap<>();
                if (!inspectIds.contains(itemId)){
                    map.put("isWrited",false);
                    map.put("id",itemId);
                }else {
                    for (IseInspection inspection : inspectionList) {
                        if (inspection.getItemCheckId().equals(itemId)){
                            map.put("isWrited",true);
                            map.put("id",inspection.getId());
                        }
                    }
                }
                map.put("section",timetable.getSection());
                map.put("name",teachingClass.getTeachingClassName());
                mapList.add(map);
            }
            resultMap.put("teachClassList",mapList);
            return resultMap;
        }
        //其他检查
        if (inspectionList.size()==0){
            resultMap.put("isWrited",false);
            resultMap.put("id",itemIds.get(0));
            return resultMap;
        }
        resultMap.put("isWrited",true);
        resultMap.put("id",inspectionList.get(0).getId());
        return resultMap;
    }

    /**
     * 手机扫码返回当前检查列表2
     *
     * @param id
     * @param type
     * @return
     */
    @Override
    public Map<String, Object> getPhoneInspect2(Long id, Integer type) {
        //构造返回对象
        Map<String,Object> resultMap = new HashMap<>();
        //获取用户名
        CmsUser teacher = userSaveDetails.getCmsUser();
        //获取当前日期
        Date nowDate = JHC.removeTime(new Date());
        //查询值班
        IseSchedule schedule = scheduleService.isSchedule(teacher.getNumber(),nowDate);
        //当前用户不值班
        if (schedule == null){
            resultMap.put("isWrited",null);
            resultMap.put("msg","未到值班时间");
            return resultMap;
        }
        if (type.equals(1)){
            Map<String,Object> map7s = getPhone7s(id);
            if (map7s!=null){
                resultMap.put("ise7s",map7s);
            }
            List<Map<String,Object>> teachList = getPhoneTeach(id);
            if (teachList!=null){
                resultMap.put("iseTeach",teachList);
            }
        }else if (type.equals(2)){
            Map<String,Object> mapDorm = getPhoneDorm(id);
            if (mapDorm!=null){
                resultMap.put("iseDorm",mapDorm);
            }
        }else if (type.equals(3)){
            Map<String,Object> mapStudy = getPhoneStudy(id);
            if (mapStudy!=null){
                resultMap.put("iseStudy",mapStudy);
            }
            List<Map<String,Object>> teachList = getPhoneTeach(id);
            if (teachList!=null){
                resultMap.put("iseTeach",teachList);
            }
        }
        return resultMap;
    }

    /**
     * 手机扫码返回7s
     *
     * @param id
     * @return
     */
    @Override
    public Map<String, Object> getPhone7s(Long id) {
        //查询检查总表
        //查询对象id
        List<Object> itemIds = getIseInspect(id,CheckType.ISE7S.getCode());
        if (itemIds.size()==0){
            return null;
        }
        //查询实训室信息
        BacTrainingRoom room = trainingRoomMapper.selectById((Long)itemIds.get(0));
        //查询老师信息
        CmsUser teacher = cmsUserMapper.selectOne(new QueryWrapper<CmsUser>().lambda()
                .eq(CmsUser::getNumber,room.getResponsibleNumber()));
        //构造返回对象
        Map<String,Object> resultMap = new HashMap<>();
        //获取当前日期
        Date nowDate = JHC.removeTime(new Date());
        //查询检查总表
        List<IseInspection> inspectionList = getInspections(nowDate,nowDate,itemIds,CheckType.ISE7S.getCode());
        if (inspectionList.size()==0){
            resultMap.put("isWrited",false);
            resultMap.put("id",itemIds.get(0));
            resultMap.put("roomName",room.getRoomName());
            resultMap.put("teacherName",teacher.getName());
            resultMap.put("teacher",teacher.getName()+"@"+teacher.getNumber());
        }else {
            resultMap.put("isWrited",true);
            resultMap.put("id",inspectionList.get(0).getId());
            resultMap.put("roomName",room.getRoomName());
            resultMap.put("teacher",teacher.getName()+"@"+teacher.getNumber());
        }
        return resultMap;
    }

    /**
     * 手机扫码返回dorm
     *
     * @param id
     * @return
     */
    @Override
    public Map<String, Object> getPhoneDorm(Long id) {
        //查询检查总表
        //查询对象id
        List<Object> itemIds = getIseInspect(id,CheckType.ISEDORM.getCode());
        if (itemIds.size()==0){
            return null;
        }
        //查询寝室信息
        BacDormitory dormitory = dormitoryMapper.selectById((Long)itemIds.get(0));
        //查询班级信息
        List<String> stringList = dormInspectService.getTeachAndClass(dormitory.getClassTeacherNumber(),dormitory.getClassId());
        if (stringList.size()==0){
            return null;
        }
        //构造返回对象
        Map<String,Object> resultMap = new HashMap<>();
        //获取当前日期
        Date nowDate = JHC.removeTime(new Date());
        //查询检查总表
        List<IseInspection> inspectionList = getInspections(nowDate,nowDate,itemIds,CheckType.ISEDORM.getCode());
        if (inspectionList.size()==0){
            resultMap.put("isWrited",false);
            resultMap.put("id",itemIds.get(0));
            resultMap.put("class",stringList.get(0));
            resultMap.put("teacher",stringList.get(1));
            // TODO 错误
            resultMap.put("roomName",dormitory.getBuildingId()+"-"+dormitory.getRoom());
        }else {
            resultMap.put("isWrited",true);
            resultMap.put("id",inspectionList.get(0).getId());
            resultMap.put("className",stringList.get(0));
            resultMap.put("teacher",stringList.get(1));
            // TODO 错误
            resultMap.put("roomName",dormitory.getBuildingId()+"-"+dormitory.getRoom());
        }
        return resultMap;
    }

    /**
     * 手机扫码返回study
     *
     * @param id
     * @return
     */
    @Override
    public Map<String, Object> getPhoneStudy(Long id) {
        //查询检查总表
        //查询对象id
        List<Object> itemIds = getIseInspect(id,CheckType.ISESTUDY.getCode());
        if (itemIds.size()==0){
            return null;
        }
        //查询自习信息
        BacStudy bacStudy = studyMapper.selectById((Long)itemIds.get(0));
        //查询教室信息
        BacTrainingRoom room = trainingRoomMapper.selectById(bacStudy.getRoomId());
        //查询行政班信息
        BacClass bacClass = classMapper.selectById(bacStudy.getClassId());
        //查询班主任信息
        CmsUser teacher = cmsUserMapper.selectOne(new QueryWrapper<CmsUser>().lambda()
                .eq(CmsUser::getNumber,bacClass.getTeacherNumber()));
        //构造返回对象
        Map<String,Object> resultMap = new HashMap<>();
        //获取当前日期
        Date nowDate = JHC.removeTime(new Date());
        //查询检查总表
        List<IseInspection> inspectionList = getInspections(nowDate,nowDate,itemIds,CheckType.ISESTUDY.getCode());
        if (inspectionList.size()==0){
            resultMap.put("isWrited",false);
            resultMap.put("id",itemIds.get(0));
            resultMap.put("teacher",teacher.getName()+"@"+teacher.getNumber());
            resultMap.put("className",bacClass.getName());
            resultMap.put("roomName",room.getRoomName());
        }else {
            resultMap.put("isWrited",true);
            resultMap.put("id",inspectionList.get(0).getId());
            resultMap.put("teacher",teacher.getName()+"@"+teacher.getNumber());
            resultMap.put("className",bacClass.getName());
            resultMap.put("roomName",room.getRoomName());
        }
        return resultMap;
    }

    /**
     * 手机扫码返回teach
     *
     * @param id
     * @return
     */
    @Override
    public List<Map<String,Object>> getPhoneTeach(Long id) {
        //查询检查总表
        //查询对象id
        List<Object> itemIds = getIseInspect(id,CheckType.ISETEACH.getCode());
        if (itemIds.size()==0){
            return null;
        }
        //获取当前日期
        Date nowDate = JHC.removeTime(new Date());
        //查询检查总表
        List<IseInspection> inspectionList = getInspections(nowDate,nowDate,itemIds,CheckType.ISETEACH.getCode());
        List<Map<String,Object>> mapList = new ArrayList<>();
        List<Object> inspectIds = inspectionList.stream().map(inspect -> inspect.getItemCheckId()).collect(Collectors.toList());
        for (Object itemId : itemIds) {
            //查询课表信息
            BacTimetable timetable = timetableMapper.selectById((Long) itemId);
            //查询教学班信息
            BacTeachingClass teachingClass = teachingClassMapper.selectById(timetable.getTeachingClassId());
            //查询教室信息
            BacTrainingRoom room = trainingRoomMapper.selectById(timetable.getRoomId());
            //查询老师信息
            CmsUser teacher = cmsUserMapper.selectOne(new QueryWrapper<CmsUser>().lambda()
                    .eq(CmsUser::getNumber,timetable.getTeacherNumber()));
            //构造封装对象
            Map<String,Object> map = new HashMap<>();
            if (!inspectIds.contains(itemId)){
                map.put("isWrited",false);
                map.put("id",itemId);
            }else {
                for (IseInspection inspection : inspectionList) {
                    if (inspection.getItemCheckId().equals(itemId)){
                        map.put("isWrited",true);
                        map.put("id",inspection.getId());
                    }
                }
            }
            map.put("roomTime",room.getRoomName()+"("+timetable.getSection()+"节"+")");
            map.put("course",timetable.getCourseTitle()+"("+teachingClass.getTeachingClassName()+")");
            map.put("teacher",teacher.getName()+"@"+teacher.getNumber());
            mapList.add(map);
        }
        return mapList;
    }

    /**
     * 根据检查类型和房间id查询检查总表记录
     *
     * @param id
     * @param type
     * @return
     */
    @Override
    public List<Object> getIseInspect(Long id, Integer type) {
        List<Object> itemIds = new ArrayList<>();
        //7s检查
        if (type.equals(10)||type.equals(20)){
            itemIds.add(id);
        }
        //自习检查
        if (type.equals(CheckType.ISESTUDY.getCode())){
            //查询自习id
            BacStudy bacStudy = studyMapper.selectOne(new QueryWrapper<BacStudy>().lambda()
                    .eq(BacStudy::getRoomId,id));
            itemIds.add(bacStudy.getId());
        }
        //教学检查
        if (type.equals(CheckType.ISETEACH.getCode())){
            Date nowDate = JHC.removeTime(new Date());
            //查询课表
            List<BacTimetable> timetableList = timetableMapper.selectList(new QueryWrapper<BacTimetable>().lambda()
                    .eq(BacTimetable::getClassDate,JHC.toDateString(nowDate))
                    .eq(BacTimetable::getRoomId,id));
            List<Object> timetableIds = timetableList.stream().map(item -> item.getId()).collect(Collectors.toList());
            itemIds.addAll(timetableIds);
        }
        return itemIds;
    }
}
