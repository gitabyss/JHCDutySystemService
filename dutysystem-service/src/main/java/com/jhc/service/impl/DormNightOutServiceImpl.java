package com.jhc.service.impl;

import com.jhc.entity.DormNightOut;
import com.jhc.mapper.DormNightOutMapper;
import com.jhc.service.IDormNightOutService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 夜不归宿进出公寓记录 服务实现类
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
@Service
public class DormNightOutServiceImpl extends ServiceImpl<DormNightOutMapper, DormNightOut> implements IDormNightOutService {

}
