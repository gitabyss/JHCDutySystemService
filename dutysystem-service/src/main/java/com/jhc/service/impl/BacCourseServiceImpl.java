package com.jhc.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.bo.UserSaveDetails;
import com.jhc.dto.AnalysisSearchDto;
import com.jhc.dto.BacCourseSearchDto;
import com.jhc.dto.BacCourseViewDto;
import com.jhc.entity.BacCourse;
import com.jhc.entity.BacTeachingClass;
import com.jhc.entity.BacTerm;
import com.jhc.entity.CmsUser;
import com.jhc.mapper.BacCourseMapper;
import com.jhc.mapper.BacTeachingClassMapper;
import com.jhc.mapper.BacTermMapper;
import com.jhc.service.IBacCourseService;
import com.jhc.vo.PageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * <p>
 * 课程信息  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class BacCourseServiceImpl extends ServiceImpl<BacCourseMapper, BacCourse> implements IBacCourseService {
    @Autowired
    private BacCourseMapper bacCourseMapper;

    @Autowired
    private UserSaveDetails userSaveDetails;

    @Autowired
    private BacTermMapper termMapper;

    @Autowired
    private BacTeachingClassMapper teachingClassMapper;

    @Autowired
    private BacCourseMapper courseMapper;
    /**
     * 课程分页条件查询
     * @param pageVo
     * @return
     */
    @Override
    public IPage<BacCourseViewDto> getCoursePageData(PageVo<BacCourseSearchDto> pageVo) {
        if (ObjectUtil.isNull(pageVo.getFilter())) {
            pageVo.setFilter(new BacCourseSearchDto());
        }
        //构造分页参数
        BacCourseSearchDto searchDto = pageVo.getFilter();
        Page page = new Page(pageVo.getCurrentPage(), pageVo.getPageSize());
        //查询院系分页数据
        IPage<BacCourseViewDto> viewDtoIPage = bacCourseMapper.getCoursePageData(page, searchDto);
        return viewDtoIPage;
    }

    /**
     * 查询课程下拉列表
     *
     * @param searchDto
     * @return
     */
    @Override
    public List<Map<String, Object>> getCourseList(AnalysisSearchDto searchDto) {
        //构造返回对象
        List<Map<String,Object>> resultMap = new ArrayList<>();
        //获取当前用户信息
        CmsUser teacher = userSaveDetails.getCmsUser();
        System.out.println(teacher);
        //查询学期
        BacTerm term = termMapper.selectOne(new QueryWrapper<BacTerm>().lambda()
                .ge(BacTerm::getEndDate,searchDto.getEndDate())
                .le(BacTerm::getOpeningDate,searchDto.getStartDate()));
        System.out.println(term);
        //查询教学班列表获取课程id
        List<Object> courseNumbers = teachingClassMapper.selectObjs(new QueryWrapper<BacTeachingClass>().lambda()
                .select(BacTeachingClass::getCourseNumber)
                .eq(BacTeachingClass::getTeacherNumber,teacher.getNumber())
                .eq(BacTeachingClass::getTermId,term.getId())
                .groupBy(BacTeachingClass::getCourseNumber));
        System.out.println(courseNumbers);
        if (courseNumbers.size()==0){
            return resultMap;
        }
        //查询课程列表
        List<BacCourse> courseList = courseMapper.selectList(new QueryWrapper<BacCourse>().lambda()
                .in(BacCourse::getNumber,courseNumbers));
        //获取课程id和name
        courseList.stream().forEach(bacCourse -> {
            Map<String,Object> map = new HashMap<>();
            map.put("courseNumber",bacCourse.getNumber());
            map.put("courseName",bacCourse.getName());
            resultMap.add(map);
        });
        return resultMap;
    }
}
