package com.jhc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.entity.IseStudentInspect;
import com.jhc.mapper.IseStudentInspectMapper;
import com.jhc.service.IIseStudentInspectService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 学生检查表  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class IseStudentInspectServiceImpl extends ServiceImpl<IseStudentInspectMapper, IseStudentInspect> implements IIseStudentInspectService {

}
