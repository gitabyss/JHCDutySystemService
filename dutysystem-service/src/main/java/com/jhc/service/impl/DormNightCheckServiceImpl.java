package com.jhc.service.impl;

import com.jhc.entity.DormNightCheck;
import com.jhc.mapper.DormNightCheckMapper;
import com.jhc.service.IDormNightCheckService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 学生夜不归宿打卡 服务实现类
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
@Service
public class DormNightCheckServiceImpl extends ServiceImpl<DormNightCheckMapper, DormNightCheck> implements IDormNightCheckService {

}
