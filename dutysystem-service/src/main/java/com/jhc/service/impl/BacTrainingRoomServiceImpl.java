package com.jhc.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.dto.BacTrainingCenterSearchDto;
import com.jhc.dto.BacTrainingCenterViewDto;
import com.jhc.dto.BacTrainingRoomSearchDto;
import com.jhc.dto.BacTrainingRoomViewDto;
import com.jhc.entity.BacTrainingRoom;
import com.jhc.mapper.BacTrainingCenterMapper;
import com.jhc.mapper.BacTrainingRoomMapper;
import com.jhc.service.IBacTrainingRoomService;
import com.jhc.vo.PageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 实训室  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class BacTrainingRoomServiceImpl extends ServiceImpl<BacTrainingRoomMapper, BacTrainingRoom> implements IBacTrainingRoomService {
    @Autowired
    private BacTrainingRoomMapper bacTrainingRoomMapper;
    /**
     * 查询实训中心分页数据
     * @param pageVo
     * @return
     */
    @Override
    public IPage<BacTrainingRoomViewDto> getTrainingRoomData(PageVo<BacTrainingRoomSearchDto> pageVo) {
        if (ObjectUtil.isNull(pageVo.getFilter())) {
            pageVo.setFilter(new BacTrainingRoomSearchDto());
        }

        //构造分页参数
        BacTrainingRoomSearchDto searchDto = pageVo.getFilter();
        Page page = new Page(pageVo.getCurrentPage(),pageVo.getPageSize());
        //查询实训中心分页数据
        IPage<BacTrainingRoomViewDto> viewDtoIPage = bacTrainingRoomMapper.getTrainingRoomData(page,searchDto);
        return viewDtoIPage;
    }
}
