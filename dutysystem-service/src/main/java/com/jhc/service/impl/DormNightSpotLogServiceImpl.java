package com.jhc.service.impl;

import com.jhc.entity.DormNightSpotLog;
import com.jhc.mapper.DormNightSpotLogMapper;
import com.jhc.service.IDormNightSpotLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 学生值班计划分配表 服务实现类
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
@Service
public class DormNightSpotLogServiceImpl extends ServiceImpl<DormNightSpotLogMapper, DormNightSpotLog> implements IDormNightSpotLogService {

}
