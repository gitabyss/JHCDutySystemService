package com.jhc.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.dto.BizCheckMessageDto;
import com.jhc.dto.BizNoticeSearchDto;
import com.jhc.entity.CmsNoticeObject;
import com.jhc.mapper.BacNoticeObjectMapper;
import com.jhc.service.IBacNoticeObjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 业务消息对象表  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class BacNoticeObjectServiceImpl extends ServiceImpl<BacNoticeObjectMapper, CmsNoticeObject> implements IBacNoticeObjectService {

    @Autowired
    private BacNoticeObjectMapper bacNoticeObjectMapper;

    @Override
    public IPage<BizCheckMessageDto> selectByUsername(BizNoticeSearchDto bizNoticeSearchDto, String username) {
        Page page = new Page(bizNoticeSearchDto.getCurrentPage(), bizNoticeSearchDto.getPageSize());
        return bacNoticeObjectMapper.selectByUsername(page, bizNoticeSearchDto, username);
    }
}
