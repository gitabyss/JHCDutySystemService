package com.jhc.service.impl;

import com.jhc.dto.IseImageViewDto;
import com.jhc.entity.IseImages;
import com.jhc.mapper.IseImagesMapper;
import com.jhc.service.IIseImagesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.utils.FileNameUtil;
import com.jhc.utils.FileUploadUtil;
import com.jhc.utils.ImageSuffix;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * <p>
 * 检查图片表 服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-12-10
 */
@Service
public class IseImagesServiceImpl extends ServiceImpl<IseImagesMapper, IseImages> implements IIseImagesService {

    @Value("${web.ise-images}")
    private String localPath;

    @Value("${web.ise-images-mapping}")
    private String mappingPath;

    @Autowired
    private ImageSuffix imageSuffix;

    /**
     * 检查图片上传
     *
     * @param file
     * @return
     */
    @Override
    public IseImageViewDto upload(MultipartFile file) {
        if (file == null){
            return null;
        }
        //获取新文件名
        String fileName = file.getOriginalFilename();
        if (!imageSuffix.isImages(fileName)){
            return null;
        }
        fileName = FileNameUtil.getFileName(fileName);
        //拼接文件路径，上传到服务器
        if (FileUploadUtil.upload(file,localPath,fileName)){
            IseImageViewDto images = new IseImageViewDto();
            //拼接图片映射路径
            images.setAddress(mappingPath+"/"+fileName);
            images.setId(0L);
            return images;
        }
        return null;
    }
}
