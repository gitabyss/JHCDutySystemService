package com.jhc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.entity.CmsStudent;
import com.jhc.mapper.CmsStudentMapper;
import com.jhc.service.ICmsStudentService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 学生  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class CmsStudentServiceImpl extends ServiceImpl<CmsStudentMapper, CmsStudent> implements ICmsStudentService {

}
