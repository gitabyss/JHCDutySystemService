package com.jhc.service.impl;

import com.jhc.entity.DormNightAdviserLog;
import com.jhc.mapper.DormNightAdviserLogMapper;
import com.jhc.service.IDormNightAdviserLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 夜不归宿班主任操作记录 服务实现类
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
@Service
public class DormNightAdviserLogServiceImpl extends ServiceImpl<DormNightAdviserLogMapper, DormNightAdviserLog> implements IDormNightAdviserLogService {

}
