package com.jhc.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.dto.BizCheckMessageDto;
import com.jhc.dto.CheckNoticeAddDto;
import com.jhc.dto.CheckNoticeJSON;
import com.jhc.entity.CmsNotice;
import com.jhc.entity.CmsNoticeObject;
import com.jhc.mapper.BacNoticeMapper;
import com.jhc.service.IBacNoticeObjectService;
import com.jhc.service.IBacNoticeService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 业务消息表  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class BacNoticeServiceImpl extends ServiceImpl<BacNoticeMapper, CmsNotice> implements IBacNoticeService {

    @Autowired
    private IBacNoticeObjectService iBacNoticeObjectService;

    @Autowired
    private BacNoticeMapper bacNoticeMapper;

    @Override
    public List<BizCheckMessageDto> sendMessage(CheckNoticeAddDto checkNoticeAddDto) {
        CmsNotice cmsNotice = new CmsNotice();
        cmsNotice.setAdminNumber(checkNoticeAddDto.getAdminNumber());
        cmsNotice.setTitle(checkNoticeAddDto.getTitle());
        // 默认抄送消息
        cmsNotice.setType(20);
        if (checkNoticeAddDto.getStaffNumberList().size() > 1) {
            // 群体消息
            cmsNotice.setObjectType(20);
        }
        // 转消息内容为对象
        CheckNoticeJSON checkNoticeJSON = new CheckNoticeJSON();
        BeanUtils.copyProperties(checkNoticeAddDto, checkNoticeJSON);
        String content = JSON.toJSONString(checkNoticeJSON);
        cmsNotice.setContent(content);

        List<BizCheckMessageDto> bizCheckMessageDtoList = new ArrayList<>();

        if (bacNoticeMapper.insert(cmsNotice) == 1) {
            // 插入消息表
            List<CmsNoticeObject> cmsNoticeObjectList = new ArrayList<>();
            checkNoticeAddDto.getStaffNumberList().forEach(item -> {
                CmsNoticeObject cmsNoticeObject = new CmsNoticeObject();
                // 默认为老师
                cmsNoticeObject.setUserType(20);
                cmsNoticeObject.setUserNumber(item);
                // 消息总表ID关联
                cmsNoticeObject.setNoticeId(cmsNotice.getId());
                cmsNoticeObjectList.add(cmsNoticeObject);
            });
            if (iBacNoticeObjectService.saveBatch(cmsNoticeObjectList)) {
                cmsNoticeObjectList.forEach(item -> {
                    // 返回给controller发送的消息内容
                    BizCheckMessageDto bizCheckMessageDto = new BizCheckMessageDto();
                    BeanUtils.copyProperties(cmsNotice, bizCheckMessageDto);
                    BeanUtils.copyProperties(item, bizCheckMessageDto);
                    bizCheckMessageDto.setId(item.getId());
                    bizCheckMessageDtoList.add(bizCheckMessageDto);
                });
                return bizCheckMessageDtoList;
            }
        }
        return null;
    }
}
