package com.jhc.service.impl;

import com.jhc.entity.DormNightSpotPlan;
import com.jhc.mapper.DormNightSpotPlanMapper;
import com.jhc.service.IDormNightSpotPlanService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 夜不归宿抽查计划表 服务实现类
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
@Service
public class DormNightSpotPlanServiceImpl extends ServiceImpl<DormNightSpotPlanMapper, DormNightSpotPlan> implements IDormNightSpotPlanService {

}
