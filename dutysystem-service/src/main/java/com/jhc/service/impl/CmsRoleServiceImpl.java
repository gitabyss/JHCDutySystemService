package com.jhc.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.dto.BizRoleSearchUserByRoleDto;
import com.jhc.dto.CmsRoleUpdate;
import com.jhc.dto.CmsTreePermission;
import com.jhc.entity.CmsPermission;
import com.jhc.entity.CmsRole;
import com.jhc.entity.CmsUser;
import com.jhc.entity.UserAuthData;
import com.jhc.mapper.CmsPermissionMapper;
import com.jhc.mapper.CmsRoleMapper;
import com.jhc.mapper.CmsTeacherMapper;
import com.jhc.mapper.CmsUserMapper;
import com.jhc.service.ICmsRoleService;
import com.jhc.service.ICmsUserService;
import com.jhc.service.RedisService;
import com.jhc.utils.JHC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * cms_role  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class CmsRoleServiceImpl extends ServiceImpl<CmsRoleMapper, CmsRole> implements ICmsRoleService {

    @Autowired
    private CmsTeacherMapper cmsTeacherMapper;

    @Autowired
    private RedisService redisService;

    @Autowired
    private CmsRoleMapper cmsRoleMapper;

    @Autowired
    private CmsPermissionMapper cmsPermissionMapper;

    @Autowired
    private CmsUserMapper cmsUserMapper;

    @Autowired
    private ICmsUserService cmsUserService;

    /**
     * 让所有拥有改角色的用户下线
     *
     * @param cmsRoleUpdate 更新的角色信息
     */
    @Async("taskExecutor")
    @Override
    public void updateRole(CmsRoleUpdate cmsRoleUpdate) {
        String strRoleId = "\"" + cmsRoleUpdate.getId() + "\"";
        // 查询所有拥有改角色的用户
        List<CmsUser> cmsUserList = cmsUserMapper.selectList(new QueryWrapper<CmsUser>()
                .lambda().select(CmsUser::getNumber).like(CmsUser::getAuthData, strRoleId));
        // 清除redis
        cmsUserList.forEach(item -> {
            String staffNumber = item.getNumber();
            if (cmsRoleUpdate.getIfFast()) {
                redisService.remove(staffNumber);
            } else {
                redisService.expire(staffNumber, 60 * 60);
            }
        });
    }

    @Override
    public List<CmsTreePermission> getPermissionList(Long roleId) {
        CmsRole cmsRole = cmsRoleMapper.selectOne(new QueryWrapper<CmsRole>().lambda().select(CmsRole::getAuthData)
                .eq(CmsRole::getId, roleId));
        if (cmsRole == null) {
            return null;
        }
        String authString = cmsRole.getAuthData();
        UserAuthData userAuthData = JSON.parseObject(authString, UserAuthData.class);
        List<String> permissionList = userAuthData.getAuthList();
        if (permissionList == null || permissionList.size() == 0) {
            return null;
        }
        List<CmsPermission> cmsPermissionList = cmsPermissionMapper.selectList(
                new QueryWrapper<CmsPermission>().lambda().select(CmsPermission::getId, CmsPermission::getParentId,
                        CmsPermission::getPermissionName, CmsPermission::getValue, CmsPermission::getSort)
                        .in(CmsPermission::getValue, permissionList)
        );

        return JHC.cmsTreePermissions(cmsPermissionList);
    }

    @Override
    public IPage<CmsUser> getUserByRoleInfo(BizRoleSearchUserByRoleDto dto) {
        String likeRoleId = "\"" + dto.getRoleId() + "\"";
        Page<CmsUser> page = new Page<>(dto.getCurrentPage(), dto.getPageSize());
        return cmsTeacherMapper.selectPageByRole(page, dto, likeRoleId);
//        return cmsTeacherMapper.selectPage(page, new QueryWrapper<CmsUser>().lambda()
//                .like(dto.getIsBelong() != null && dto.getIsBelong(), CmsUser::getAuthData, likeRoleId)
//                .notLike(dto.getIsBelong() != null && !dto.getIsBelong(), CmsUser::getAuthData, likeRoleId)
//                .like(dto.getName() != null, CmsUser::getName, dto.getName())
//                .like(dto.getDepartment() != null, CmsUser::getDepartmentName, dto.getDepartment())
//                .like(dto.getStaffNumber() != null, CmsUser::getNumber, dto.getStaffNumber()));
    }
}
