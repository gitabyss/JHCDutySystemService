package com.jhc.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.dto.BacMajorSearchDto;
import com.jhc.dto.BacMajorViewDto;
import com.jhc.dto.BacStudySearchDto;
import com.jhc.dto.BacStudyViewDto;
import com.jhc.entity.BacStudy;
import com.jhc.mapper.BacStudyMapper;
import com.jhc.service.IBacStudyService;
import com.jhc.vo.PageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 自习表  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-12-06
 */
@Service
public class BacStudyServiceImpl extends ServiceImpl<BacStudyMapper, BacStudy> implements IBacStudyService {
    @Autowired
    private BacStudyMapper bacStudyMapper;
    /**
     * 获取自习分页数据
     * @param pageVo
     * @return
     */
    @Override
    public IPage<BacStudyViewDto> studyPageData(PageVo<BacStudySearchDto> pageVo) {
        if (ObjectUtil.isNull(pageVo.getFilter())) {
            pageVo.setFilter(new BacStudySearchDto());
        }

        //构造分页参数
        BacStudySearchDto searchDto = pageVo.getFilter();
        Page page = new Page(pageVo.getCurrentPage(), pageVo.getPageSize());
        //查询院系分页数据
        IPage<BacStudyViewDto> viewDtoIPage = bacStudyMapper.getStudyDataPage(page, searchDto);
        return viewDtoIPage;
    }

}
