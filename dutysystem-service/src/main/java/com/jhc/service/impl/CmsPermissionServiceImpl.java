package com.jhc.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.dto.CmsTreePermission;
import com.jhc.entity.CmsPermission;
import com.jhc.mapper.CmsPermissionMapper;
import com.jhc.service.ICmsPermissionService;
import com.jhc.utils.JHC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * cms_permission  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class CmsPermissionServiceImpl extends ServiceImpl<CmsPermissionMapper, CmsPermission> implements ICmsPermissionService {

    @Autowired
    private CmsPermissionMapper cmsPermissionMapper;

    @Override
    public List<CmsTreePermission> tree() {
        List<CmsPermission> cmsPermissionList = cmsPermissionMapper.selectList(new QueryWrapper<CmsPermission>().lambda()
                .select(CmsPermission::getId, CmsPermission::getValue,CmsPermission::getParentId, CmsPermission::getPermissionName,CmsPermission::getSort));
        return JHC.cmsTreePermissions(cmsPermissionList);
    }
}
