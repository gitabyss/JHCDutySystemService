package com.jhc.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.dto.BacTrainingCenterSearchDto;
import com.jhc.dto.BacTrainingCenterViewDto;
import com.jhc.entity.BacTrainingCenter;
import com.jhc.mapper.BacTrainingCenterMapper;
import com.jhc.service.IBacTrainingCenterService;
import com.jhc.vo.PageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 实训中心   服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class BacTrainingCenterServiceImpl extends ServiceImpl<BacTrainingCenterMapper, BacTrainingCenter> implements IBacTrainingCenterService {
    @Autowired
    private BacTrainingCenterMapper bacTrainingCenterMapper;
    /**
     * 查询实训中心分页数据
     * @param pageVo
     * @return
     */
    @Override
    public IPage<BacTrainingCenterViewDto> getTrainingCenterData(PageVo<BacTrainingCenterSearchDto> pageVo) {
        if (ObjectUtil.isNull(pageVo.getFilter())) {
            pageVo.setFilter(new BacTrainingCenterSearchDto());
        }

        //构造分页参数
        BacTrainingCenterSearchDto searchDto = pageVo.getFilter();
        Page page = new Page(pageVo.getCurrentPage(),pageVo.getPageSize());
        //查询实训中心分页数据
        IPage<BacTrainingCenterViewDto> viewDtoIPage = bacTrainingCenterMapper.getTrainingCenterData(page,searchDto);
        return viewDtoIPage;
    }

    /**
     * 查询实训中心下拉框
     *
     * @return
     */
    @Override
    public List<Map<String, Object>> getTrainCenterList() {
        List<BacTrainingCenter> trainingCenterList = bacTrainingCenterMapper.selectList(null);
        List<Map<String,Object>> resultList = new ArrayList<>();
        trainingCenterList.stream().forEach(item -> {
            Map<String,Object> map = new HashMap<>();
            map.put("centerId",item.getId());
            map.put("centerName",item.getCenterName());
            resultList.add(map);
        });
        return resultList;
    }
}
