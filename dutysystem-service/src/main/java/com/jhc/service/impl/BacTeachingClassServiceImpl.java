package com.jhc.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.dto.BacMajorSearchDto;
import com.jhc.dto.BacMajorViewDto;
import com.jhc.dto.BacTeachingClassSearchDto;
import com.jhc.dto.BacTeachingClassViewDto;
import com.jhc.entity.BacTeachingClass;
import com.jhc.mapper.*;
import com.jhc.service.IBacTeachingClassService;
import com.jhc.vo.PageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 教学班  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class BacTeachingClassServiceImpl extends ServiceImpl<BacTeachingClassMapper, BacTeachingClass> implements IBacTeachingClassService {
    @Autowired
    private BacTeachingClassMapper bacTeachingClassMapper;

    /**
     * 分页条件查询教学班
     * @param pageVo
     * @return
     */
    @Override
    public IPage<BacTeachingClassViewDto> getTeachingClassPageData(PageVo<BacTeachingClassSearchDto> pageVo) {
        if (ObjectUtil.isNull(pageVo.getFilter())) {
            pageVo.setFilter(new BacTeachingClassSearchDto());
        }

        //构造分页参数
        BacTeachingClassSearchDto searchDto = pageVo.getFilter();
        Page page = new Page(pageVo.getCurrentPage(), pageVo.getPageSize());
        //查询院系分页数据
        IPage<BacTeachingClassViewDto> viewDtoIPage = bacTeachingClassMapper.getTeachingClassPageData(page, searchDto);
        return viewDtoIPage;
    }
}
