package com.jhc.service.impl;

import com.jhc.entity.DormNightCounselorPlan;
import com.jhc.mapper.DormNightCounselorPlanMapper;
import com.jhc.service.IDormNightCounselorPlanService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 辅导员值班计划 服务实现类
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
@Service
public class DormNightCounselorPlanServiceImpl extends ServiceImpl<DormNightCounselorPlanMapper, DormNightCounselorPlan> implements IDormNightCounselorPlanService {

}
