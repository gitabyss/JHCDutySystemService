package com.jhc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.entity.IseInspectChild;
import com.jhc.mapper.IseInspectChildMapper;
import com.jhc.service.IIseInspectChildService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 检查子表  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class IseInspectChildServiceImpl extends ServiceImpl<IseInspectChildMapper, IseInspectChild> implements IIseInspectChildService {

}
