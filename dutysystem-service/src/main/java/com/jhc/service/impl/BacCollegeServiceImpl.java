package com.jhc.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jhc.dto.BacCollegeViewDto;
import com.jhc.dto.BacCollegeSearchDto;
import com.jhc.dto.CmsTeacherSearchDto;
import com.jhc.dto.CmsTeacherViewDto;
import com.jhc.entity.BacCollege;
import com.jhc.mapper.BacCollegeMapper;
import com.jhc.service.IBacCollegeService;
import com.jhc.vo.PageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 院系档案  服务实现类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Service
public class BacCollegeServiceImpl extends ServiceImpl<BacCollegeMapper, BacCollege> implements IBacCollegeService {
    @Autowired
    private BacCollegeMapper bacCollegeMapper;

    /**
     * 获取院系分页过滤条件查询数据
     * @param pageVo
     * @return
     */
    @Override
    public IPage<BacCollegeViewDto> getCollegePageData(PageVo<BacCollegeSearchDto> pageVo) {
        if (ObjectUtil.isNull(pageVo.getFilter())) {
            pageVo.setFilter(new BacCollegeSearchDto());
        }

        //构造分页参数
        BacCollegeSearchDto searchDto = pageVo.getFilter();
        Page page = new Page(pageVo.getCurrentPage(),pageVo.getPageSize());
        //查询院系分页数据
        IPage<BacCollegeViewDto> viewDtoIPage = bacCollegeMapper.getCollegePageData(page,searchDto);
        return viewDtoIPage;
    }

    /**
     *
     * 获取学院下拉框
     * @return
     */
    @Override
    public List<Map<String, Object>> getCollegeList() {
        //构造返回对象
        List<Map<String,Object>> resultList = new ArrayList<>();
        //查询部门列表
        List<BacCollege> collegeList = bacCollegeMapper.selectCollegeList();
        //提取number和name
        collegeList.stream().forEach(college ->{
            Map<String,Object> map = new HashMap<>();
            map.put("collegeNumber",college.getNumber());
            map.put("collegeName",college.getName());
            resultList.add(map);
        });
        return resultList;
    }

    /**
     * 获取部门下拉框
     *
     * @return
     */
    @Override
    public List<Map<String, Object>> getDepartmentList() {
        //构造返回对象
        List<Map<String,Object>> resultList = new ArrayList<>();
        //查询部门列表
        List<BacCollege> collegeList = bacCollegeMapper.selectDepartmentList();
        //提取number和name
        collegeList.stream().forEach(college ->{
            Map<String,Object> map = new HashMap<>();
            map.put("departmentNumber",college.getNumber());
            map.put("departmentName",college.getName());
            resultList.add(map);
        });
        return resultList;
    }

    /**
     * 根据学院获取部门下拉框
     *
     * @param collegeNumber
     * @return
     */
    @Override
    public List<Map<String, Object>> getDepartmentList(String collegeNumber) {
        //构造返回对象
        List<Map<String,Object>> resultList = new ArrayList<>();
        //查询部门列表
        List<BacCollege> departmentList = bacCollegeMapper.selectList(new QueryWrapper<BacCollege>().lambda()
                .eq(BacCollege::getParentNumber,collegeNumber));
        //提取number和name
        departmentList.stream().forEach(department -> {
            Map<String,Object> map = new HashMap<>();
            map.put("departmentNumber",department.getNumber());
            map.put("departmentName",department.getName());
            resultList.add(map);
        });
        return resultList;
    }
}
