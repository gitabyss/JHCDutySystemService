package com.jhc.service;

import com.jhc.dto.IseImageViewDto;
import com.jhc.entity.IseImages;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 * 检查图片表 服务类
 * </p>
 *
 * @author zfm
 * @since 2019-12-10
 */
public interface IIseImagesService extends IService<IseImages> {

    /**
     * 检查图片上传
     *
     * @param file
     * @return
     */
    IseImageViewDto upload(MultipartFile file);

}
