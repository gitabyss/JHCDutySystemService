package com.jhc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.jhc.dto.BacDormitorySearchDto;
import com.jhc.dto.BacDormitoryViewDto;
import com.jhc.dto.BacTrainingCenterSearchDto;
import com.jhc.dto.BacTrainingCenterViewDto;
import com.jhc.entity.BacTrainingCenter;
import com.jhc.vo.PageVo;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 实训中心   服务类
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IBacTrainingCenterService extends IService<BacTrainingCenter> {
    /**
     * 分页查询实训中心分页数据
     * @param pageVo
     * @return
     */
    IPage<BacTrainingCenterViewDto> getTrainingCenterData(PageVo<BacTrainingCenterSearchDto> pageVo);

    /**
     * 查询实训中心下拉框
     *
     * @return
     */
    List<Map<String,Object>> getTrainCenterList();
}
