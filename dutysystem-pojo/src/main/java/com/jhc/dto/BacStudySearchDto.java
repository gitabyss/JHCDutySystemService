package com.jhc.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhuzhixu
 */
@Data
@ApiModel
public class BacStudySearchDto {
    @ApiModelProperty("班级名字")
    private String className;
    @ApiModelProperty("班主任名字")
    private String teacherName;
}
