package com.jhc.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhuzhixu
 */
@Data
@ApiModel
public class BacClassViewDto {
    @ApiModelProperty("编号")
    private Long id;
    /**
     * 名称
     */
    @ApiModelProperty("行政班名称")
    private String name;

    /**
     * 专业
     */
    @ApiModelProperty("所属专业名称")
    private String majorName;

    /**
     * 学院编号
     */
    @ApiModelProperty("所属学院名字")
    private String collegeName;

    /**
     * 班主任名字
     */
    @ApiModelProperty("班主任名字")
    private String teacherName;

    /**
     * 是否启用
     */
    @ApiModelProperty("是否启用")
    private Boolean isEnabled;

    /**
     * 是否毕业
     */
    @ApiModelProperty("是否毕业")
    private Boolean status;
}
