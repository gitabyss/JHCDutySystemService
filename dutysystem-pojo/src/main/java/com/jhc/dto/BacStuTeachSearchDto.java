package com.jhc.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhuzhixu
 */
@ApiModel
@Data
public class BacStuTeachSearchDto {
    /**
     * 所属学院编号
     */
    @ApiModelProperty("所属学院编号")
    private String collegeNumber;
    /**
     * 专业编号
     */
    @ApiModelProperty("专业编号")
    private String specialtyNumber;
    /**
     * 教学班名字
     */
    @ApiModelProperty("教学班名字")
    private String teachingClassName;
    /**
     * 学生姓名
     */
    @ApiModelProperty("学生姓名")
    private String studentName;
    /**
     * 学生学号
     */
    @ApiModelProperty("学生学号")
    private String studentNumber;
}
