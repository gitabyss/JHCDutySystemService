package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author: zfm
 * @Date: 2019/11/14 16:21
 */
@Data
public class CmsUserRoleAdd {

    @NotNull(message = "用户ID不能为空")
    @JsonSerialize(using= ToStringSerializer.class)
    @ApiModelProperty(value = "用户ID")
    private Long id;

    @NotNull(message = "角色不能为空")
    @ApiModelProperty(value = "角色ID")
    private String roleId;

    /**
     * 是否马上生效
     */
    private Boolean ifFast;
}
