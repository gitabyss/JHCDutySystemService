package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/10 16:23
 */
@Data
public class IseDormInspectUpdateDto {

    /**
     *检查对象id
     */
    @NotNull
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty("检查总表id")
    private Long inspectId;

    /**
     * 其他相关老师 单个或多个老师
     */
    @ApiModelProperty("其他相关教师")
    private List<String> teacherNumbers;

    /**
     * 图片
     */
    @ApiModelProperty("图片地址")
    private String imageAddress;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remarks;

    /**
     * 整改要求 检查结果及存在问题
     */
    @ApiModelProperty("整改要求")
    private String requires;

//    /**
//     * 反馈状态 表示是/否反馈和已反馈/待反馈
//     */
//    @NotNull
//    @ApiModelProperty("是否反馈 true->反馈 false->不反馈")
//    private Boolean feedback;

    /**
     * 地面不整洁，垃圾未清理
     */
    @ApiModelProperty("地面不整洁，垃圾未清理")
    private Boolean floor;

    /**
     * 洗漱台无序，水槽有污垢
     */
    @ApiModelProperty("洗漱台无序，水槽有污垢")
    private Boolean pentrough;

    /**
     * 便池不清洁，空气有异味
     */
    @ApiModelProperty("便池不清洁，空气有异味")
    private Boolean toilet;

    /**
     * 被子未叠正，床帷未拉开
     */
    @ApiModelProperty("被子未叠正，床帷未拉开")
    private Boolean bed;

    /**
     * 桌面较凌乱，书柜欠整齐
     */
    @ApiModelProperty("桌面较凌乱，书柜欠整齐")
    private Boolean desk;

    /**
     * 衣服未挂齐，鞋子乱摆放
     */
    @ApiModelProperty("衣服未挂齐，鞋子乱摆放")
    private Boolean clothes;

    /**
     * 阳台堆杂物，门窗有积灰
     */
    @ApiModelProperty("阳台堆杂物，门窗有积灰")
    private Boolean balcony;

    /**
     * 大功率
     */
    @ApiModelProperty("大功率")
    private Boolean electrical;

    /**
     * 宠物
     */
    @ApiModelProperty("宠物")
    private Boolean pet;

    /**
     * 刀具
     */
    @ApiModelProperty("刀具")
    private Boolean knife;

    /**
     * 酒瓶
     */
    @ApiModelProperty("酒瓶")
    private Boolean bottle;

    /**
     *夜不归宿
     */
    @ApiModelProperty("夜不归宿")
    private Boolean night;
}
