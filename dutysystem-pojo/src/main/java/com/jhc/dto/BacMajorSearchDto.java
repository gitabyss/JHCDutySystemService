package com.jhc.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhuzhixu
 */
@Data
@ApiModel
public class BacMajorSearchDto {
    @ApiModelProperty("专业名称")
    private String professionalName;

    @ApiModelProperty("所属学院编号")
    private String collegeNumber;

    @ApiModelProperty("所属部门编号")
    private String departmentNumber;
}
