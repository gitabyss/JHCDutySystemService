package com.jhc.dto;

import lombok.Data;

/**
 * @Author: zfm
 * @Date: 2019/12/10 19:01
 */
@Data
public class CheckNoticeJSON {
    private String content;
    private Long checkId;
}
