package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/10 16:25
 */
@Data
public class IseTeaInspectUpdateDto {

    /**
     *检查对象id
     */
    @NotNull
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty("检查总表id")
    private Long inspectId;

    /**
     * 其他相关老师 单个或多个老师
     */
    @ApiModelProperty("其他相关教师")
    private List<String> teacherNumbers;

    /**
     * 图片
     */
    @ApiModelProperty("图片地址")
    private String imageAddress;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remarks;

    /**
     * 整改要求 检查结果及存在问题
     */
    @ApiModelProperty("整改要求")
    private String requires;

//    /**
//     * 反馈状态 表示是/否反馈和已反馈/待反馈
//     */
//    @NotNull
//    @ApiModelProperty("是否反馈 true->反馈 false->不反馈")
//    private Boolean feedback;

    /**
     * 迟到
     */
    @NotNull
    @ApiModelProperty("迟到人数")
    private Integer late;

    /**
     * 旷课
     */
    @NotNull
    @ApiModelProperty("旷课人数")
    private Integer truancy;

    /**
     * 请假
     */
    @NotNull
    @ApiModelProperty("请假人数")
    private Integer holiday;

    /**
     * 睡觉
     */
    @ApiModelProperty("睡觉")
    private Boolean sleep;

    /**
     * 交头接耳
     */
    @ApiModelProperty("交头接耳")
    private Boolean chat;

    /**
     * 吃零食
     */
    @ApiModelProperty("吃零食")
    private Boolean eat;

    /**
     * 游戏
     */
    @ApiModelProperty("游戏")
    private Boolean play;

    /**
     * 不带课本
     */
    @ApiModelProperty("不带课本")
    private Boolean noBook;

    /**
     * 玩手机
     */
    @ApiModelProperty("玩手机")
    private Boolean playPhone;

    /**
     * 不穿工作服
     */
    @ApiModelProperty("不穿工作服")
    private Boolean noClothes;

    /**
     * 学生其他信息
     */
    @ApiModelProperty("学生其他信息")
    private String stuOther;

    /**
     * 读ppt
     */
    @ApiModelProperty("读ppt")
    private Boolean ppt;

    /**
     * 无互动
     */
    @ApiModelProperty("无互动")
    private Boolean noInteract;

    /**
     * 不写板书
     */
    @ApiModelProperty("不写板书")
    private Boolean noWrite;

    /**
     * 课堂管理欠佳
     */
    @ApiModelProperty("课堂管理欠佳")
    private Boolean discipline;

    /**
     * 迟到早退
     */
    @ApiModelProperty("迟到早退")
    private Boolean lateLeave;

    /**
     * 坐着上课
     */
    @ApiModelProperty("坐着上课")
    private Boolean sit;

    /**
     * 老师其他信息
     */
    @ApiModelProperty("老师其他信息")
    private String teaOther;
}
