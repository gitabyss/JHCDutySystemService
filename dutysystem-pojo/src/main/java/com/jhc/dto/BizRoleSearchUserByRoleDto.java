package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.jhc.bo.PageDetails;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Author: zfm
 * @Date: 2019/12/15 15:53
 */
@Data
public class BizRoleSearchUserByRoleDto extends PageDetails {

    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "角色ID不得为空")
    private Long roleId;

    @ApiModelProperty(value = "是否属于")
    private Boolean isBelong;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "工号")
    private String staffNumber;

    @ApiModelProperty(value = "隶属部门")
    private String department;
}
