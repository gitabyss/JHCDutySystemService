package com.jhc.dto;

import com.jhc.bo.PageDetails;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/6 10:30
 */
@Data
public class Finish7sSearchDto extends PageDetails {

    /**
     * 开始日期
     */
    @ApiModelProperty("开始日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    /**
     * 结束日期
     */
    @ApiModelProperty("结束日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    /**
     * 管理责任人(实验员)姓名
     */
    @ApiModelProperty("管理责任人(实验员)姓名")
    private String responsibleName;

    /**
     * 实训室名称
     */
    @ApiModelProperty("实训室名称")
    private String roomName;


}
