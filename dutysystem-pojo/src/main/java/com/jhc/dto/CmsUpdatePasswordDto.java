package com.jhc.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * @Author: zfm
 * @Date: 2019/12/26 10:32
 */
@Data
public class CmsUpdatePasswordDto {
    @NotBlank(message = "用户编号不能为空")
    private String number;
    @NotBlank(message = "旧密码不能为空")
    private String oldPassword;
    @NotBlank(message = "新密码不能为空")
    @Size(min = 6, message = "密码不能少于6位")
    private String password;
}
