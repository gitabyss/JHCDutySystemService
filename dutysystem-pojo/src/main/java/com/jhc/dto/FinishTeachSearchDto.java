package com.jhc.dto;

import com.jhc.bo.PageDetails;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/6 16:01
 */
@Data
public class FinishTeachSearchDto extends PageDetails {

    /**
     * 开始日期
     */
    @ApiModelProperty("开始日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    /**
     * 结束日期
     */
    @ApiModelProperty("结束日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    /**
     * 课程名称
     */
    @ApiModelProperty("课程名称")
    private String courseTitle;

    /**
     * 教学班名称
     */
    @ApiModelProperty("教学班名称")
    private String teachClassName;

    /**
     * 上课教师姓名
     */
    @ApiModelProperty("上课教师姓名")
    private String teacherName;

    /**
     * 节次
     */
    @ApiModelProperty("节次")
    private String section;

    /**
     * 教室名称，上课地点
     */
    @ApiModelProperty("上课地点")
    private String roomName;
}
