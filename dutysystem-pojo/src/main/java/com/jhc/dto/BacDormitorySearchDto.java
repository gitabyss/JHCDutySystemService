package com.jhc.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhuzhixu
 */
@Data
@ApiModel
public class BacDormitorySearchDto {
    /**
     * 寝室id号
     */
    @ApiModelProperty("寝室id号")
    private Integer id;

    /**
     * 房间号
     */
    @ApiModelProperty("房间号")
    private Integer room;

    /**
     * 楼幢
     */
    @ApiModelProperty("楼幢")
    private String buildings;

    /**
     * 所属班级
     */
    @ApiModelProperty("所属班级名字")
    private String className;

    /**
     * 班主任
     */
    @ApiModelProperty("班主任名字")
    private String classTeacherName;

    /**
     * 辅导员
     */
    @ApiModelProperty("辅导员名字")
    private String instructorName;

    /**
     * 专业编号
     */
    @ApiModelProperty("专业编号")
    private String specialtyNumber;
}
