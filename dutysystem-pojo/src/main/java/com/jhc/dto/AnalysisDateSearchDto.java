package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/17 10:34
 */
@Data
public class AnalysisDateSearchDto {

    /**
     *班级id
     */
    @ApiModelProperty("班级id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long classId;

    /**
     * 数据类型
     */
    @ApiModelProperty("数据类型 1->学期 2->周次")
    private Integer type = 1;

    /**
     * 学期或周次id
     */
    @ApiModelProperty("学期或周次id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long dateId;
}
