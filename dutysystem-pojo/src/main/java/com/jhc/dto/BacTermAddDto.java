package com.jhc.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @Author: zfm
 * @Date: 2019/12/1 10:32
 */
@Data
public class BacTermAddDto {

    /**
     * 总周次
     */
    @ApiModelProperty("总周次")
    @NotNull(message = "总周次不能为空")
    private Integer teachingWeek;

    /**
     * 开学日期
     */
    @ApiModelProperty("开学日期")
    @NotNull(message = "开学日期不能为空")
    private Date openingDate;

    /**
     * 结束日期
     */
    @ApiModelProperty("结束日期")
    @NotNull(message = "结束日期不能为空")
    private Date endDate;

    /**
     * 学期名称
     */
    @ApiModelProperty("学期名称")
    @NotNull(message = "学期名称不能为空")
    private String termName;

}
