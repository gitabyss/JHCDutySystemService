package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhuzhixu
 */
@Data
@ApiModel
public class CmsTeacherViewDto {
    /**
     * 序号
     */
    @ApiModelProperty("教师编号")
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    /**
     * 职工号
     */
    @ApiModelProperty("职工号")
    private String number;

    /**
     * 姓名
     */
    @ApiModelProperty("姓名")
    private String name;

    /**
     * 性别
     */
    @ApiModelProperty("性别")
    private Integer sex;

    /**
     * 所属学院编号
     */
    @ApiModelProperty("所属学院编号")
    private String collegeNumber;

    /**
     * 所属学院名称
     */
    @ApiModelProperty("所属学院名称")
    private String collegeName;

    /**
     * 所属部门编号
     */
    @ApiModelProperty("所属部门编号")
    private String departmentNumber;

    /**
     * 所属部门名称
     */
    @ApiModelProperty("所属部门名称")
    private String departmentName;

    /**
     * 教师类型
     */
    @ApiModelProperty("教师类型")
    private String type;

    /**
     * 启停状态
     */
    @ApiModelProperty("启停状态")
    private Boolean isEnabled;

}
