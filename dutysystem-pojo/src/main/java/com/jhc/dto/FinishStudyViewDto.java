package com.jhc.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/8 15:21
 */
@Data
public class FinishStudyViewDto {

    /**
     *检查id
     */
    @ApiModelProperty("检查id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 检查日期
     */
    @ApiModelProperty("检查日期")
    @JsonFormat(timezone = "GMT+8")
    private Date dutyDate;

    /**
     * 周次
     */
    @ApiModelProperty("周次")
    private Integer week;

    /**
     * 值班人员
     */
    @ApiModelProperty("值班人员编号")
    private String dutyNumber;

    /**
     * 值班人员
     */
    @ApiModelProperty("值班人员名字")
    private String dutyName;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private Date createTime;

    /**
     * 班级名称
     */
    @ApiModelProperty("班级名称")
    private String className;

    /**
     * 专业名称
     */
    @ApiModelProperty("专业名称")
    private String professionalName;

    /**
     * 部门学院名称
     */
    @ApiModelProperty("部门学院名称")
    private String collegeName;

    /**
     * 班主任姓名
     */
    @ApiModelProperty("班主任姓名")
    private String teacherName;

    /**
     * 班主任编号
     */
    @ApiModelProperty("班主任编号")
    private String teacherNumber;

    /**
     * 教室名称
     */
    @ApiModelProperty("教室名称")
    private String roomName;

    /**
     * 旷课学生数
     */
    private Integer truancy;

    /**
     * 请公假人数
     */
    private Integer publicHoliday;

    /**
     * 请私假人数
     */
    private Integer privateHoliday;

    /**
     * 学生自习状态   10->多数人在学习 20->少数人在学习 30->基本不在学习 40->主题班会讲座
     */
    private Integer studyState;

    /**
     * 整改要求 检查结果及存在问题
     */
    private String requires;

    /**
     * 整改结果 问题反馈及解决情况
     */
    private String result;

    /**
     * 反馈状态 表示是/否反馈和已反馈/待反馈
     */
    private Integer feedbackStatus;
}
