package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/5 18:40
 */
@Data
public class IseStudyInspectViewDto {

    @ApiModelProperty("自习表id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty("检查总表id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long inspectId;

    /**
     * 周次
     */
    @ApiModelProperty("周次")
    private Integer week;

    /**
     * 班级名称
     */
    @ApiModelProperty("班级名称")
    private String className;

    /**
     * 专业名称
     */
    @ApiModelProperty("专业名称")
    private String professionalName;

    /**
     * 部门学院名称
     */
    @ApiModelProperty("部门学院名称")
    private String collegeName;

    /**
     * 班主任姓名
     */
    @ApiModelProperty("班主任姓名")
    private String teacherName;

    /**
     * 班主任编号
     */
    @ApiModelProperty("班主任编号")
    private String teacherNumber;

    /**
     * 教室名称
     */
    @ApiModelProperty("教室名称")
    private String roomName;

    /**
     * 是否填写
     */
    @ApiModelProperty("是否填写 false->未填写 true->已填写")
    private Boolean isWrited=false;
}
