package com.jhc.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhuzhixu
 */
@Data
@ApiModel
public class BacClassStuViewDto {
    /**
     * 性别
     */
    @ApiModelProperty("性别0->男 1->女 2->保密")
    private Integer sex;
    /**
     * 学生姓名
     */
    @ApiModelProperty("学生姓名")
    private String studentName;
    /**
     * 序号
     */
    @ApiModelProperty("编号")
    private Long id;
    /**
     * 行政班编号
     */
    @ApiModelProperty("行政班名字")
    private String className;

    /**
     * 学号
     */
    @ApiModelProperty("学号")
    private String studentNumber;

    /**
     * 专业编号
     */
    @ApiModelProperty("专业名")
    private String specialty;

    /**
     * 学籍状态
     */
    @ApiModelProperty("学籍状态0->在校 1->离校")
    private Integer status;

    /**
     * 所属学院
     */
    @ApiModelProperty("所属学院名字")
    private String collegeName;

    /**
     * 启停状态
     */
    @ApiModelProperty("启停状态 0->启用 1->停用")
    private Boolean isEnable;

}
