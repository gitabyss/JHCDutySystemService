package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.jhc.entity.IseStudyInspect;
import lombok.Data;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/16 18:15
 */
@Data
public class AnalysisStudyViewDto {

    /**
     * 班级id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long classId;

    /**
     * 班级名称
     */
    private String className;

    /**
     * 旷课学生数
     */
    private Integer truancy = 0;

    /**
     * 请公假人数
     */
    private Integer publicHoliday = 0;

    /**
     * 请私假人数
     */
    private Integer privateHoliday = 0;

    /**
     * 学生自习状态   10->多数人在学习 20->少数人在学习 +1 30->基本不在学习 +2 40->主题班会讲座
     */
    private Integer studyState = 0;

    public void addAnalysis(IseStudyInspect studyInspect){
        this.truancy = this.truancy+studyInspect.getTruancy();
        this.publicHoliday = this.publicHoliday+studyInspect.getPublicHoliday();
        this.privateHoliday = this.privateHoliday+studyInspect.getPrivateHoliday();
        if (studyInspect.getStudyState().equals(20)){
            this.studyState++;
        }else if (studyInspect.getStudyState().equals(30)){
            this.studyState = this.studyState+2;
        }
    }


}
