package com.jhc.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author zhuzhixu
 */
@Data
@ApiModel
public class BacTermViewDto {
    @ApiModelProperty("编号")
    private Long id;
    @ApiModelProperty("学期名字")
    private String termName;
    @ApiModelProperty("开学日期")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    private Date openingDate;
    @ApiModelProperty("结束日期")
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    private Date endDate;
    @ApiModelProperty("教学周次")
    private Integer teachingWeek;
    @ApiModelProperty("当前状态")
    private Integer currentState;
}
