package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/17 17:15
 */
@Data
public class WeekIdAndNameDto {

    /**
     * 周次id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long weekId;

    /**
     * 周次
     */
    private Integer week;
}
