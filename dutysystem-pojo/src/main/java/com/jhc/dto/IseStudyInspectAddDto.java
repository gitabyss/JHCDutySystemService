package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/10 16:24
 */
@Data
public class IseStudyInspectAddDto extends IseInspectionAddDto {

    /**
     * 旷课学生数
     */
    @NotNull
    @ApiModelProperty("旷课学生数")
    private Integer truancy;

    /**
     * 请公假人数
     */
    @NotNull
    @ApiModelProperty("请公假人数")
    private Integer publicHoliday;

    /**
     * 请私假人数
     */
    @NotNull
    @ApiModelProperty("请私假人数")
    private Integer privateHoliday;

    /**
     * 学生自习状态   10->多数人在学习 20->少数人在学习 30->基本不在学习 40->主题班会讲座
     */
    @NotNull
    @ApiModelProperty("学生自习状态   10->多数人在学习 20->少数人在学习 30->基本不在学习 40->主题班会讲座")
    private Integer studyState;
}
