package com.jhc.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhuzhixu
 */
@Data
@ApiModel
public class CmsStudentDormitoryViewDto {
    @ApiModelProperty("学号")
    private String number;
    @ApiModelProperty("学生名字")
    private String name;
    @ApiModelProperty("学生班级名字")
    private String className;
    @ApiModelProperty("学生班主任名字")
    private String teacherName;
    @ApiModelProperty("学生电话")
    private String phoneNumber;
}
