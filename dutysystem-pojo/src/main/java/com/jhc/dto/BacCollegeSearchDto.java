package com.jhc.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhuzhixu
 */
@Data
@ApiModel
public class BacCollegeSearchDto {
    @ApiModelProperty("隶属部门编号")
    private String parentNumber;
    @ApiModelProperty("部门名字")
    private String name;
}
