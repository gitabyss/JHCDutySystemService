package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/10 8:52
 */
@Data
public class Ise7sInspectUpdateDto {

    /**
     *检查对象id
     */
    @NotNull
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty("检查总表id")
    private Long inspectId;

    /**
     * 其他相关老师 单个或多个老师
     */
    @ApiModelProperty("其他相关教师")
    private List<String> teacherNumbers;

    /**
     * 整理
     */
    @ApiModelProperty("整理")
    private Boolean arrange;

    /**
     * 整顿
     */
    @ApiModelProperty("整顿")
    private Boolean rectify;

    /**
     * 清扫
     */
    @ApiModelProperty("清扫")
    private Boolean clear;

    /**
     * 报修
     */
    @ApiModelProperty("报修")
    private Boolean repair;

    /**
     * 安全
     */
    @ApiModelProperty("安全")
    private Boolean safe;

    /**
     * 节约
     */
    @ApiModelProperty("节约")
    private Boolean economize;

    /**
     * 表扬
     */
    @ApiModelProperty("表扬")
    private Boolean praise;

    /**
     * 图片
     */
    @ApiModelProperty("图片地址")
    private String imageAddress;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remarks;

    /**
     * 整改要求 检查结果及存在问题
     */
    @ApiModelProperty("整改要求")
    private String requires;

//    /**
//     * 反馈状态 表示是/否反馈和已反馈/待反馈
//     */
//    @NotNull
//    @ApiModelProperty("是否反馈 true->反馈 false->不反馈")
//    private Boolean feedback;
}
