package com.jhc.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author chenkexin
 */
@Data
@ApiModel
public class BacTrainingCenterViewDto {
    @ApiModelProperty("编号")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty("实训中心名称")
    private String centerName;

    @ApiModelProperty("实训中心主任名称")
    private String directorName;

    @ApiModelProperty("实训中心主任编号")
    private String directorNumber;

    @ApiModelProperty("级别")
    private Integer level;

    @ApiModelProperty("类型")
    private Integer type;

    @ApiModelProperty("类别")
    private Integer category;

    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    private Date designDate;

    @ApiModelProperty("实训中心地址")
    private String address;
}
