package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/17 15:26
 */
@Data
public class TermIdAndNameDto {

    /**
     * 学期id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long termId;

    /**
     * 学期名称
     */
    private String termName;
}
