package com.jhc.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @Author: zfm
 * @Date: 2019/11/14 11:14
 */
@Data
public class CmsRoleDelete {

    @NotEmpty(message = "角色ID不能为空")
    private List<Long> roleIdList;
}
