package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.jhc.entity.IseTeachInspect;
import lombok.Data;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/18 14:31
 */
@Data
public class AnalysisTeachStudentViewDto {

    /**
     * 教学班id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long teachClassId;

    /**
     * 教学班名称
     */
    private String teachClassName;

    /**
     * 迟到
     */
    private Integer late = 0;

    /**
     * 旷课
     */
    private Integer truancy = 0;

    /**
     * 请假
     */
    private Integer holiday = 0;

    /**
     * 睡觉
     */
    private Integer sleep = 0;

    /**
     * 交头接耳
     */
    private Integer chat = 0;

    /**
     * 吃零食
     */
    private Integer eat = 0;

    /**
     * 游戏
     */
    private Integer play = 0;

    /**
     * 不带课本
     */
    private Integer noBook = 0;

    /**
     * 玩手机
     */
    private Integer playPhone = 0;

    /**
     * 不穿工作服
     */
    private Integer noClothes = 0;

    public void addAnalysis(IseTeachInspect teachInspect){
        this.late = this.late + teachInspect.getLate();
        this.truancy = this.truancy+teachInspect.getTruancy();
        this.holiday = this.holiday+teachInspect.getHoliday();
        if (teachInspect.getSleep()){
            this.sleep++;
        }
        if (teachInspect.getChat()){
            this.chat++;
        }
        if (teachInspect.getEat()){
            this.eat++;
        }
        if (teachInspect.getPlay()){
            this.play++;
        }
        if (teachInspect.getNoBook()){
            this.noBook++;
        }
        if (teachInspect.getPlayPhone()){
            this.playPhone++;
        }
        if (teachInspect.getNoClothes()){
            this.noClothes++;
        }

    }

    public void addAnalysisAll(AnalysisTeachStudentViewDto viewDto){
        this.late = this.late+viewDto.getLate();
        this.truancy = this.truancy+viewDto.getTruancy();
        this.holiday = this.holiday+viewDto.getHoliday();
        this.sleep = this.sleep+viewDto.getSleep();
        this.chat = this.chat+viewDto.getChat();
        this.eat = this.eat+viewDto.getEat();
        this.play = this.play+viewDto.getPlay();
        this.noBook = this.noBook+viewDto.getNoBook();
        this.playPhone = this.playPhone+viewDto.getPlayPhone();
        this.noClothes = this.noClothes+viewDto.getNoClothes();
    }
}
