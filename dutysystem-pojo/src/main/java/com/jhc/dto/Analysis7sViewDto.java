package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.jhc.entity.Ise7sInspect;
import lombok.Data;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/18 10:06
 */
@Data
public class Analysis7sViewDto {

    /**
     * 实训室id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long roomId;

    /**
     * 实训室名称
     */
    private String roomName;

    /**
     * 物品整理
     */
    private Integer arrange = 0;

    /**
     * 定位整顿
     */
    private Integer rectify = 0;

    /**
     * 环境清扫
     */
    private Integer clear = 0;

    /**
     * 设备报修
     */
    private Integer repair = 0;

    /**
     * 安全警示
     */
    private Integer safe = 0;

    /**
     * 耗材浪费
     */
    private Integer economize = 0;

    /**
     * 通报表扬
     */
    private Integer praise = 0;


    public void addAnalysis(Ise7sInspect ise7sInspect){
        if (ise7sInspect.getArrange()){
            this.arrange++;
        }
        if (ise7sInspect.getClear()){
            this.clear++;
        }
        if (ise7sInspect.getEconomize()){
            this.economize++;
        }
        if (ise7sInspect.getPraise()){
            this.praise++;
        }
        if (ise7sInspect.getRectify()){
            this.rectify++;
        }
        if (ise7sInspect.getSafe()){
            this.safe++;
        }
        if (ise7sInspect.getRepair()){
            this.repair++;
        }
    }

    public void addAnalysisAll(Analysis7sViewDto viewDto){
        this.arrange = this.arrange+viewDto.getArrange();
        this.clear = this.clear+viewDto.getClear();
        this.economize = this.economize+viewDto.getEconomize();
        this.praise = this.praise+viewDto.getPraise();
        this.rectify = this.rectify+viewDto.getRectify();
        this.safe = this.safe+viewDto.getSafe();
        this.repair = this.repair+viewDto.getRepair();
    }
}
