package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/9 14:00
 */
@Data
public class FeedBackAddDto {

    /**
     *检查id
     */
    @ApiModelProperty("检查id")
    @JsonSerialize(using = ToStringSerializer.class)
    @NotNull(message = "检查id不能为空")
    private Long id;

    /**
     * 整改结果 问题反馈及解决情况(反馈)
     */
    @ApiModelProperty("反馈")
    @NotBlank(message = "反馈不能为空")
    private String result;
}
