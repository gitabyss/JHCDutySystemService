package com.jhc.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author zhuzhixu
 */
@Data
@ApiModel
public class BacTimeTableSearchDto {
    @ApiModelProperty("课程代码")
    private String courseNumber;

    @ApiModelProperty("课程名称")
    private String courseTitle;

    @ApiModelProperty("教学班名称")
    private String teachingClassName;

    @ApiModelProperty("上课时间起始日期")
    private Date beginDate;

    @ApiModelProperty("上课时间结束日期")
    private Date endDate;

    @ApiModelProperty("教师名字")
    private String name;
}
