package com.jhc.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhuzhixu
 */
@Data
@ApiModel
public class BacCollegeViewDto {
    @ApiModelProperty("序号")
    private Long id;
    @ApiModelProperty("部门编号")
    private String number;
    @ApiModelProperty("部门名称")
    private String name;
    @ApiModelProperty("隶属部门名字")
    private String parentName;
}
