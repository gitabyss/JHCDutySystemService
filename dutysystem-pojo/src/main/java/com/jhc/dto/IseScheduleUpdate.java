package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/1 13:46
 */
@Data
public class IseScheduleUpdate {


    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty("值班表id")
    private Long id;

    /**
     * 值班领导id
     */
    @NotBlank
    @ApiModelProperty("值班领导")
    private String leaderNumber;

    /**
     * 值班中层id
     */
    @NotBlank
    @ApiModelProperty("值班中层")
    private String middleNumber;

    /**
     * 值班班主任id
     */
    @NotBlank
    @ApiModelProperty("值班班主任1")
    private String dutyOneNumber;

    /**
     * 值班班主任2id
     */
    @NotBlank
    @ApiModelProperty("值班班主任2")
    private String dutyTwoNumber;

    /**
     * 值班辅导员
     */
    @NotBlank
    @ApiModelProperty("值班辅导员")
    private String instructorNumber;

    /**
     * 计划
     */
    @ApiModelProperty("计划")
    private String plan;

    /**
     * 总结
     */
    @ApiModelProperty("总结")
    private String summary;
}
