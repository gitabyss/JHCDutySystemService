package com.jhc.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/1 11:07
 */
@Data
public class IseScheduleDelete {

    @ApiModelProperty("值班表id列表")
    private List<Long> scheduleIds;
}
