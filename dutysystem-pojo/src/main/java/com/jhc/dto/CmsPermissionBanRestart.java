package com.jhc.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @Author: zfm
 * @Date: 2019/11/14 10:13
 */
@Data
public class CmsPermissionBanRestart {

    @NotEmpty(message = "权限ID不能为空")
    private List<Long> permissionId;
}
