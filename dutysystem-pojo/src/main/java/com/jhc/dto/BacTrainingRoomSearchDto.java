package com.jhc.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenkexin
 */
@Data
@ApiModel
public class BacTrainingRoomSearchDto {
    @ApiModelProperty("实训中心编号")
    private String centerId;

    @ApiModelProperty("实训室名称")
    private String roomName;

    @ApiModelProperty("管理负责人名称")
    private String responsibleName;
}
