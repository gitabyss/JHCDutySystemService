package com.jhc.dto;

import lombok.Data;

import java.util.Date;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/13 10:27
 */
@Data
public class InspectionCreateDto extends IseInspectionAddDto {

    public InspectionCreateDto() {
    }

    public InspectionCreateDto(Integer week, Date nowDate, String username, Integer type) {
        this.week = week;
        this.nowDate = nowDate;
        this.username = username;
        this.type = type;
    }

    /**
     * 周次
     */
    private Integer week;

    /**
     * 日期
     */
    private Date nowDate;

    /**
     * 账号
     */
    private String username;

    /**
     * 检查类型
     */
    private Integer type;
}
