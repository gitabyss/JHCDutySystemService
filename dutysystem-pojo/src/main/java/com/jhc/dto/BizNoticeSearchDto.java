package com.jhc.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import com.jhc.bo.PageDetails;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @Author: zfm
 * @Date: 2019/12/11 10:02
 */
@Data
public class BizNoticeSearchDto extends PageDetails {
    @NotNull(message = "发送人不能不填")
    private String adminUsername;
    @NotNull(message = "标题不能不填")
    private String title;
    @ApiModelProperty(name = "不填代表查询全部，true：已读，false：未读")
    private Boolean isRead;
    @ApiModelProperty(name = "开始时间")
    private Date startTime;
    @ApiModelProperty(name = "结束时间")
    private Date endTime;
}
