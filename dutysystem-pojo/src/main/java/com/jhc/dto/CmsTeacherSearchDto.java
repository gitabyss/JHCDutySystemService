package com.jhc.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhuzhixu
 */
@Data
@ApiModel
public class CmsTeacherSearchDto {
    @ApiModelProperty("所属学院编号")
    private String collegeNumber;
    @ApiModelProperty("所属部门编号")
    private String departmentNumber;
    @ApiModelProperty("姓名")
    private String name;
    @ApiModelProperty("职工号")
    private String number;
}
