package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Author: zfm
 * @Date: 2019/12/11 08:58
 */
@Data
public class BizCheckMessageDto {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    private String adminName;

    /**
     * 标题
     */
    private String title;
    /**
     * 内容
     */
    private String content;
    /**
     * 管理员id
     */
    private String adminNumber;

    /**
     * 用户类型10:学生 20:老师
     */
    private Integer userType;

    /**
     * 用户id
     */
    private String userNumber;

    /**
     * 是否已读
     */
    private Boolean isRead;

    /**
     * 阅读时间
     */
    private LocalDateTime readTime;
}
