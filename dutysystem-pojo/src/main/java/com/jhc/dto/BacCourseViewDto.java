package com.jhc.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhuzhixu
 */
@Data
@ApiModel
public class BacCourseViewDto {
    @ApiModelProperty("编号")
    private Long id;
    /**
     * 课程代码
     */
    @ApiModelProperty("课程代码")
    private String number;

    /**
     * 课程名称
     */
    @ApiModelProperty("课程名字")
    private String name;

    /**
     * 是否启用
     */
    @ApiModelProperty("是否启用")
    private Boolean isEnabled;
}
