package com.jhc.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/8 19:57
 */
@Data
public class DetailTeachViewDto {

    /**
     *检查id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 周次
     */
    private Integer week;

    /**
     * 检查日期
     */
    @ApiModelProperty("检查日期")
    @JsonFormat(timezone = "GMT+8")
    private Date dutyDate;

    /**
     * 值班人员
     */
    @ApiModelProperty("值班人员编号")
    private String dutyNumber;

    /**
     * 值班人员
     */
    @ApiModelProperty("值班人员名字")
    private String dutyName;

    /**
     * 课程名称
     */
    @ApiModelProperty("课程名称")
    private String courseTitle;

    /**
     * 教学班名称
     */
    @ApiModelProperty("教学班名称")
    private String teachClassName;

    /**
     * 上课教师姓名
     */
    @ApiModelProperty("上课教师姓名")
    private String teacherName;

    /**
     * 授课老师编号
     */
    @ApiModelProperty("授课老师编号")
    private String teacherNumber;

    /**
     * 教室名称
     */
    @ApiModelProperty("教室名称")
    private String roomName;

    /**
     * 其他相关老师 单个或多个老师
     */
    private String teachersNumbers;

    /**
     * 迟到
     */
    private Integer late;

    /**
     * 旷课
     */
    private Integer truancy;

    /**
     * 请假
     */
    private Integer holiday;

    /**
     * 睡觉
     */
    private Boolean sleep;

    /**
     * 交头接耳
     */
    private Boolean chat;

    /**
     * 吃零食
     */
    private Boolean eat;

    /**
     * 游戏
     */
    private Boolean play;

    /**
     * 不带课本
     */
    private Boolean noBook;

    /**
     * 玩手机
     */
    private Boolean playPhone;

    /**
     * 不穿工作服
     */
    private Boolean noClothes;

    /**
     * 学生其他信息
     */
    private String stuOther;

    /**
     * 读ppt
     */
    private Boolean ppt;

    /**
     * 无互动
     */
    private Boolean noInteract;

    /**
     * 不写板书
     */
    private Boolean noWrite;

    /**
     * 课堂管理欠佳
     */
    private Boolean discipline;

    /**
     * 迟到早退
     */
    private Boolean lateLeave;

    /**
     * 坐着上课
     */
    private Boolean sit;

    /**
     * 老师其他信息
     */
    private String teaOther;

    /**
     * 图片
     */
    private String image;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 整改要求 检查结果及存在问题
     */
    private String requires;

    /**
     * 整改结果 问题反馈及解决情况
     */
    private String result;

    /**
     * 反馈状态 表示是/否反馈和已反馈/待反馈
     */
    private Integer feedbackStatus;
}
