package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author: zfm
 * @Date: 2019/12/10 18:50
 */
@Data
public class CheckNoticeAddDto {

    /**
     * 管理员id
     */
    @NotBlank(message = "发送人ID不能为空")
    private String adminNumber;

    /**
     * 标题
     */
    @NotBlank(message = "消息标题不能为空")
    private String title;

    /**
     * 内容
     */
    @NotBlank(message = "内容不能为空")
    private String content;

    /**
     * 抄送人ID
     */
    @NotEmpty(message = "抄送人不能为空")
    private List<String> staffNumberList;

    /**
     * 检查ID
     */
    @NotNull(message = "关联检查ID不能为空")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long checkId;

}
