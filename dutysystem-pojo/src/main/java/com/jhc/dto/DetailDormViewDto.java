package com.jhc.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/8 19:14
 */
@Data
public class DetailDormViewDto {

    /**
     *检查id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 周次
     */
    private Integer week;

    /**
     * 检查日期
     */
    @ApiModelProperty("检查日期")
    @JsonFormat(timezone = "GMT+8")
    private Date dutyDate;

    /**
     * 值班人员
     */
    @ApiModelProperty("值班人员编号")
    private String dutyNumber;

    /**
     * 值班人员
     */
    @ApiModelProperty("值班人员名字")
    private String dutyName;

    /**
     * 房间号
     */
    @ApiModelProperty("房间号")
    private Integer room;

    /**
     * 楼幢
     */
    @ApiModelProperty("楼幢")
    private String buildings;

    /**
     * 辅导员number
     */
    @ApiModelProperty("辅导员number")
    private String instructorNumber;

    /**
     * 辅导员name
     */
    @ApiModelProperty("辅导员name")
    private String instructorName;

    /**
     * 所属班级
     */
    @ApiModelProperty("所属班级")
    private String classes;

    /**
     * 班主任
     */
    @ApiModelProperty("班主任")
    private String classTeacheres;

    /**
     * 其他相关老师 单个或多个老师
     */
    private String teachersNumbers;

    /**
     * 地面不整洁，垃圾未清理
     */
    private Boolean floor;

    /**
     * 洗漱台无序，水槽有污垢
     */
    private Boolean pentrough;

    /**
     * 便池不清洁，空气有异味
     */
    private Boolean toilet;

    /**
     * 被子未叠正，床帷未拉开
     */
    private Boolean bed;

    /**
     * 桌面较凌乱，书柜欠整齐
     */
    private Boolean desk;

    /**
     * 衣服未挂齐，鞋子乱摆放
     */
    private Boolean clothes;

    /**
     * 阳台堆杂物，门窗有积灰
     */
    private Boolean balcony;

    /**
     * 大功率
     */
    private Boolean electrical;

    /**
     * 宠物
     */
    private Boolean pet;

    /**
     * 刀具
     */
    private Boolean knife;

    /**
     * 酒瓶
     */
    private Boolean bottle;

    /**
     *夜不归宿
     */
    @ApiModelProperty("夜不归宿")
    private Boolean night;

    /**
     * 图片
     */
    private String image;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 整改要求 检查结果及存在问题
     */
    private String requires;

    /**
     * 整改结果 问题反馈及解决情况
     */
    private String result;

    /**
     * 反馈状态 表示是/否反馈和已反馈/待反馈
     */
    private Integer feedbackStatus;
}
