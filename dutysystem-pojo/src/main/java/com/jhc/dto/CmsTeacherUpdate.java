package com.jhc.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @Author: zfm
 * @Date: 2020/1/6 09:17
 */
@Data
public class CmsTeacherUpdate {

    @NotBlank(message = "用户名称不能为空")
    private String name;

    @NotNull(message = "性别不能为空")
    private Integer sex;

    @NotBlank(message = "学院编号不能为空")
    private String collegeNumber;

    @NotBlank(message = "部门编号不能为空")
    private String departmentNumber;
}
