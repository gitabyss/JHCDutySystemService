package com.jhc.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhuzhixu
 */
@Data
@ApiModel
public class BacTeachingClassViewDto {
    /**
     * 序号
     */
    @ApiModelProperty("序号")
    private Long id;

    /**
     * 教学班名称
     */
    @ApiModelProperty("教学班名称")
    private String teachingClassName;

    /**
     * 授课老师名字
     */
    @ApiModelProperty("授课老师编号")
    private String teacherName;

    /**
     * 授课老师编号
     */
    @ApiModelProperty("授课老师编号")
    private String teacherNumber;


    /**
     * 专业名
     */
    @ApiModelProperty("专业名")
    private String specialty;

    /**
     * 所属学院
     */
    @ApiModelProperty("所属学院")
    private String collegeName;

    /**
     * 启停状态
     */
    @ApiModelProperty("启停状态")
    private Boolean isEnable;
}
