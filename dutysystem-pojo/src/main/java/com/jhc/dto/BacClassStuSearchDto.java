package com.jhc.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhuzhixu
 */
@Data
@ApiModel
public class BacClassStuSearchDto {
    /**
     * 姓名
     */
    @ApiModelProperty("名字")
    private String studentName;

    /**
     * 行政班编号
     */
    @ApiModelProperty("行政班编号")
    private Long classId;

    /**
     * 学号
     */
    @ApiModelProperty("学号")
    private String studentNumber;

    /**
     * 专业编号
     */
    @ApiModelProperty("专业编号")
    private String specialtyNumber;

    /**
     * 所属学院
     */
    @ApiModelProperty("所属学院编号")
    private String collegeNumber;

    /**
     * 启停状态
     */
    @ApiModelProperty("启停状态 0->启用 1->停用")
    private Boolean isEnable;

}
