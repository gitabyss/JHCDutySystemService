package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.jhc.entity.IseDormInspect;
import lombok.Data;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/16 15:11
 */
@Data
public class AnalysisDormViewDto {

    /**
     * 班级id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long classId;

    /**
     * 班级名称
     */
    private String className;



    /**
     * 地面不整洁，垃圾未清理
     */
    private Integer floor = 0;

    /**
     * 洗漱台无序，水槽有污垢
     */
    private Integer pentrough = 0;

    /**
     * 便池不清洁，空气有异味
     */
    private Integer toilet = 0;

    /**
     * 被子未叠正，床帷未拉开
     */
    private Integer bed = 0;

    /**
     * 桌面较凌乱，书柜欠整齐
     */
    private Integer desk = 0;

    /**
     * 衣服未挂齐，鞋子乱摆放
     */
    private Integer clothes = 0;

    /**
     * 阳台堆杂物，门窗有积灰
     */
    private Integer balcony = 0;

    /**
     * 大功率
     */
    private Integer electrical = 0;

    /**
     * 宠物
     */
    private Integer pet = 0;

    /**
     * 刀具
     */
    private Integer knife = 0;

    /**
     * 酒瓶
     */
    private Integer bottle = 0;

    /**
     *夜不归宿
     */
    private Integer night = 0;

    public void addAnalysis(IseDormInspect dormInspect){
        if (dormInspect.getBottle()){
            this.bottle++;
        }
        if (dormInspect.getPet()){
            this.pet++;
        }
        if (dormInspect.getKnife()){
            this.knife++;
        }
        if (dormInspect.getElectrical()){
            this.electrical++;
        }
        if (dormInspect.getBalcony()){
            this.balcony++;
        }
        if (dormInspect.getClothes()){
            this.clothes++;
        }
        if (dormInspect.getDesk()){
            this.desk++;
        }
        if (dormInspect.getBed()){
            this.bed++;
        }
        if (dormInspect.getToilet()){
            this.toilet++;
        }
        if (dormInspect.getPentrough()){
            this.pentrough++;
        }
        if (dormInspect.getFloor()){
            this.floor++;
        }
        if (dormInspect.getNight()){
            this.night++;
        }
    }

    public Integer getDormHygiene(){
        Integer sum = 0;
        sum = floor+pentrough+toilet+bed+desk+clothes+balcony;
        return sum;
    }
}
