package com.jhc.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhuzhixu
 */
@Data
@ApiModel
public class BacClassSearchDto {
    /**
     * 班级名称
     */
    @ApiModelProperty("班级名称")
    private String name;

    /**
     * 专业编号
     */
    @ApiModelProperty("专业编号")
    private String majorNumber;

    /**
     * 学院编号
     */
    @ApiModelProperty("学院编号")
    private String collegeNumber;

    /**
     * 是否启用 1->启用 0->不启用
     */
    @ApiModelProperty("是否启用")
    private Boolean isEnabled;

}
