package com.jhc.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhuzhixu
 */
@Data
@ApiModel
public class BacCourseSearchDto {
    /**
     * 课程代码
     */
    @ApiModelProperty("课程代码")
    private String number;

    /**
     * 课程名称
     */
    @ApiModelProperty("课程名字")
    private String name;

}
