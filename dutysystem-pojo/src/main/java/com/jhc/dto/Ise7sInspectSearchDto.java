package com.jhc.dto;

import com.jhc.bo.PageDetails;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/5 8:58
 */
@Data
public class Ise7sInspectSearchDto extends PageDetails {

    /**
     * 管理责任人姓名
     */
    @ApiModelProperty("管理责任人姓名")
    private String responsibleName;

    /**
     * 实训室名称
     */
    @ApiModelProperty("实训室名称")
    private String roomName;

    /**
     * 是否填写
     */
    @ApiModelProperty("是否填写 false->未填写 true->已填写")
    private Boolean isWrited;
}
