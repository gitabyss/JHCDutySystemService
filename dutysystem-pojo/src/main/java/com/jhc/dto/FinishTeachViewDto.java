package com.jhc.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/8 13:25
 */
@Data
public class FinishTeachViewDto {

    /**
     *检查id
     */
    @ApiModelProperty("检查总表id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 检查日期
     */
    @ApiModelProperty("检查日期")
    @JsonFormat(timezone = "GMT+8")
    private Date dutyDate;

    /**
     * 周次
     */
    @ApiModelProperty("周次")
    private Integer week;

    /**
     * 星期
     */
    @ApiModelProperty("星期")
    private Integer weekDay;

    /**
     * 节次
     */
    @ApiModelProperty("节次")
    private String section;

    /**
     * 课程名称
     */
    @ApiModelProperty("课程名称")
    private String courseTitle;

    /**
     * 教学班名称
     */
    @ApiModelProperty("教学班名称")
    private String teachClassName;

    /**
     * 上课教师姓名
     */
    @ApiModelProperty("上课教师姓名")
    private String teacherName;

    /**
     * 授课老师编号
     */
    @ApiModelProperty("授课老师编号")
    private String teacherNumber;

    /**
     * 教室名称
     */
    @ApiModelProperty("教室名称")
    private String roomName;

    /**
     * 值班人员
     */
    @ApiModelProperty("值班人员编号")
    private String dutyNumber;

    /**
     * 值班人员
     */
    @ApiModelProperty("值班人员名字")
    private String dutyName;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private Date createTime;

    /**
     * 迟到
     */
    private Integer late;

    /**
     * 旷课
     */
    private Integer truancy;

    /**
     * 请假
     */
    private Integer holiday;

    /**
     * 睡觉
     */
    private Boolean sleep;

    /**
     * 整改要求 检查结果及存在问题
     */
    private String requires;

    /**
     * 整改结果 问题反馈及解决情况
     */
    private String result;

    /**
     * 反馈状态 表示是/否反馈和已反馈/待反馈
     */
    private Integer feedbackStatus;
}
