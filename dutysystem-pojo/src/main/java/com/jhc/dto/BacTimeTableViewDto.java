package com.jhc.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author zhuzhixu
 */
@Data
@ApiModel
public class BacTimeTableViewDto {
    @ApiModelProperty("课程id")
    private Long id;

    @ApiModelProperty("课程代码")
    private String courseNumber;

    @ApiModelProperty("课程名称")
    private String courseTitle;

    @ApiModelProperty("教学班名称")
    private String teachingClassName;

    @ApiModelProperty("周次")
    private Long weekDay;

    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    private Date classDate;
    private Long day;

    @ApiModelProperty("上课节数")
    private String section;

    @ApiModelProperty("教师名字")
    private String name;

    @ApiModelProperty("教师职工号")
    private String teacherNumber;
}
