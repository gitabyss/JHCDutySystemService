package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/10 8:52
 */
@Data
public class Ise7sInspectAddDto extends IseInspectionAddDto {

    /**
     * 整理
     */
    @ApiModelProperty("整理")
    private Boolean arrange;

    /**
     * 整顿
     */
    @ApiModelProperty("整顿")
    private Boolean rectify;

    /**
     * 清扫
     */
    @ApiModelProperty("清扫")
    private Boolean clear;

    /**
     * 报修
     */
    @ApiModelProperty("报修")
    private Boolean repair;

    /**
     * 安全
     */
    @ApiModelProperty("安全")
    private Boolean safe;

    /**
     * 节约
     */
    @ApiModelProperty("节约")
    private Boolean economize;

    /**
     * 表扬
     */
    @ApiModelProperty("表扬")
    private Boolean praise;
}
