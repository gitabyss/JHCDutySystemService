package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/17 8:29
 */
@Data
public class AnalysisAllViewDto {

    /**
     * 班级id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long classId;

    /**
     * 班级名称
     */
    private String className;

    /**
     *夜不归宿
     */
    private Integer night = 0;

    /**
     * 大功率
     */
    private Integer electrical = 0;

    /**
     * 宠物
     */
    private Integer pet = 0;

    /**
     * 刀具
     */
    private Integer knife = 0;

    /**
     * 寝室卫生
     */
    private Integer dormHygiene = 0;

    /**
     * 学生自习状态
     */
    private Integer studyState = 0;

    /**
     * 学生听课状态
     */
    private Integer classingState = 0;

    /**
     * 迟到
     */
    private Integer late = 0;

    /**
     * 旷课
     */
    private Integer truancy = 0;

    /**
     * 请假
     */
    private Integer holiday = 0;
}
