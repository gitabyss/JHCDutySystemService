package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.jhc.entity.IseTeachInspect;
import lombok.Data;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/18 14:31
 */
@Data
public class AnalysisTeachTeacherViewDTo {

    /**
     * 教学班id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long teachClassId;

    /**
     * 教学班名称
     */
    private String teachClassName;

    /**
     * 读ppt
     */
    private Integer ppt = 0;

    /**
     * 无互动
     */
    private Integer noInteract = 0;

    /**
     * 不写板书
     */
    private Integer noWrite = 0;

    /**
     * 课堂管理欠佳
     */
    private Integer discipline = 0;

    /**
     * 迟到早退
     */
    private Integer lateLeave = 0;

    /**
     * 坐着上课
     */
    private Integer sit = 0;

    public void addAnalysis(IseTeachInspect teachInspect){
        if (teachInspect.getPpt()){
            this.ppt++;
        }
        if (teachInspect.getNoInteract()){
            this.noInteract++;
        }
        if (teachInspect.getNoWrite()){
            this.noWrite++;
        }
        if (teachInspect.getDiscipline()){
            this.discipline++;
        }
        if (teachInspect.getLateLeave()){
            this.lateLeave++;
        }
        if (teachInspect.getSit()){
            this.sit++;
        }
    }

    public void addAnalysisAll(AnalysisTeachTeacherViewDTo viewDTo){
        this.ppt = this.ppt + viewDTo.getPpt();
        this.noInteract = this.noInteract + viewDTo.getNoInteract();
        this.noWrite = this.noWrite + viewDTo.getNoWrite();
        this.discipline = this.discipline + viewDTo.getDiscipline();
        this.lateLeave = this.lateLeave + viewDTo.getLateLeave();
        this.sit = this.sit + viewDTo.getSit();
    }
}
