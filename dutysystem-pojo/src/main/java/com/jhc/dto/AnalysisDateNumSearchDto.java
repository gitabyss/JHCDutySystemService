package com.jhc.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/18 16:41
 */
@Data
public class AnalysisDateNumSearchDto extends AnalysisAllSearchDto {

    @ApiModelProperty("课程编号")
    private String number;
}
