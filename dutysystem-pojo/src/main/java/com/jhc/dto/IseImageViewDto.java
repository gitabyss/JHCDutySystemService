package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/11 8:28
 */
@Data
public class IseImageViewDto {

    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 地址
     */
    @ApiModelProperty("图片地址")
    private String address;
}
