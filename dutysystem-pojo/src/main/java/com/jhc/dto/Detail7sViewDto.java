package com.jhc.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/8 16:02
 */
@Data
public class Detail7sViewDto {


    /**
     *检查id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 周次
     */
    private Integer week;

    /**
     * 检查日期
     */
    @ApiModelProperty("检查日期")
    @JsonFormat(timezone = "GMT+8")
    private Date dutyDate;

    /**
     * 管理责任人(实验员)姓名
     */
    @ApiModelProperty("管理责任人(实验员)姓名")
    private String responsibleName;

    /**
     * 管理责任人(实验员)编号
     */
    @ApiModelProperty("管理责任人(实验员)编号")
    private String responsibleNumber;

    /**
     * 值班人员
     */
    @ApiModelProperty("值班人员编号")
    private String dutyNumber;

    /**
     * 值班人员
     */
    @ApiModelProperty("值班人员名字")
    private String dutyName;

    /**
     * 实训室名称
     */
    @ApiModelProperty("实训室名称")
    private String roomName;

    /**
     * 其他相关老师 单个或多个老师
     */
    private String teachersNumbers;

    /**
     * 整理
     */
    private Boolean arrange;

    /**
     * 整顿
     */
    private Boolean rectify;

    /**
     * 清扫
     */
    private Boolean clear;

    /**
     * 报修
     */
    private Boolean repair;

    /**
     * 安全
     */
    private Boolean safe;

    /**
     * 节约
     */
    private Boolean economize;

    /**
     * 表扬
     */
    private Boolean praise;

    /**
     * 图片
     */
    private String image;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 整改要求 检查结果及存在问题
     */
    private String requires;

    /**
     * 整改结果 问题反馈及解决情况
     */
    private String result;

    /**
     * 反馈状态 表示是/否反馈和已反馈/待反馈
     */
    private Integer feedbackStatus;
}
