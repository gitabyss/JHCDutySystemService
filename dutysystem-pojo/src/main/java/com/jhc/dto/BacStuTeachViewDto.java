package com.jhc.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhuzhixu
 */
@ApiModel
@Data
public class BacStuTeachViewDto {
    @ApiModelProperty("序号")
    private Long id;
    @ApiModelProperty("学号")
    private String studentNumber;
    @ApiModelProperty("学生姓名")
    private String studentName;
    @ApiModelProperty("学生性别")
    private Integer sex;
    @ApiModelProperty("教学班名字")
    private String teachingClassName;
    @ApiModelProperty("授课老师名字")
    private String teacherName;
    @ApiModelProperty("授课老师职工号")
    private String teacherNumber;
    @ApiModelProperty("专业名字")
    private String specialty;
    @ApiModelProperty("学院名字")
    private String collegeName;
    @ApiModelProperty("启停状态")
    private Boolean isEnable;
    @ApiModelProperty("学籍状态")
    private Boolean isGraduate;
}
