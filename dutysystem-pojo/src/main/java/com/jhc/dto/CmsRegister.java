package com.jhc.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @Author: zfm
 * @Date: 2019/11/13 10:37
 */
@Data
public class CmsRegister {
    /**
     * 职工号 职工号
     */
    @NotBlank(message = "职工号不能为空")
    @Size(min = 6,max = 12,message = "职工号必须多于6位少于12位")
    private String number;

    /**
     * 密码 密码，加密
     */
    private String password;

    @NotNull(message = "身份不能为空")
    private Integer type;

    @NotBlank(message = "名字不能为空")
    private String name;

    @NotNull(message = "性别不能为空")
    private Integer sex;

    @NotBlank(message = "学院编号不能为空")
    private String collegeNumber;

    @NotBlank(message = "部门编号不能为空")
    private String departmentNumber;

}
