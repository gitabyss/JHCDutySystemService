package com.jhc.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenkexin
 */
@Data
@ApiModel
public class BacTrainingCenterSearchDto {
    @ApiModelProperty("实训中心名称")
    private String centerName;

    @ApiModelProperty("实训中心主任名称")
    private String directorName;
}
