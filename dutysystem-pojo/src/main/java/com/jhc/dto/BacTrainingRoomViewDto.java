package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chenkexin
 */
@Data
@ApiModel
public class BacTrainingRoomViewDto {
    @ApiModelProperty("编号")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty("实训室名称")
    private String roomName;

    @ApiModelProperty("实训中心名称")
    private String centerName;

    @ApiModelProperty("服务专业")
    private String professionalName;

    @ApiModelProperty("人数容量")
    private Integer peopleNumber;

    @ApiModelProperty("管理负责人名称")
    private String responsibleName;

    @ApiModelProperty("管理负责人编号")
    private String responsibleNumber;
}
