package com.jhc.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 教学检查的检查列表dto
 *
 * @Author: xiaxinlin
 * @Date: 2019/12/3 9:20
 */
@Data
public class IseTeaInspectViewDto {

    /**
     *课表id
     */
    @ApiModelProperty("课表id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty("检查总表id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long inspectId;

    /**
     * 周次
     */
    @ApiModelProperty("周次")
    private Integer week;

    /**
     * 上课日期
     */
    @ApiModelProperty("上课日期")
    @JsonFormat(timezone = "GMT+8")
    private Date classDate;

    /**
     * 星期
     */
    @ApiModelProperty("星期")
    private Integer weekDay;

    /**
     * 节次
     */
    @ApiModelProperty("节次")
    private String section;

    /**
     * 课程名称
     */
    @ApiModelProperty("课程名称")
    private String courseTitle;

    /**
     * 教学班名称
     */
    @ApiModelProperty("教学班名称")
    private String teachClassName;

    /**
     * 上课教师姓名
     */
    @ApiModelProperty("上课教师姓名")
    private String teacherName;

    /**
     * 授课老师编号
     */
    @ApiModelProperty("授课老师编号")
    private String teacherNumber;

    /**
     * 教室名称
     */
    @ApiModelProperty("教室名称")
    private String roomName;

    /**
     * 是否填写
     */
    @ApiModelProperty("是否填写 false->未填写 true->已填写")
    private Boolean isWrited=false;
}
