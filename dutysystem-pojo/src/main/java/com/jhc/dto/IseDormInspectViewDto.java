package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.jhc.bo.TeacherDetail;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/4 9:54
 */
@Data
public class IseDormInspectViewDto {

    /**
     *寝室id
     */
    @ApiModelProperty("寝室id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    @ApiModelProperty("检查总表寝室id")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long inspectId;

    /**
     * 周次
     */
    @ApiModelProperty("周次")
    private Integer week;

    /**
     * 房间号
     */
    @ApiModelProperty("房间号")
    private Integer room;

    /**
     * 楼幢
     */
    @ApiModelProperty("楼幢")
    private String buildings;

    /**
     * 辅导员number
     */
    @ApiModelProperty("辅导员number")
    private String instructorNumber;

    /**
     * 辅导员name
     */
    @ApiModelProperty("辅导员name")
    private String instructorName;

    /**
     * 所属班级
     */
    @ApiModelProperty("所属班级")
    private String classes;

    /**
     * 班主任
     */
    @ApiModelProperty("班主任")
    private String classTeacheres;

    /**
     * 是否填写
     */
    @ApiModelProperty("是否填写 false->未填写 true->已填写")
    private Boolean isWrited=false;
}
