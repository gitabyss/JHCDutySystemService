package com.jhc.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhuzhixu
 */
@Data
public class BacStuTeachMappingDto {
    @ApiModelProperty("序号")
    private Long id;
    @ApiModelProperty("学号")
    private String studentNumber;
    @ApiModelProperty("学生姓名")
    private String name;
    @ApiModelProperty("学生性别")
    private Integer sex;
    @ApiModelProperty("教学班名字")
    private String teachingClassName;
    @ApiModelProperty("授课老师编号")
    private String teacherNumber;
    @ApiModelProperty("专业编号")
    private String specialtyNumber;
    @ApiModelProperty("学院编号")
    private String collegeNumber;
    @ApiModelProperty("启停状态")
    private Boolean isEnable;
    @ApiModelProperty("学籍状态")
    private Boolean isGraduate;
}
