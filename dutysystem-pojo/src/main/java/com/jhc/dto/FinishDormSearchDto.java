package com.jhc.dto;

import com.jhc.bo.PageDetails;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/6 14:15
 */
@Data
public class FinishDormSearchDto extends PageDetails {

    /**
     * 开始日期
     */
    @ApiModelProperty("开始日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    /**
     * 结束日期
     */
    @ApiModelProperty("结束日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;


    /**
     * 房间号
     */
    @ApiModelProperty("房间号")
    private Integer room;

    /**
     * 楼幢
     */
    @ApiModelProperty("楼幢")
    private String buildings;

    /**
     * 辅导员
     */
    @ApiModelProperty("辅导员")
    private String instructorName;


}
