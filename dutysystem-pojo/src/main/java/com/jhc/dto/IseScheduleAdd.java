package com.jhc.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhc.entity.IseSchedule;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @Author: xiaxinlin
 * @Date: 2019/11/30 14:35
 */
@Data
public class IseScheduleAdd{




    /**
     * 学期id
     */
    @NotNull(message = "学期不能为空")
    @ApiModelProperty("学期id")
    private Long termId;

    /**
     * 周次id
     */
    @NotNull(message = "周次不能为空")
    @ApiModelProperty("周次id")
    private Long weekDayId;

    /**
     * 周次
     */
    @NotNull
    @ApiModelProperty("周次")
    private Integer week;

    /**
     * 值班领导id
     */
    @NotBlank(message = "值班领导不能为空")
    @ApiModelProperty("值班领导number")
    private String leaderNumber;

    /**
     * 值班中层id
     */
    @NotBlank(message = "值班中层不能为空")
    @ApiModelProperty("值班中层number")
    private String middleNumber;

    /**
     * 值班班主任id
     */
    @NotBlank(message = "值班班主任1不能为空")
    @ApiModelProperty("值班班主任number")
    private String dutyOneNumber;

    /**
     * 值班班主任2id
     */
    @NotBlank(message = "值班班主任2不能为空")
    @ApiModelProperty("值班班主任2number")
    private String dutyTwoNumber;

    /**
     * 值班辅导员
     */
    @NotBlank(message = "值班辅导员不能为空")
    @ApiModelProperty("值班辅导员number")
    private String instructorNumber;

    /**
     * 计划
     */
    @ApiModelProperty("计划")
    private String plan;

    /**
     * 总结
     */
    @ApiModelProperty("总结")
    private String summary;

}
