package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 7s检查列表视图dto
 * @Author: xiaxinlin
 * @Date: 2019/12/5 8:55
 */
@Data
public class Ise7sInspectViewDto {

    /**
     * 实训室id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty("实训室id")
    private Long id;

    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty("检查总表id")
    private Long inspectId;

    /**
     * 周次
     */
    @ApiModelProperty("周次")
    private Integer week;

    /**
     * 管理责任人编号
     */
    @ApiModelProperty("管理责任人编号")
    private String responsibleNumber;

    /**
     * 管理责任人姓名
     */
    @ApiModelProperty("管理责任人姓名")
    private String responsibleName;

    /**
     * 实训室名称
     */
    @ApiModelProperty("实训室名称")
    private String roomName;

    /**
     * 是否填写
     */
    @ApiModelProperty("是否填写 false->未填写 true->已填写")
    private Boolean isWrited=false;
}
