package com.jhc.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/8 13:33
 */
@Data
public class AllTeachSearchDto extends FinishTeachSearchDto {

    /**
     * 值班人员
     */
    @ApiModelProperty("值班人员姓名")
    private String dutyName;

    /**
     * 反馈状态 表示是/否反馈和已反馈/待反馈
     * 反馈状态 10->不反馈 20->已反馈 30->待反馈
     */
    @ApiModelProperty("反馈状态 10->不反馈 20->已反馈 30->待反馈")
    private Integer feedbackStatus;
}
