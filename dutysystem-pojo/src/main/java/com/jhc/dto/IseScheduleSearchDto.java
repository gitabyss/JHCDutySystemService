package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.jhc.bo.PageDetails;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/1 16:43
 */
@Data
public class IseScheduleSearchDto extends PageDetails {

    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty("学期id")
    private Long termId;

    /**
     * 周次
     */
    @ApiModelProperty("周次")
    private Integer week;

    /**
     * 值班领导
     */
    @ApiModelProperty(value = "值班领导")
    private String leaderName;

    /**
     * 值班中层
     */
    @ApiModelProperty("值班中层")
    private String middleName;

    /**
     * 值班班主任
     */
    @ApiModelProperty("值班班主任")
    private String dutyOneName;

    /**
     * 值班班主任2
     */
    @ApiModelProperty("值班班主任2")
    private String dutyTwoName;

    /**
     * 值班辅导员
     */
    @ApiModelProperty("值班辅导员")
    private String instructorName;


}
