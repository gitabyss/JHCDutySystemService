package com.jhc.dto;

import com.jhc.bo.PageDetails;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/4 9:53
 */
@Data
public class IseDormInspectSearchDto extends PageDetails {

    /**
     * 房间号
     */
    @ApiModelProperty("房间号")
    private Integer room;

    /**
     * 楼幢
     */
    @ApiModelProperty("楼幢")
    private String buildings;

    /**
     * 辅导员
     */
    @ApiModelProperty("辅导员")
    private String instructorName;

    /**
     * 是否填写
     */
    @ApiModelProperty("是否填写 false->未填写 true->已填写")
    private Boolean isWrited;
}
