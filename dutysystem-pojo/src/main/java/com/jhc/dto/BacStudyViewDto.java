package com.jhc.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhuzhixu
 */
@Data
@ApiModel
public class BacStudyViewDto {
    @ApiModelProperty("班级名字")
    private String className;
    @ApiModelProperty("专业名字")
    private String professionalName;
    @ApiModelProperty("学院名字")
    private String collegeName;
    @ApiModelProperty("教室名字")
    private String roomName;
    @ApiModelProperty("班主任名字")
    private String teacherName;
}
