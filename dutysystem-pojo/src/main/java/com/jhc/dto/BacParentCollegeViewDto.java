package com.jhc.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhuzhixu
 */
@Data
@ApiModel
public class BacParentCollegeViewDto {
    @ApiModelProperty("部门编号")
    private String number;
    @ApiModelProperty("部门名字")
    private String name;
}
