package com.jhc.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jhc.bo.PageDetails;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 教学检查的检查列表查询条件dto
 *
 * @Author: xiaxinlin
 * @Date: 2019/12/3 9:19
 */
@Data
public class IseTeaInspectSearchDto extends PageDetails {

    /**
     * 课程名称
     */
    @ApiModelProperty("课程名称")
    private String courseTitle;

    /**
     * 教学班名称
     */
    @ApiModelProperty("教学班名称")
    private String teachClassName;

    /**
     * 上课教师姓名
     */
    @ApiModelProperty("上课教师姓名")
    private String teacherName;

//    /**
//     * 上课日期
//     */
//    @DateTimeFormat(pattern = "yyyy-MM-dd")     //date类型接收前端传入的该格式的字符串
//    private Date classDate=new Date();

    /**
     * 节次
     */
    @ApiModelProperty("节次")
    private String section;

    /**
     * 教室名称，上课地点
     */
    @ApiModelProperty("上课地点")
    private String roomName;

    /**
     * 是否填写
     */
    @ApiModelProperty("是否填写 false->未填写 true->已填写")
    private Boolean isWrited;
}
