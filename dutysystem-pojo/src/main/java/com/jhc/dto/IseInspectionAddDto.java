package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/13 10:15
 */
@Data
public class IseInspectionAddDto {

    /**
     *检查对象id
     */
    @NotNull
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty("检查对象id")
    private Long id;

    /**
     * 其他相关老师 单个或多个老师
     */
    @ApiModelProperty("其他相关教师")
    private List<String> teacherNumbers;

    /**
     * 图片
     */
    @ApiModelProperty("图片地址")
    private String image;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remarks;

    /**
     * 整改要求 检查结果及存在问题
     */
    @ApiModelProperty("整改要求")
    private String requires;

    /**
     * 反馈状态 表示是/否反馈和已反馈/待反馈
     */
    @NotNull
    @ApiModelProperty("是否反馈 true->反馈 false->不反馈")
    private Boolean feedback;
}
