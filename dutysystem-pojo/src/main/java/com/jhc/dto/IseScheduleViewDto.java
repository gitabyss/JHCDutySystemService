package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/1 16:22
 */
@Data
public class IseScheduleViewDto {


    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty("值班表id")
    private Long id;

    /**
     * 学期名称
     */
    @ApiModelProperty("学期名称")
    private String termName;

    /**
     * 周次
     */
    @ApiModelProperty("周次")
    private Integer week;

    /**
     * 值班领导id
     */
    @ApiModelProperty("值班领导number")
    private String leaderNumber;

    /**
     * 值班中层id
     */
    @ApiModelProperty("值班中层number")
    private String middleNumber;

    /**
     * 值班班主任id
     */
    @ApiModelProperty("值班班主任1number")
    private String dutyOneNumber;

    /**
     * 值班班主任2id
     */
    @ApiModelProperty("值班班主任2number")
    private String dutyTwoNumber;

    /**
     * 值班辅导员id
     */
    @ApiModelProperty("值班辅导员number")
    private String instructorNumber;

    /**
     * 值班领导
     */
    @ApiModelProperty("值班领导name")
    private String leaderName;

    /**
     * 值班中层
     */
    @ApiModelProperty("值班中层name")
    private String middleName;

    /**
     * 值班班主任
     */
    @ApiModelProperty("值班班主任name")
    private String dutyOneName;

    /**
     * 值班班主任2
     */
    @ApiModelProperty("值班班主任2name")
    private String dutyTwoName;

    /**
     * 值班辅导员
     */
    @ApiModelProperty("值班辅导员name")
    private String instructorName;

    /**
     * 值班计划
     */
    private String plan;

    /**
     * 值班总结
     */
    private String summary;


}
