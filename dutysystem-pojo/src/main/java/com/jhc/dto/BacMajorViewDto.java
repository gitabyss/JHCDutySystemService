package com.jhc.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhuzhixu
 */
@Data
@ApiModel
public class BacMajorViewDto {
    @ApiModelProperty("编号")
    private Long id;

    @ApiModelProperty("专业代码")
    private String number;

    @ApiModelProperty("专业名称")
    private String professionalName;

    @ApiModelProperty("专业主任")
    private String directorNumber;

    @ApiModelProperty("所属学院")
    private String collegeNumber;

    @ApiModelProperty("所属部门")
    private String departmentNumber;
}
