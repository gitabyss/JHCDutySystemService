package com.jhc.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/10 16:24
 */
@Data
public class IseStudyInspectUpdateDto {

    /**
     *检查对象id
     */
    @NotNull
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty("检查总表id")
    private Long inspectId;

    /**
     * 其他相关老师 单个或多个老师
     */
    @ApiModelProperty("其他相关教师")
    private List<String> teacherNumbers;

    /**
     * 图片
     */
    @ApiModelProperty("图片地址")
    private String imageAddress;

    /**
     * 备注
     */
    @ApiModelProperty("备注")
    private String remarks;

    /**
     * 整改要求 检查结果及存在问题
     */
    @ApiModelProperty("整改要求")
    private String requires;

//    /**
//     * 反馈状态 表示是/否反馈和已反馈/待反馈
//     */
//    @NotNull
//    @ApiModelProperty("是否反馈 true->反馈 false->不反馈")
//    private Boolean feedback;

    /**
     * 旷课学生数
     */
    @NotNull
    @ApiModelProperty("旷课学生数")
    private Integer truancy;

    /**
     * 请公假人数
     */
    @NotNull
    @ApiModelProperty("请公假人数")
    private Integer publicHoliday;

    /**
     * 请私假人数
     */
    @NotNull
    @ApiModelProperty("请私假人数")
    private Integer privateHoliday;

    /**
     * 学生自习状态   10->多数人在学习 20->少数人在学习 30->基本不在学习 40->主题班会讲座
     */
    @NotNull
    @ApiModelProperty("学生自习状态   10->多数人在学习 20->少数人在学习 30->基本不在学习 40->主题班会讲座")
    private Integer studyState;
}
