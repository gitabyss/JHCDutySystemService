package com.jhc.dto;

import com.jhc.bo.PageDetails;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/8 10:51
 */
@Data
public class AllDormSearchDto extends PageDetails {

    /**
     * 开始日期
     */
    @ApiModelProperty("开始日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    /**
     * 结束日期
     */
    @ApiModelProperty("结束日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;


    /**
     * 房间号
     */
    @ApiModelProperty("房间号")
    private Integer room;

    /**
     * 楼幢
     */
    @ApiModelProperty("楼幢")
    private String buildings;

    /**
     * 辅导员
     */
    @ApiModelProperty("辅导员")
    private String instructorName;

    /**
     * 值班人员
     */
    @ApiModelProperty("值班人员姓名")
    private String dutyName;

    /**
     * 反馈状态 表示是/否反馈和已反馈/待反馈
     * 反馈状态 10->不反馈 20->已反馈 30->待反馈
     */
    @ApiModelProperty("反馈状态 10->不反馈 20->已反馈 30->待反馈")
    private Integer feedbackStatus;
}
