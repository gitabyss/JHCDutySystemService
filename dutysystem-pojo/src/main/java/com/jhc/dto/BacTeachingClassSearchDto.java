package com.jhc.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zhuzhixu
 */
@Data
@ApiModel
public class BacTeachingClassSearchDto {
    /**
     * 教学班名称
     */
    @ApiModelProperty("教学班名称")
    private String teachingClassName;

    /**
     * 所属专业编号
     */
    @ApiModelProperty("所属专业编号")
    private String specialtyNumber;

    /**
     * 所属学院编号
     */
    @ApiModelProperty("所属学院编号")
    private String collegeNumber;

}
