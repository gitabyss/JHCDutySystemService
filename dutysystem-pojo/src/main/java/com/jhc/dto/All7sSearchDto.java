package com.jhc.dto;

import com.jhc.bo.PageDetails;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/8 9:56
 */
@Data
public class All7sSearchDto extends PageDetails {

    /**
     * 开始日期
     */
    @ApiModelProperty("开始日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    /**
     * 结束日期
     */
    @ApiModelProperty("结束日期")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    /**
     * 管理责任人(实验员)姓名
     */
    @ApiModelProperty("管理责任人(实验员)姓名")
    private String responsibleName;

    /**
     * 实训室名称
     */
    @ApiModelProperty("实训室名称")
    private String roomName;

    /**
     * 值班人员
     */
    @ApiModelProperty("值班人员姓名")
    private String dutyName;

    /**
     * 反馈状态 表示是/否反馈和已反馈/待反馈
     * 反馈状态 10->不反馈 20->已反馈 30->待反馈
     */
    @ApiModelProperty("反馈状态 10->不反馈 20->已反馈 30->待反馈")
    private Integer feedbackStatus;
}
