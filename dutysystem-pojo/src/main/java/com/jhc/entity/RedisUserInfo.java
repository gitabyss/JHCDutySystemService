package com.jhc.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.jhc.dto.CmsTreePermission;
import lombok.Data;

import java.util.List;

/**
 * @Author: zfm
 * @Date: 2019/11/15 14:59
 */
@Data
public class RedisUserInfo {

    /**
     * 用户id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    /**
     * 用户number
     */
    private String staffNumber;

    /**
     * 用户name
     */
    private String staffName;

    /**
     * 学院Name
     */
    private String collegeName;

    /**
     * token
     */
    private String token;

    /**
     * 权限列表
     */
    private List<CmsPermission> permissionList;

    /**
     * 路由
     */
    private List<CmsTreePermission> router;
}
