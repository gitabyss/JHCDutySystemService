package com.jhc.entity;

import java.time.LocalDateTime;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 7s检查记录表 
 * </p>
 *
 * @author zfm
 * @since 2019-12-03
 */
@Data
@TableName("ise_7s_inspect")
public class Ise7sInspect extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 整理
     */
    private Boolean arrange;

    /**
     * 整顿
     */
    private Boolean rectify;

    /**
     * 清扫
     */
    private Boolean clear;

    /**
     * 报修
     */
    private Boolean repair;

    /**
     * 安全
     */
    private Boolean safe;

    /**
     * 节约
     */
    private Boolean economize;

    /**
     * 表扬
     */
    private Boolean praise;

    /**
     * 检查项id 关联检查汇总
     */
    private Long inspectId;

    /**
     * 图片
     */
    private String image;


}
