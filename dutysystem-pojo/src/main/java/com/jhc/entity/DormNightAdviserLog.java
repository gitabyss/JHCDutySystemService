package com.jhc.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 夜不归宿班主任操作记录
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DormNightAdviserLog extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 老师工号
     */
    private String teacherNumber;

    /**
     * 学生工号
     */
    private String studentNumber;

    /**
     * 备注信息
     */
    private String remark;

    /**
     * 10 -> 把不正常改为正常 20-> 把正常改为不正常
     */
    private Integer status;

    /**
     * 关联的打卡ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long nightCheckId;

}
