package com.jhc.entity;

import java.time.LocalDateTime;
import com.jhc.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户
 * </p>
 *
 * @author zhengyue
 * @since 2020-03-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CmsUser extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 用户编号
     */
    private String number;

    /**
     * 密码
     */
    private String password;

    /**
     * 姓名
     */
    private String name;

    /**
     * 性别 0->男，1->女，2->保密
     */
    private Integer sex;

    /**
     * 电话
     */
    private String phonenumber;

    /**
     * 头像地址
     */
    private String image;

    /**
     * 启停状态 1->启用 0->禁用
     */
    private Boolean isEnable;

    /**
     * 学院编号
     */
    private String collegeNumber;

    /**
     * 权限集合
     */
    private String authData;

    /**
     * 身份类型 10->教师，20->学生
     */
    private Integer type;

    /**
     * 详情表ID
     */
    private Long userDetailId;
}