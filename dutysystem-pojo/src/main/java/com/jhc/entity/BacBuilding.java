package com.jhc.entity;

import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author zfm
 * @since 2020-03-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BacBuilding extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 楼栋名称
     */
    private String name;

    /**
     * 坐标位置
     */
    private String fence;


}
