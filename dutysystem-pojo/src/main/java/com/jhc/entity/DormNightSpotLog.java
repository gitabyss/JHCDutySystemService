package com.jhc.entity;

import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.jhc.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 学生值班计划分配表
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DormNightSpotLog extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 楼幢ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long buildingId;

    /**
     * 楼幢名称
     */
    private String buidingName;

    /**
     * 寝室ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long roomId;

    /**
     * 寝室号
     */
    private String roomName;


    /**
     * 抽查日期
     */
    private Date spotDate;

    /**
     * 值班计划ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long planId;


}
