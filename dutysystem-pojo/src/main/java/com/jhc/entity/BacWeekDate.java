package com.jhc.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * <p>
 * 周次表
 * </p>
 *
 * @author xiaxinlin
 * @since 2019-11-24
 */
@Data
@TableName(value = "bac_week_date")
public class BacWeekDate extends BaseEntity implements Serializable {

    /**
     * 周次
     */
    @TableField(value = "week")
    private Integer week;

    /**
     * 开始时间
     */
    @TableField(value = "start_day")
    private Date startDay;

    /**
     * 结束时间
     */
    @TableField(value = "end_day")
    private Date endDay;

    /**
     * 学期id
     */
    @TableField(value = "term_id")
    private Long termId;
}