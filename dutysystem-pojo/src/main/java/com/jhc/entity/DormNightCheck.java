package com.jhc.entity;

import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 学生夜不归宿打卡
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DormNightCheck extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 打卡学生用户名
     */
    private String studentNumber;

    /**
     * 打卡状态
     */
    private Boolean punchStatus;

    /**
     * 登记状态， 10-> 未出寝室， 20 -> 出寝室， 30-> 出寝室回来
     */
    private Integer outStatus;

    /**
     *抽查状态， 10-> 未检查， 20 -> 检查在寝室， 30->检查不在寝室
     */
    private Integer checkStatus;

    /**
     * 班主任检查状态，  10无需处理， 20未处理， 30已处理
     */
    private Boolean adviserCheck;

    /**
     * 最终状态
     */
    private Boolean status;

    /**
     * 所属楼幢
     */
    private String buildingName;

    /**
     * 所属班级
     */
    private String className;

    /**
     * 所属学院
     */
    private String collegeName;

    /**
     * 备注
     */
    private String remark;

    /**
     * 所属楼幢ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long buildingId;

    /**
     * 所属班级ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long classId;

    /**
     * 所属学院ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long collegeId;

}
