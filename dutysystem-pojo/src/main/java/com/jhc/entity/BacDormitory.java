package com.jhc.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 寝室  
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BacDormitory extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 房间号
     */
    private Integer room;

    /**
     * 楼幢ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long buildingId;

    /**
     * 所属班级 可有多个班级用逗号隔开
     */
    private String classId;

    /**
     * 班主任 可有多个班主任用逗号隔开
     */
    private String classTeacherNumber;

    /**
     * 辅导员
     */
    private String instructorNumber;

    /**
     * 所属学院编号
     */
    private String collageNumber;

}
