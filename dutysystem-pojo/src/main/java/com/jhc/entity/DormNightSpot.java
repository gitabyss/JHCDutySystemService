package com.jhc.entity;

import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.jhc.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 夜不归宿抽查记录
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DormNightSpot extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 备注
     */
    private String remark;

    /**
     * 分配表ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long logId;

    /**
     *计划表ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long planId;

    /**
     * 寝室iID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long roomId;

    /**
     * 寝室号
     */
    private String roomName;

    /**
     * 学生ID
     */
    private String stuNumber;

    /**
     * 学生姓名
     */
    private String stuName;

    /**
     * 床号
     */
    private Integer bed;

    /**
     * 10 -> 未查 20 -> 在寝室 30 -> 不在寝室
     */
    private Integer status;

    /**
     * 学院名称
     */
    private String collegeName;

    /**
     * 学院ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long collegeId;


}
