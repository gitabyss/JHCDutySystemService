package com.jhc.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 教学检查 
 * </p>
 *
 * @author zfm
 * @since 2019-12-03
 */
@Data
public class IseTeachInspect extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 迟到
     */
    private Integer late;

    /**
     * 旷课
     */
    private Integer truancy;

    /**
     * 请假
     */
    private Integer holiday;

    /**
     * 睡觉
     */
    private Boolean sleep;

    /**
     * 交头接耳
     */
    private Boolean chat;

    /**
     * 吃零食
     */
    private Boolean eat;

    /**
     * 游戏
     */
    private Boolean play;

    /**
     * 不带课本
     */
    private Boolean noBook;

    /**
     * 玩手机
     */
    private Boolean playPhone;

    /**
     * 不穿工作服
     */
    private Boolean noClothes;

    /**
     * 学生其他信息
     */
    private String stuOther;

    /**
     * 读ppt
     */
    private Boolean ppt;

    /**
     * 无互动
     */
    private Boolean noInteract;

    /**
     * 不写板书
     */
    private Boolean noWrite;

    /**
     * 课堂管理欠佳
     */
    private Boolean discipline;

    /**
     * 迟到早退
     */
    private Boolean lateLeave;

    /**
     * 坐着上课
     */
    private Boolean sit;

    /**
     * 老师其他信息
     */
    private String teaOther;

    /**
     * 图片
     */
    private String image;

    /**
     * 检查项id
     */
    private Long inspectId;

}
