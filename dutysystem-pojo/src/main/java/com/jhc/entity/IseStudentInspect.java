package com.jhc.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 学生检查表 
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class IseStudentInspect extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 检查项id
     */
    private Long itemId;

    /**
     * 检查总表id
     */
    private Long inspectionId;

    /**
     * 学生id
     */
    private String studentNumber;

    /**
     * 检查结果
     */
    private String result;


}
