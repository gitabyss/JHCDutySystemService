package com.jhc.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 自习表 
 * </p>
 *
 * @author zfm
 * @since 2019-12-06
 */
@Data
public class BacStudy extends BaseEntity implements Serializable {


    /**
     * 班级id
     */
    private Long classId;

    /**
     * 教室id
     */
    private Long roomId;

    /**
     * 教室名
     */
    private String roomName;


}
