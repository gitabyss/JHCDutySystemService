package com.jhc.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.jhc.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 夜不归宿打卡记录
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DormNightPunch extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     *总表id
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long checkId;

    /**
     *是否在范围内打卡
     */
    private Boolean isWithin;

    /**
     * 是否二次打卡
     */
    private Boolean isTwice;

    /**
     * 打卡位置
     */
    private String position;

    /**
     * 打卡学生学号
     */
    private String studentNumber;

    /**
     * 备注
     */
    private String remark;


}
