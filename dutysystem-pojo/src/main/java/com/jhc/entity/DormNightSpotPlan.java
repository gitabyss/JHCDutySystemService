package com.jhc.entity;

import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.jhc.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 夜不归宿抽查计划表
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DormNightSpotPlan extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 学生1账号
     */
    private String stuOneNumber;

    /**
     * 学生1姓名
     */
    private String stuOneName;

    /**
     * 学生2账号
     */
    private String stuTwoNumber;

    /**
     * 学生2姓名
     */
    private String stuTwoName;

    /**
     * 周次
     */
    private Integer week;

    /**
     * 周次ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long weekId;

    /**
     * 备注信息
     */
    private String remark;


}
