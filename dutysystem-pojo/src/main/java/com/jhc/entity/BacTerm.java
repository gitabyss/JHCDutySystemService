package com.jhc.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 学期信息 
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BacTerm extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 学期名称
     */
    private String termName;

    /**
     * 开学日期
     */
    private Date openingDate;

    /**
     * 结束日期
     */
    private Date endDate;

    /**
     * 教学周次
     * 总周次
     */
    private Integer teachingWeek;

    /**
     * 当前状态
     */
    private Integer currentState;


}
