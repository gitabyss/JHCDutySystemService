package com.jhc.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.jhc.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author zfm
 * @since 2020-03-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BacCalendar extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 节假日日期
     */
    private Date date;

    /**
     * 备注
     */
    private String remark;

    /**
     * 1->工作日 2-> 周末节假日 3->寒假 4->暑假
     */
    private Integer type;


}
