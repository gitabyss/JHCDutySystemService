package com.jhc.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 行政班 
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BacClass extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    private String name;

    /**
     * 专业编号
     */
    private String majorNumber;

    /**
     * 学院编号
     */
    private String collegeNumber;

    /**
     * 班主任number
     */
    private String teacherNumber;

    /**
     * 是否启用 1->启用 0->不启用
     */
    private Boolean isEnabled;

    /**
     * 是否毕业
     */
    private Boolean status;

    /**
     * 开始日期
     */
    private Date startDate;

    /**
     * 结束日期
     */
    private Date endDate;
}
