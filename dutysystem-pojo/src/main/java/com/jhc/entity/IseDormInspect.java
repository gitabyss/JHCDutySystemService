package com.jhc.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 寝室检查记录表 
 * </p>
 *
 * @author zfm
 * @since 2019-12-03
 */
@Data
public class IseDormInspect extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 地面不整洁，垃圾未清理
     */
    private Boolean floor;

    /**
     * 洗漱台无序，水槽有污垢
     */
    private Boolean pentrough;

    /**
     * 便池不清洁，空气有异味
     */
    private Boolean toilet;

    /**
     * 被子未叠正，床帷未拉开
     */
    private Boolean bed;

    /**
     * 桌面较凌乱，书柜欠整齐
     */
    private Boolean desk;

    /**
     * 衣服未挂齐，鞋子乱摆放
     */
    private Boolean clothes;

    /**
     * 阳台堆杂物，门窗有积灰
     */
    private Boolean balcony;

    /**
     * 大功率
     */
    private Boolean electrical;

    /**
     * 宠物
     */
    private Boolean pet;

    /**
     * 刀具
     */
    private Boolean knife;

    /**
     * 酒瓶
     */
    private Boolean bottle;

    /**
     *夜不归宿
     */
    private Boolean night;

    /**
     * 图片
     */
    private String image;

    /**
     * 检查项id
     */
    private Long inspectId;


}
