package com.jhc.entity;

import java.time.LocalDateTime;
import com.jhc.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 学生档案  
 * </p>
 *
 * @author zhengyue
 * @since 2020-03-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CmsStudent extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 登录密码
     */
    private String password;

    /**
     * 学号
     */
    private String studentNumber;

    /**
     * 所属学院
     */
    private String collegeNumber;

    /**
     * 启停状态
     */
    private Boolean isEnable;

    /**
     * 名字
     */
    private String name;

    /**
     * 性别
     */
    private Integer sex;

    /**
     * 学生电话
     */
    private String phone;

    //--------------------------------------

    /**
     * 行政班编号
     */
    private Long classId;

    /**
     * 床号
     */
    private Integer bed;

    /**
     * 专业编号
     */
    private String specialtyNumber;

    /**
     * 学籍状态
     */
    private Integer status;

    /**
     * 班主任(教师)编号
     */
    private String headNumber;

    /**
     * 是否毕业 1->毕业 0->未毕业
     */
    private Boolean isGraduate;

    /**
     * 寝室id
     */
    private Long dormId;


}
