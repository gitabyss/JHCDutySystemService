package com.jhc.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 自习检查 
 * </p>
 *
 * @author zfm
 * @since 2019-12-03
 */
@Data
public class IseStudyInspect extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 旷课学生数
     */
    private Integer truancy;

    /**
     * 请公假人数
     */
    private Integer publicHoliday;

    /**
     * 请私假人数
     */
    private Integer privateHoliday;

    /**
     * 学生自习状态   10->多数人在学习 20->少数人在学习 30->基本不在学习 40->主题班会讲座
     */
    private Integer studyState;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 图片
     */
    private String image;

    /**
     * 检查项id
     */
    private Long inspectId;


}
