package com.jhc.entity;

import java.time.LocalDateTime;
import com.jhc.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 教师档案
 * </p>
 *
 * @author zhengyue
 * @since 2020-03-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CmsTeacher extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 职工号
     */
    private String number;

    /**
     * 登录密码
     */
    private String password;

    /**
     * 姓名
     */
    private String name;

    /**
     * 性别
     */
    private Integer sex;

    /**
     * 所属学院
     */
    private String collegeNumber;

    /**
     * 启停状态
     */
    private Boolean isEnabled;

    /**
     * 权限集合
     */
    private String authData;

    //-------------------------------------

    /**
     * 教师类型 10->外聘 20->在编
     */
    private Integer type;

    /**
     * 部门编号
     */
    private String departmentNumber;

    /**
     * 部门名称
     */
    private String departmentName;

}
