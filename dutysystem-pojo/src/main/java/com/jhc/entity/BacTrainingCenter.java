package com.jhc.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * <p>
 * 实训中心  
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BacTrainingCenter extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 中心名称
     */
    private String centerName;

    /**
     * 所属学院
     */
    private String collegeNumber;

    /**
     * 级别
     */
    private Integer level;

    /**
     * 类型
     */
    private Integer type;

    /**
     * 类别
     */
    private Integer category;

    /**
     * 设计年月
     */
    private LocalDate designDate;

    /**
     * 地址
     */
    private String address;

    /**
     * 实训中心主任编号
     */
    private String directorNumber;

    /**
     * 获得荣誉
     */
    private String honor;

    /**
     * 中心介绍
     */
    private String introduce;


}
