package com.jhc.entity;

import java.time.LocalDateTime;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 检查图片表
 * </p>
 *
 * @author zfm
 * @since 2019-12-10
 */
@Data
public class IseImages extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 检查总表id
     */
    private Long inspectId;

    /**
     * 图片名
     */
    private String name;

    /**
     * 描述
     */
    private String description;

    /**
     * 地址
     */
    private String address;

    /**
     * 大小
     */
    private String size;


}
