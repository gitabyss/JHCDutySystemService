package com.jhc.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.jhc.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 辅导员值班计划
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DormNightCounselorPlan extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 值班老师工号
     */
    private String teacherNumber;

    /**
     * 值班老师姓名
     */
    private String teacherName;

    /**
     * 值班日期
     */
    private Date dutyDate;

    /**
     * 值班周次
     */
    private Integer week;

    /**
     * 值班楼幢ID
     */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long buildingId;

    /**
     * 备注
     */
    private String remark;


}
