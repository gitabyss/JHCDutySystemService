package com.jhc.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 课表档案 
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BacTimetable extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 课程代码
     */
    private String courseNumber;

    /**
     * 课程名称
     */
    private String courseTitle;

    /**
     * 教学班编号
     */
    private Long teachingClassId;

    /**
     * 星期
     */
    private Integer weekDay;

    /**
     * 节次
     */
    private String section;

    /**
     * 周次
     */
    private Integer week;

    /**
     * 上课日期
     */
    private Date classDate;

    /**
     * 教师编号
     */
    private String teacherNumber;

    /**
     * 上课地点，教室id
     */
    private Long roomId;


    /**
     * 周次表id
     */
    private Long weekId;
}
