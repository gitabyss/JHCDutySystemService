package com.jhc.entity;

import java.time.LocalDateTime;
import com.jhc.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统配置
 * </p>
 *
 * @author zfm
 * @since 2020-03-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CmsConfig extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 配置字段名
     */
    private String name;

    /**
     * 配置值
     */
    private String value;

    /**
     * 描述
     */
    private String description;

    /**
     * 配置标题
     */
    private String title;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 分组名称
     */
    private String summary;


}
