package com.jhc.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import com.jhc.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 夜不归宿进出公寓记录
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DormNightOut extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 学生学号
     */
    private String studentNumber;

    /**
     * 登记老师工号
     */
    private String teacherNumber;

    /**
     * 10-> 外出 20 -> 回来
     */
    private Integer status;

    /**
     * 备注
     */
    private String remark;


}
