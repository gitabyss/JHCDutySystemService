package com.jhc.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 业务消息对象表
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CmsNoticeObject extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 消息id
     */
    private Long noticeId;

    /**
     * 用户类型10:学生 20:老师
     */
    private Integer userType;

    /**
     * 用户id
     */
    private String userNumber;

    /**
     * 是否已读
     */
    private Boolean isRead;

    /**
     * 阅读时间
     */
    private Date readTime;


}
