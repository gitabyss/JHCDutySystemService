package com.jhc.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 课程信息 
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BacCourse extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 课程代码
     */
    private String number;

    /**
     * 课程名称
     */
    private String name;

    /**
     * 是否启用
     */
    private Boolean isEnabled;


}
