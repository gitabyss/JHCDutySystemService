package com.jhc.bo;

import com.jhc.entity.CmsPermission;
import com.jhc.entity.CmsTeacher;
import com.jhc.entity.CmsUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author: zfm
 * @Date: 2019/11/11 22:45
 */
public class AdminUserDetails implements UserDetails {

    private CmsUser cmsUser;
    private List<CmsPermission> permissionList;

    public AdminUserDetails(CmsUser cmsUser, List<CmsPermission> permissionList) {
        this.cmsUser = cmsUser;
        this.permissionList = permissionList;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        // 返回当前用户的权限
        return permissionList.stream()
                .filter(permission -> permission.getValue() != null)
                .map(permission -> new SimpleGrantedAuthority(permission.getValue()))
                .collect(Collectors.toList());
    }

    @Override
    public String getPassword() {
        return cmsUser.getPassword();
    }

    @Override
    public String getUsername() {
        return cmsUser.getNumber();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return cmsUser.getIsEnable();
    }
}
