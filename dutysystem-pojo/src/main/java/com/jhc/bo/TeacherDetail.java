package com.jhc.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/4 11:48
 */
@Data
public class TeacherDetail {

    private String name;

    private String number;
}
