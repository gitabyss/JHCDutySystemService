package com.jhc.bo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/2 10:29
 */
@Data
public class PageDetails {

    @ApiModelProperty(value = "当前页码", required = true)
    private Long currentPage = 1L;

    @ApiModelProperty(value = "每页记录数", required = true)
    private Long pageSize = 5L;
}
