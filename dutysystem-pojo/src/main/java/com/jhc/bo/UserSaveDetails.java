package com.jhc.bo;

import com.jhc.entity.CmsUser;
import com.jhc.entity.CmsTeacher;
import lombok.Data;

/**
 * @Author: xiaxinlin
 * @Date: 2019/11/30 16:47
 */
@Data
public class UserSaveDetails {

    private CmsUser cmsUser;

}
