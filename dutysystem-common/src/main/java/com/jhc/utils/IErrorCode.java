package com.jhc.utils;

/**
 * @Author: zfm
 * @Date: 2019/11/11 22:16
 */
public interface IErrorCode {
    long getCode();

    String getMessage();
}
