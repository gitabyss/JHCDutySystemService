package com.jhc.utils;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/10 15:15
 */
@Data
@Component
//@ConfigurationProperties(prefix = "web")
public class ImageSuffix {

    @Value("${web.images-suffix}")
    List<String> suffix = new ArrayList<>();

    public boolean isImages(String fileName){
        System.out.println(suffix);
        //获取后缀名
        String str = FileNameUtil.getSuffix(fileName).toUpperCase();
        //校验后缀名
        if (suffix.contains(str)){
            return true;
        }
        return false;
    }
}
