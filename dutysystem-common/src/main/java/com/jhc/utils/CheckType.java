package com.jhc.utils;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/13 14:27
 */
public enum CheckType {

    //类型标识符
    ISE7S(10,"7s检查"),
    ISEDORM(20,"寝室检查"),
    ISESTUDY(30,"自习检查"),
    ISETEACH(40,"教学检查");
    private Integer code;
    private String message;

    private  CheckType(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public static CheckType getCheckType(Integer code){
        for (CheckType value : CheckType.values()) {
            if (value.getCode().equals(code)){
                return value;
            }
        }
        return null;
    }
}
