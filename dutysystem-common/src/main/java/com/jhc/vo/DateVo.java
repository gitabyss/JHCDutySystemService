package com.jhc.vo;

import lombok.Data;

import java.util.Date;

/**
 * @Author: xiaxinlin
 * @Date: 2019/12/6 14:29
 */
@Data
public class DateVo {

    private Date startDate;

    private Date endDate;
}
