package com.jhc.mapper;

import com.jhc.entity.DormNightSpotPlan;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 夜不归宿抽查计划表 Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
public interface DormNightSpotPlanMapper extends BaseMapper<DormNightSpotPlan> {

}
