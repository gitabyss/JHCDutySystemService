package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.dto.BacMajorSearchDto;
import com.jhc.dto.BacMajorViewDto;
import com.jhc.dto.BacStudySearchDto;
import com.jhc.dto.BacStudyViewDto;
import com.jhc.entity.BacStudy;
import com.jhc.vo.PageVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 自习表  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-12-06
 */
@Repository
public interface BacStudyMapper extends BaseMapper<BacStudy> {
    /**
     * 获取自习分页数据
     * @param page,searchDto
     * @return
     */
//    List<BacStudyViewDto> getStudyDataPage(PageVo<BacStudySearchDto> pageVo);
    IPage<BacStudyViewDto> getStudyDataPage(Page page, @Param("dto") BacStudySearchDto searchDto);
    /**
     * 获取数据条数
     * @return
     */
    Long getStudyCount(PageVo<BacStudySearchDto> pageVo);
}
