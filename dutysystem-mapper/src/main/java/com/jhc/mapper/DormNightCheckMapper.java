package com.jhc.mapper;

import com.jhc.entity.DormNightCheck;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 学生夜不归宿打卡 Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
public interface DormNightCheckMapper extends BaseMapper<DormNightCheck> {

}
