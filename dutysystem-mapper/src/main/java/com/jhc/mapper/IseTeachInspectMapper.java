package com.jhc.mapper;
import java.util.List;
import java.util.Date;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.dto.*;
import com.jhc.entity.IseTeachInspect;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 教学检查  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-12-03
 */
public interface IseTeachInspectMapper extends BaseMapper<IseTeachInspect> {

    /**
     * 分页查询所有教学检查
     * @param page
     * @param iseTeaInspectSearchDto
     * @return
     */
    IPage<IseTeaInspectViewDto> selectByIseTeaInspectSearchDto(Page page,@Param("dto") IseTeaInspectSearchDto iseTeaInspectSearchDto,@Param("list") List<Object> longs);


    /**
     * 查询已填教学检查列表（输出教学检查表id）
     *
     * @param page
     * @param searchDto
     * @param username
     * @param feedback
     * @return
     */
    IPage<FinishTeachViewDto> selectByFinishTeachSearchDto2(Page page, @Param("dto") AllTeachSearchDto searchDto, @Param("user")String username,@Param("feedback")String feedback);

    /**
     * 查询已填教学检查列表（输出检查总表id）
     *
     * @param page
     * @param searchDto
     * @param username
     * @param feedback
     * @return
     */
    IPage<FinishTeachViewDto> selectByFinishTeachSearchDto(Page page, @Param("dto") AllTeachSearchDto searchDto, @Param("user")String username,@Param("feedback")String feedback);

}
