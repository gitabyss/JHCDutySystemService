package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.dto.*;
import com.jhc.entity.BacCourse;
import com.jhc.vo.PageVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 课程信息  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Repository
public interface BacCourseMapper extends BaseMapper<BacCourse> {
    /**
     * 获取课程分页信息
     * @param page,searchDto
     * @return
     */
//    List<BacCourseViewDto> getCoursePageData(PageVo<BacCourseSearchDto> pageVo);
    IPage<BacCourseViewDto> getCoursePageData(Page page, @Param("dto") BacCourseSearchDto searchDto);
    /**
     * 获取数据条数
     * @return
     */
    Long getCourseCount(PageVo<BacCourseSearchDto> pageVo);
}
