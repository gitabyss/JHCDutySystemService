package com.jhc.mapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.dto.All7sSearchDto;
import com.jhc.dto.Finish7sViewDto;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Date;

import com.jhc.dto.Finish7sSearchDto;
import com.jhc.entity.Ise7sInspect;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 7s检查记录表  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-12-03
 */
public interface Ise7sInspectMapper extends BaseMapper<Ise7sInspect> {

    /**
     * 查询出寝室检查表的id
     *
     * @param page
     * @param searchDto
     * @param username
     * @param feedback
     * @return
     */
    IPage<Finish7sViewDto> selectByFinish7sSearchDto(Page page, @Param("dto")All7sSearchDto searchDto, @Param("user")String username,@Param("feedback")String feedback);

    /**
     * 查询出检查总表的id
     *
     * @param page
     * @param searchDto
     * @param username
     * @param feedback
     * @return
     */
    IPage<Finish7sViewDto> selectByFinish7sSearchDto2(Page page, @Param("dto")All7sSearchDto searchDto, @Param("user")String username,@Param("feedback")String feedback);



}
