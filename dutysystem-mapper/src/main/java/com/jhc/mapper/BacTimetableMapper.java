package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.dto.*;
import com.jhc.entity.BacTimetable;
import com.jhc.vo.PageVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 课表档案  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Repository
public interface BacTimetableMapper extends BaseMapper<BacTimetable> {
    /**
     * 获取专业档案分页信息
     * @param page,searchDto
     * @return
     */
//    List<BacTimeTableViewDto> getTimeTableDataPage(PageVo<BacTimeTableSearchDto> pageVo);
    IPage<BacTimeTableViewDto> getTimeTableDataPage(Page page, @Param("dto") BacTimeTableSearchDto searchDto);
    /**
     * 获取数据条数
     * @return
     */
    Long getTimeTableCount(PageVo<BacTimeTableSearchDto> pageVo);
}
