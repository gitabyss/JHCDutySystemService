package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.entity.BacWeekDate;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 周次表  Mapper 接口
 * </p>
 *
 * @author xiaxinlin
 * @since 2019-11-24
 */
@Repository
public interface BacWeekDateMapper extends BaseMapper<BacWeekDate> {
}