package com.jhc.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.dto.*;
import com.jhc.entity.IseStudyInspect;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 自习检查  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-12-03
 */
public interface IseStudyInspectMapper extends BaseMapper<IseStudyInspect> {

    IPage<IseStudyInspectViewDto> selectByIseStudyInspectSearchDto(Page page, @Param("dto")IseStudyInspectSearchDto iseStudyInspectSearchDto,@Param("list") List<Object> longs);

    /**
     * 查询已填自习列表（输出自习检查表id）
     *
     * @param page
     * @param searchDto
     * @param username
     * @param feedback
     * @return
     */
    IPage<FinishStudyViewDto> selectByFinishStudySearchDto2(Page page, @Param("dto") AllStudySearchDto searchDto, @Param("user")String username,@Param("feedback")String feedback);

    /**
     * 查询已填自习列表（输出检查总表id）
     *
     * @param page
     * @param searchDto
     * @param username
     * @param feedback
     * @return
     */
    IPage<FinishStudyViewDto> selectByFinishStudySearchDto(Page page, @Param("dto") AllStudySearchDto searchDto, @Param("user")String username,@Param("feedback")String feedback);

}
