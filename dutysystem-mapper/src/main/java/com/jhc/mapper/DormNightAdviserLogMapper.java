package com.jhc.mapper;

import com.jhc.entity.DormNightAdviserLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 夜不归宿班主任操作记录 Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
public interface DormNightAdviserLogMapper extends BaseMapper<DormNightAdviserLog> {

}
