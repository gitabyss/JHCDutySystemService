package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.entity.CmsPermission;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * cms_permission  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Repository
public interface CmsPermissionMapper extends BaseMapper<CmsPermission> {

    /**
     * 恢复权限
     *
     * @param permissionId 权限ID
     * @return 是否更新成功
     */
    Boolean restart(@Param("idList") List<Long> permissionId);

    /**
     * 禁用权限
     * @param permissionId 权限ID
     * @return 是否禁用成功
     */
    Boolean ban(@Param("idList") List<Long> permissionId);

}
