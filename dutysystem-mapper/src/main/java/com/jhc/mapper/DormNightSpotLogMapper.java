package com.jhc.mapper;

import com.jhc.entity.DormNightSpotLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 学生值班计划分配表 Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
public interface DormNightSpotLogMapper extends BaseMapper<DormNightSpotLog> {

}
