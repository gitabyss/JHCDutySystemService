package com.jhc.mapper;
import java.time.LocalDateTime;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.dto.IseScheduleSearchDto;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.dto.IseScheduleViewDto;
import com.jhc.entity.IseSchedule;

/**
 * <p>
 * 值班计划   Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IseScheduleMapper extends BaseMapper<IseSchedule> {

    /**
     *查询所有值班信息
     * @param page
     * @param scheduleSearchDto
     * @return
     */
    IPage<IseScheduleViewDto> selectByIseScheduleSearchDto(Page page, @Param("dto") IseScheduleSearchDto scheduleSearchDto);

    /**
     * 查询该日值班人员
     *
     * @param date
     * @return
     */
    IseSchedule selectByDate(@Param("date")String date);



}
