package com.jhc.mapper;

import com.jhc.entity.DormNightPunch;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 夜不归宿打卡记录 Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
public interface DormNightPunchMapper extends BaseMapper<DormNightPunch> {

}
