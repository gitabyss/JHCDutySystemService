package com.jhc.mapper;

import com.jhc.entity.BacCalendar;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2020-03-11
 */
public interface BacCalendarMapper extends BaseMapper<BacCalendar> {

}
