package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.dto.*;
import com.jhc.entity.BacStuTeach;
import com.jhc.vo.PageVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 学生教学班关系表  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Repository
public interface BacStuTeachMapper extends BaseMapper<BacStuTeach> {
    /**
     * 分页条件查询教学班学生数据
     * @param page,searchDto
     * @return
     */
//    List<BacStuTeachViewDto> getStuTeachPageData(PageVo<BacStuTeachSearchDto> pageVo);
    IPage<BacStuTeachViewDto> getStuTeachPageData(Page page, @Param("dto") BacStuTeachSearchDto searchDto);

    /**
     * 获取数据条数
     * @return
     */
    Long getStuTeachCount(PageVo<BacStuTeachSearchDto> pageVo);
}
