package com.jhc.mapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.dto.*;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.entity.BacTeachingClass;
import com.jhc.vo.PageVo;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 教学班  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Repository
public interface BacTeachingClassMapper extends BaseMapper<BacTeachingClass> {
    /**
     * 分页条件查询教学班数据
     * @param page,searchDto
     * @return
     */
//    List<BacTeachingClassViewDto> getTeachingClassPageData(PageVo<BacTeachingClassSearchDto> pageVo);
    IPage<BacTeachingClassViewDto> getTeachingClassPageData(Page page, @Param("dto") BacTeachingClassSearchDto searchDto);

    /**
     * 获取数据条数
     * @return
     */
    Long getTeachingClassCount(PageVo<BacTeachingClassSearchDto> pageVo);

}
