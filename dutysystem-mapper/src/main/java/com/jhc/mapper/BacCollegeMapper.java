package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.dto.*;
import com.jhc.entity.BacCollege;
import com.jhc.vo.PageVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 院系档案  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Repository
public interface BacCollegeMapper extends BaseMapper<BacCollege> {
    /**
     * 获取专业档案分页信息
     * @param page,searchDto
     * @return
     */
    IPage<BacCollegeViewDto> getCollegePageData(Page page, @Param("dto") BacCollegeSearchDto searchDto);

    /**
     * 获取数据条数
     * @return
     */
    Long getCollegeCount(PageVo<BacCollegeSearchDto> pageVo);

    /**
     * 查询学院下拉框
     *
     * @return
     */
    List<BacCollege> selectCollegeList();

    /**
     * 查询部门下拉框
     *
     * @return
     */
    List<BacCollege> selectDepartmentList();




}
