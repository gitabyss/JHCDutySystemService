package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.entity.CmsStudent;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 学生  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Repository
public interface CmsStudentMapper extends BaseMapper<CmsStudent> {

}
