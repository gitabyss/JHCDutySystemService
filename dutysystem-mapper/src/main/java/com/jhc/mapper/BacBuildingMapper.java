package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.entity.BacBuilding;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2020-03-04
 */
@Repository
public interface BacBuildingMapper extends BaseMapper<BacBuilding> {

}
