package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.dto.BizRoleSearchUserByRoleDto;
import com.jhc.dto.CmsTeacherSearchDto;
import com.jhc.dto.CmsTeacherViewDto;
import com.jhc.entity.CmsTeacher;
import com.jhc.entity.CmsUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 教师档案  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Repository
public interface CmsTeacherMapper extends BaseMapper<CmsTeacher> {
    /**
     * 教师档案分页条件查询
     * @param pageVo
     * @return
     */
//    List<CmsTeacherViewDto> getTeacherPageData(PageVo<CmsTeacherSearchDto> pageVo);

    /**
     * 查询教师档案列表数据条数
     * @return
     */
    Long getTeacherTotal();

    IPage<CmsTeacherViewDto> getTeacherPageData(Page page,@Param("dto") CmsTeacherSearchDto searchDto);

    /**
     *
     * @param page
     * @param dto
     * @return
     */
    IPage<CmsUser> selectPageByRole(Page<CmsUser> page, @Param("dto") BizRoleSearchUserByRoleDto dto, @Param("likeRoleId") String likeRoleId);
}
