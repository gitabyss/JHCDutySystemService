package com.jhc.mapper;

import com.jhc.entity.CmsConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 系统配置 Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2020-03-11
 */
public interface CmsConfigMapper extends BaseMapper<CmsConfig> {

}
