package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.dto.BacCourseSearchDto;
import com.jhc.dto.BacCourseViewDto;
import com.jhc.dto.BacMajorSearchDto;
import com.jhc.dto.BacMajorViewDto;
import com.jhc.entity.BacMajor;
import com.jhc.vo.PageVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 专业档案  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Repository
public interface BacMajorMapper extends BaseMapper<BacMajor> {
    /**
     * 获取专业档案分页信息
     * @param page,searchDto
     * @return
     */
//    List<BacMajorViewDto> getMajorPageData(PageVo<BacMajorSearchDto> pageVo);
    IPage<BacMajorViewDto> getMajorPageData(Page page, @Param("dto") BacMajorSearchDto searchDto);
    /**
     * 获取数据条数
     * @return
     */
    Long getMajorCount(PageVo<BacMajorSearchDto> pageVo);
}
