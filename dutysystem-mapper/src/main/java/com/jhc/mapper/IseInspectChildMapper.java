package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.entity.IseInspectChild;

/**
 * <p>
 * 检查子表  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IseInspectChildMapper extends BaseMapper<IseInspectChild> {

}
