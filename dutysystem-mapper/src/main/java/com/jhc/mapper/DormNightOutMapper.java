package com.jhc.mapper;

import com.jhc.entity.DormNightOut;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 夜不归宿进出公寓记录 Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
public interface DormNightOutMapper extends BaseMapper<DormNightOut> {

}
