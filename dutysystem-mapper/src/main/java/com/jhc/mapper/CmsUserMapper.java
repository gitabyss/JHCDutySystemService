package com.jhc.mapper;

import com.jhc.entity.CmsTeacher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.entity.CmsUser;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 用户  Mapper 接口
 * </p>
 *
 * @author zhengyue
 * @since 2020-03-13
 */
@Repository
public interface CmsUserMapper extends BaseMapper<CmsUser> {

}
