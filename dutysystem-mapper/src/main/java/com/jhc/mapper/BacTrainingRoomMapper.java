package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.dto.BacTrainingCenterSearchDto;
import com.jhc.dto.BacTrainingCenterViewDto;
import com.jhc.dto.BacTrainingRoomSearchDto;
import com.jhc.dto.BacTrainingRoomViewDto;
import com.jhc.entity.BacTrainingRoom;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 实训室  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface BacTrainingRoomMapper extends BaseMapper<BacTrainingRoom> {
    /**
     * 获取实训室分页信息
     * @param page,searchDto
     * @return
     */
    IPage<BacTrainingRoomViewDto> getTrainingRoomData(Page page, @Param("dto") BacTrainingRoomSearchDto searchDto);
}
