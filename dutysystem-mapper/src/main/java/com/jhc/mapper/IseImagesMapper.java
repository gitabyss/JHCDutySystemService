package com.jhc.mapper;

import com.jhc.entity.IseImages;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 检查图片表 Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-12-10
 */
public interface IseImagesMapper extends BaseMapper<IseImages> {

}
