package com.jhc.mapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.dto.*;
import org.apache.ibatis.annotations.Param;
import java.util.List;

import com.jhc.entity.IseDormInspect;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 寝室检查记录表  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-12-03
 */
public interface IseDormInspectMapper extends BaseMapper<IseDormInspect> {

    IPage<IseDormInspectViewDto> selectByIseDormInspectSearchDto(Page page, @Param("dto") IseDormInspectSearchDto iseDormInspectSearchDto, @Param("list")List<Object> longs);


    /**
     * 查询已填寝室列表（输出寝室检查表id）
     *
     * @param page
     * @param searchDto
     * @param username
     * @param feedback
     * @return
     */
    IPage<FinishDormViewDto> selectByFinishDormSearchDto2(Page page, @Param("dto")AllDormSearchDto searchDto, @Param("user")String username,@Param("feedback")String feedback);

    /**
     * 查询已填寝室列表（输出检查总表id）
     *
     * @param page
     * @param searchDto
     * @param username
     * @param feedback
     * @return
     */
    IPage<FinishDormViewDto> selectByFinishDormSearchDto(Page page, @Param("dto")AllDormSearchDto searchDto, @Param("user")String username,@Param("feedback")String feedback);


}
