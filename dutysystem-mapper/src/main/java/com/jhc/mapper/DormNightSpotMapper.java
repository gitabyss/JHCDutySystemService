package com.jhc.mapper;

import com.jhc.entity.DormNightSpot;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 夜不归宿抽查记录 Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
public interface DormNightSpotMapper extends BaseMapper<DormNightSpot> {

}
