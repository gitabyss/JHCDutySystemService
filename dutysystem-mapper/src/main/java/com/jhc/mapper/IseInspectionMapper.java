package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.entity.IseInspection;

/**
 * <p>
 * 检查汇总 填写完检查后生成一条新的记录，未填写前没有这条记录 Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface IseInspectionMapper extends BaseMapper<IseInspection> {

}
