package com.jhc.mapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.dto.BacClassSearchDto;
import com.jhc.dto.BacClassStuSearchDto;
import com.jhc.dto.BacClassStuViewDto;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import java.util.Date;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.jhc.dto.BacClassViewDto;
import com.jhc.entity.BacClass;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 行政班  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Repository
public interface BacClassMapper extends BaseMapper<BacClass> {

    IPage<BacClassViewDto> selectByBacClassSearchDto(Page page,@Param("dto") BacClassSearchDto searchDto);

    IPage<BacClassStuViewDto> selectByBacClassStuViewDto(Page page, @Param("dto")BacClassStuSearchDto stuSearchDto);


}
