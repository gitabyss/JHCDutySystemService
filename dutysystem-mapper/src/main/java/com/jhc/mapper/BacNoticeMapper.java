package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jhc.entity.CmsNotice;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 业务消息表  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Repository
public interface BacNoticeMapper extends BaseMapper<CmsNotice> {

}
