package com.jhc.mapper;

import com.jhc.entity.DormNightCounselorPlan;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 辅导员值班计划 Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2020-03-10
 */
public interface DormNightCounselorPlanMapper extends BaseMapper<DormNightCounselorPlan> {

}
