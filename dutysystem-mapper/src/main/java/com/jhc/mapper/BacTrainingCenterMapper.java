package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.dto.BacMajorSearchDto;
import com.jhc.dto.BacMajorViewDto;
import com.jhc.dto.BacTrainingCenterSearchDto;
import com.jhc.dto.BacTrainingCenterViewDto;
import com.jhc.entity.BacTrainingCenter;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 实训中心   Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
public interface BacTrainingCenterMapper extends BaseMapper<BacTrainingCenter> {
    /**
     * 获取实训中心分页信息
     * @param page,searchDto
     * @return
     */
    IPage<BacTrainingCenterViewDto> getTrainingCenterData(Page page, @Param("dto") BacTrainingCenterSearchDto searchDto);
}
