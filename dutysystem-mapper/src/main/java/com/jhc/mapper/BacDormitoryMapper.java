package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.dto.*;
import com.jhc.entity.BacDormitory;
import com.jhc.vo.PageVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 * 寝室   Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Repository
public interface BacDormitoryMapper extends BaseMapper<BacDormitory> {
    /**
     * 获取寝室分页信息
     * @param page,searchDto
     * @return
     */
//     List<BacDormitoryViewDto> getDormitoryPageData(PageVo<BacDormitorySearchDto> pageVo);
    IPage<BacDormitoryViewDto> getDormitoryPageData(Page page, @Param("dto") BacDormitorySearchDto searchDto);
    /**
     * 根据寝室编号获取学生列表信息
     * @param id
     * @return
     */
    List<CmsStudentDormitoryViewDto> getStudentListByDormitoryId(Long id);

    /**
     * 获取数据条数
     * @return
     */
    Long getDormitoryCount(PageVo<BacDormitorySearchDto> pageVo);
}
