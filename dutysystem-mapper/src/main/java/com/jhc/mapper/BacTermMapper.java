package com.jhc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.dto.BacMajorSearchDto;
import com.jhc.dto.BacMajorViewDto;
import com.jhc.dto.BacTermSearchDto;
import com.jhc.dto.BacTermViewDto;
import com.jhc.entity.BacTerm;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 学期信息  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Repository
public interface BacTermMapper extends BaseMapper<BacTerm> {
    /**
     * 获取学期档案分页信息
     * @param page,searchDto
     * @return
     */
//    List<BacTermViewDto> getMajorPageData(PageVo<BacTermSearchDto> pageVo);
    IPage<BacTermViewDto> getTeacherPageData(Page page, @Param("dto") BacTermSearchDto searchDto);
}
