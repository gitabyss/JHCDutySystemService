package com.jhc.mapper;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.jhc.dto.BizCheckMessageDto;
import com.jhc.dto.BizNoticeSearchDto;
import com.jhc.entity.CmsNoticeObject;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 业务消息对象表  Mapper 接口
 * </p>
 *
 * @author zfm
 * @since 2019-11-24
 */
@Repository
public interface BacNoticeObjectMapper extends BaseMapper<CmsNoticeObject> {

    /**
     * 查询消息列表
     *
     * @param bizNoticeSearchDto
     * @param page               分页查询
     * @param username           用户名
     * @return
     */
    IPage<BizCheckMessageDto> selectByUsername(Page page, @Param("dto") BizNoticeSearchDto bizNoticeSearchDto, @Param("username") String username);


}
