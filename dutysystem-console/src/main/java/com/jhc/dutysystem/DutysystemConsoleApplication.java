package com.jhc.dutysystem;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAdminServer
public class DutysystemConsoleApplication {

    public static void main(String[] args) {
        SpringApplication.run(DutysystemConsoleApplication.class, args);
    }

}
