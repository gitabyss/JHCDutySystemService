/*
 Navicat Premium Data Transfer

 Source Server         : sql
 Source Server Type    : MySQL
 Source Server Version : 50643
 Source Host           : 47.106.103.133:3306
 Source Schema         : dutysystemx2

 Target Server Type    : MySQL
 Target Server Version : 50643
 File Encoding         : 65001

 Date: 13/11/2019 09:34:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for bac_class
-- ----------------------------
DROP TABLE IF EXISTS `bac_class`;
CREATE TABLE `bac_class`  (
  `id` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `major_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '专业编号',
  `college_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '学院编号',
  `is_enabled` bit(1) NOT NULL COMMENT '是否启用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '行政班 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bac_college
-- ----------------------------
DROP TABLE IF EXISTS `bac_college`;
CREATE TABLE `bac_college`  (
  `id` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '部门编号',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '部门名称',
  `parent_number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '隶属部门',
  `sort` int(11) NULL DEFAULT NULL COMMENT '显示顺序',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '院系档案 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bac_course
-- ----------------------------
DROP TABLE IF EXISTS `bac_course`;
CREATE TABLE `bac_course`  (
  `id` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '课程代码',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '课程名称',
  `is_enabled` bit(1) NOT NULL COMMENT '是否启用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '课程信息 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bac_dormitory
-- ----------------------------
DROP TABLE IF EXISTS `bac_dormitory`;
CREATE TABLE `bac_dormitory`  (
  `id` bigint(19) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `room` int(11) NOT NULL COMMENT '房间号',
  `buildings` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '楼幢',
  `class_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '所属班级 可有多个班级用逗号隔开',
  `class_teacher_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '班主任 可有多个班主任用逗号隔开',
  `instructor_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '辅导员',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '寝室  ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bac_major
-- ----------------------------
DROP TABLE IF EXISTS `bac_major`;
CREATE TABLE `bac_major`  (
  `id` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `professional_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '专业代码',
  `professional_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '专业名称',
  `director_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '专业主任',
  `college_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '所属学院',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '专业档案 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bac_stu_teach
-- ----------------------------
DROP TABLE IF EXISTS `bac_stu_teach`;
CREATE TABLE `bac_stu_teach`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` int(11) NULL DEFAULT 0 COMMENT '逻辑删除',
  `student_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '学生编号',
  `class_id` bigint(20) NULL DEFAULT NULL COMMENT '教学班编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '学生教学班关系表 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bac_teaching_class
-- ----------------------------
DROP TABLE IF EXISTS `bac_teaching_class`;
CREATE TABLE `bac_teaching_class`  (
  `class_id` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `teaching_class_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '教学班名称',
  `instructor_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '授课老师编号',
  `specialty_id` bigint(19) NOT NULL COMMENT '专业编号',
  `college_id` bigint(19) NOT NULL COMMENT '学院编号',
  `start_and_stop_state` bit(1) NOT NULL COMMENT '启停状态',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  PRIMARY KEY (`class_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '教学班 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bac_term
-- ----------------------------
DROP TABLE IF EXISTS `bac_term`;
CREATE TABLE `bac_term`  (
  `id` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `term_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '学期名称',
  `opening_date` datetime(0) NOT NULL COMMENT '开学日期',
  `end_date` datetime(0) NOT NULL COMMENT '结束日期',
  `teaching_week` int(10) NOT NULL COMMENT '教学周次',
  `current_state` int(10) NOT NULL COMMENT '当前状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '学期信息 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bac_timetable
-- ----------------------------
DROP TABLE IF EXISTS `bac_timetable`;
CREATE TABLE `bac_timetable`  (
  `id` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `course_code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '课程代码',
  `course_title` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '课程名称',
  `teaching_class_id` bigint(19) NOT NULL COMMENT '教学班编号',
  `week_day` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '星期',
  `section` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '节次',
  `week` int(10) NOT NULL COMMENT '周次',
  `class_date` datetime(0) NOT NULL COMMENT '上课日期',
  `class_time` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '上课时间',
  `teacher_id` bigint(19) NOT NULL COMMENT '教师编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '课表档案 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bac_training_center
-- ----------------------------
DROP TABLE IF EXISTS `bac_training_center`;
CREATE TABLE `bac_training_center`  (
  `id` bigint(19) NOT NULL AUTO_INCREMENT,
  `center_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '中心名称',
  `college_id` bigint(19) NULL DEFAULT NULL COMMENT '所属学院',
  `level` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '级别',
  `type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '类型',
  `category` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '类别',
  `design_date` date NULL DEFAULT NULL COMMENT '设计年月',
  `address` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址',
  `director_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '实训中心主任编号',
  `honor` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '获得荣誉',
  `introduce` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '中心介绍',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '实训中心  ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bac_training_room
-- ----------------------------
DROP TABLE IF EXISTS `bac_training_room`;
CREATE TABLE `bac_training_room`  (
  `id` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '实训室ID',
  `room_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '实训室名称',
  `trainingcenter_id` bigint(19) NULL DEFAULT NULL COMMENT '实训中心 关联实训中心表的实训中心id',
  `professional_id` bigint(19) NULL DEFAULT NULL COMMENT '服务专业',
  `courses` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '开设课程',
  `location` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '具体地点',
  `people_number` int(10) NULL DEFAULT NULL COMMENT '人数容量',
  `responsible_teacher_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '管理责任人编号',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '实训室 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_permission
-- ----------------------------
DROP TABLE IF EXISTS `cms_permission`;
CREATE TABLE `cms_permission`  (
  `id` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '权限名称',
  `code` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '权限编码',
  `parent_code` bigint(19) NOT NULL COMMENT '权限父编号',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '权限信息表 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_role
-- ----------------------------
DROP TABLE IF EXISTS `cms_role`;
CREATE TABLE `cms_role`  (
  `id` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `description` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色描述',
  `count` int(10) NOT NULL COMMENT '拥有该角色的用户数量',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色信息表 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `cms_role_permission`;
CREATE TABLE `cms_role_permission`  (
  `id` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `permission_id` bigint(19) NOT NULL COMMENT '权限编号',
  `role_id` bigint(19) NOT NULL COMMENT '角色编号',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色权限关系表 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_stu_role
-- ----------------------------
DROP TABLE IF EXISTS `cms_stu_role`;
CREATE TABLE `cms_stu_role`  (
  `id` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `stu_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '学生编号',
  `role_id` bigint(19) NOT NULL COMMENT '角色编号',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '学生角色关系表 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_student
-- ----------------------------
DROP TABLE IF EXISTS `cms_student`;
CREATE TABLE `cms_student`  (
  `id` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登录密码',
  `administrtive_classid` bigint(19) NULL DEFAULT NULL COMMENT '行政班编号',
  `student_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '学号',
  `specialty_id` bigint(19) NOT NULL COMMENT '专业编号',
  `status_of_student_status` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '学籍状态',
  `college_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '所属学院',
  `head_teacher_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '班主任(教师)编号',
  `is_enabled` bit(1) NOT NULL COMMENT '启停状态',
  `graduate` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否毕业',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '学生 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_teacher
-- ----------------------------
DROP TABLE IF EXISTS `cms_teacher`;
CREATE TABLE `cms_teacher`  (
  `id` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `number` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '职工号',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登录密码',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '姓名',
  `sex` int(11) NOT NULL COMMENT '性别',
  `college_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '所属部门 根据这个部门查所属学院',
  `type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '教师类型',
  `is_enabled` bit(1) NOT NULL COMMENT '启停状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '教师档案 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_tec_role
-- ----------------------------
DROP TABLE IF EXISTS `cms_tec_role`;
CREATE TABLE `cms_tec_role`  (
  `id` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `tec_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '教师编号',
  `role_id` bigint(19) NOT NULL COMMENT '角色编号',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '教师角色关系表 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for ise_7s_inspect
-- ----------------------------
DROP TABLE IF EXISTS `ise_7s_inspect`;
CREATE TABLE `ise_7s_inspect`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NULL DEFAULT b'0' COMMENT '逻辑删除',
  `arrange` bit(1) NULL DEFAULT NULL COMMENT '整理',
  `rectify` bit(1) NULL DEFAULT NULL COMMENT '整顿',
  `clear` bit(1) NULL DEFAULT NULL COMMENT '清扫',
  `clean` bit(1) NULL DEFAULT NULL COMMENT '清洁',
  `attainment` bit(1) NULL DEFAULT NULL COMMENT '素养',
  `safe` bit(1) NULL DEFAULT NULL COMMENT '安全',
  `economize` bit(1) NULL DEFAULT NULL COMMENT '节约',
  `inspect_id` bigint(20) NULL DEFAULT NULL COMMENT '检查项id 关联检查汇总',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '7s检查记录表 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for ise_dorm_inspect
-- ----------------------------
DROP TABLE IF EXISTS `ise_dorm_inspect`;
CREATE TABLE `ise_dorm_inspect`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NULL DEFAULT b'0' COMMENT '逻辑删除',
  `bed_num` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '夜不归宿(床号)',
  `high_power` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '大功率',
  `connect` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '私拉乱接',
  `hygiene` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '卫生',
  `praise` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表扬',
  `inspect_id` bigint(20) NULL DEFAULT NULL COMMENT '检查项id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '寝室检查记录表 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for ise_inspection
-- ----------------------------
DROP TABLE IF EXISTS `ise_inspection`;
CREATE TABLE `ise_inspection`  (
  `id` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NOT NULL DEFAULT b'0' COMMENT '逻辑删除',
  `duty_date` date NOT NULL COMMENT '值班日期',
  `week` int(10) NOT NULL COMMENT '周次',
  `duty_teacher` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '值班人员',
  `check_teacher` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '检查人员',
  `check_item` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '检查项目',
  `check_situation` varchar(228) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '具体检查内容和情况 用json串包含各个检查情况',
  `remarks` varchar(228) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `requires` varchar(228) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '整改要求 检查结果及存在问题',
  `result` varchar(228) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '整改结果 问题反馈及解决情况',
  `item_check_id` bigint(19) NOT NULL COMMENT '检查对象编号 关联检查对象的id，比如寝室检查关联寝室id',
  `feedback_status` int(10) NULL DEFAULT NULL COMMENT '反馈状态 表示是/否反馈和已反馈/待反馈',
  `teachers_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '其他相关老师 单个或多个老师',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '检查汇总 填写完检查后生成一条新的记录，未填写前没有这条记录' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for ise_schedule
-- ----------------------------
DROP TABLE IF EXISTS `ise_schedule`;
CREATE TABLE `ise_schedule`  (
  `id` bigint(19) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `create_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NULL DEFAULT b'0' COMMENT '逻辑删除',
  `term_id` bigint(19) NOT NULL COMMENT '学期id',
  `week` int(10) NULL DEFAULT NULL COMMENT '周次',
  `leader_teacher_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '值班领导id',
  `middle_teacher_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '值班中层id',
  `duty_teacher_one_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '值班班主任id',
  `duty_teacher_two_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '值班班主任2id',
  `instructor_teacher_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '值班辅导员',
  `create_teacher_id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '创建人id',
  `plan` varchar(228) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '计划',
  `summary` varchar(228) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '总结',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '值班计划  ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for ise_study_inspect
-- ----------------------------
DROP TABLE IF EXISTS `ise_study_inspect`;
CREATE TABLE `ise_study_inspect`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NULL DEFAULT b'0' COMMENT '逻辑删除',
  `on_time` bit(1) NULL DEFAULT NULL COMMENT '提前或按时到课',
  `chair_neat` bit(1) NULL DEFAULT NULL COMMENT '桌椅整齐',
  `clean` bit(1) NULL DEFAULT NULL COMMENT '垃圾清理及时',
  `discipline` int(11) NULL DEFAULT NULL COMMENT '纪律',
  `arrive_should` int(11) NULL DEFAULT NULL COMMENT '应到 学生到课情况',
  `arrive_actual` int(11) NULL DEFAULT NULL COMMENT '实到 学生到课情况',
  `arrive_leave` int(11) NULL DEFAULT NULL COMMENT '请假 学生到课情况',
  `arrive_late` int(11) NULL DEFAULT NULL COMMENT '迟到 学生到课情况',
  `attend_eat` int(11) NULL DEFAULT NULL COMMENT '带早餐 学生上课情况',
  `attend_phone` int(11) NULL DEFAULT NULL COMMENT '玩手机 学生上课情况',
  `attend_play` int(11) NULL DEFAULT NULL COMMENT '游戏 学生上课情况',
  `attend_sleep` int(11) NULL DEFAULT NULL COMMENT '睡觉 学生上课情况',
  `attend_nobook` int(11) NULL DEFAULT NULL COMMENT '不带书 学生上课情况',
  `attend_clothes` int(11) NULL DEFAULT NULL COMMENT '衣着不文明 学生上课情况',
  `inspect_id` bigint(20) NULL DEFAULT NULL COMMENT '检查项id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '自习检查 ' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for ise_teach_inspect
-- ----------------------------
DROP TABLE IF EXISTS `ise_teach_inspect`;
CREATE TABLE `ise_teach_inspect`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  `is_del` bit(1) NULL DEFAULT b'0' COMMENT '逻辑删除',
  `on_time` bit(1) NULL DEFAULT NULL COMMENT '提前或按时到课',
  `is_same` bit(1) NULL DEFAULT NULL COMMENT '实际是否与课表一致',
  `arrive_should` int(11) NULL DEFAULT NULL COMMENT '应到 学生到课情况',
  `arrive_actual` int(11) NULL DEFAULT NULL COMMENT '实到 学生到课情况',
  `arrive_leave` int(11) NULL DEFAULT NULL COMMENT '请假 学生到课情况',
  `arrive_late` int(11) NULL DEFAULT NULL COMMENT '迟到 学生到课情况',
  `attend_eat` int(11) NULL DEFAULT NULL COMMENT '带早餐 学生上课情况',
  `attend_phone` int(11) NULL DEFAULT NULL COMMENT '玩手机 学生上课情况',
  `attend_play` int(11) NULL DEFAULT NULL COMMENT '游戏 学生上课情况',
  `attend_sleep` int(11) NULL DEFAULT NULL COMMENT '睡觉 学生上课情况',
  `attend_nobook` int(11) NULL DEFAULT NULL COMMENT '不带书 学生上课情况',
  `attend_clothes` int(11) NULL DEFAULT NULL COMMENT '衣着不文明 学生上课情况',
  `inspect_id` bigint(20) NULL DEFAULT NULL COMMENT '检查项id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '教学检查 ' ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
